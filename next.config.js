const localeSubpaths = {};

module.exports = {
  env: {
    apiUrl: "https://el2tdnha20.execute-api.us-east-2.amazonaws.com/dev/graphql",
    pusherKey: "18160601861a89d7f8f7",
  },
  publicRuntimeConfig: {
    localeSubpaths,
  },
  webpack(config) {
    config.module.rules.push({
      test: /\.svg$/,
      issuer: {
        test: /\.(js|ts)x?$/,
      },
      use: ["@svgr/webpack"],
    });

    return config;
  },
};
