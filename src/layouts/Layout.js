import PropTypes from "prop-types";
import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Container from "@material-ui/core/Container";
import styles from "../../scss/layout/Layout.module.scss";
import Header from "./Header";
import Footer from "./Footer";
import Authentication from "../auth/authentication";
import SnackbarMessenger from "../components/UI/SnackBar";

const Layout = ({ children }) => {
  const { logout, refresh, getToken } = Authentication();
  const router = useRouter();

  useEffect(() => {
    refresh();
  }, []);

  const token = getToken();

  const [stateSnackBar, setStateSnackBar] = useState({
    open: false,
    message: "",
    vertical: "top",
    horizontal: "center",
  });

  const handleClickSnackBar = (text, newState) => {
    setStateSnackBar({ open: true, message: text, ...newState });
  };

  const handleCloseSnackBar = () => {
    setStateSnackBar({ ...stateSnackBar, open: false });
  };

  const newChild = React.cloneElement(children, {
    token,
    snackBarOpen: handleClickSnackBar,
  });

  return (
    <Container maxWidth="xl">
      <div className={styles.layout}>
        {router.asPath !== "/login" && <Header logout={logout} />}
        <div className={styles.main}>
          <div className={styles.content}>{newChild}</div>
        </div>
        {router.asPath !== "/login" && <Footer />}
      </div>

      <SnackbarMessenger
        handleClose={handleCloseSnackBar}
        horizontal={stateSnackBar.horizontal}
        vertical={stateSnackBar.vertical}
        open={stateSnackBar.open}
        message={stateSnackBar.message}
      />
    </Container>
  );
};

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
