import PropTypes from "prop-types";
import { Button } from "@material-ui/core";
import Logo from "../components/general/Logo";
import styles from "../../scss/layout/Header.module.scss";

const Header = ({ logout }) => (
  <div className={styles.container}>
    <Logo urlLogo="/static/images/logoFooter/branding.png" styleType="logo" href="/" />
    <Button variant="contained" color="primary" onClick={logout} style={{ height: "50%" }}>
      Logout
    </Button>
  </div>
);

Header.defaultProps = {
  logout: () => {},
};

Header.propTypes = {
  logout: PropTypes.func,
};
export default Header;
