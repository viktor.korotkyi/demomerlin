import { ApolloProvider } from "@apollo/client";
import PropTypes from "prop-types";
import { useApollo } from "../../apollo/client";
import Layout from "../Layout";

const Apollo = ({ Component, pageProps }) => {
  const apolloClient = useApollo(pageProps.initialApolloState);
  return (
    <ApolloProvider client={apolloClient}>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </ApolloProvider>
  );
};

Apollo.propTypes = {
  Component: PropTypes.func.isRequired,
  pageProps: PropTypes.shape({
    initialApolloState: PropTypes.shape({}),
  }).isRequired,
};

export default Apollo;
