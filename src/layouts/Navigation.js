import PropTypes from "prop-types";
import Menu from "../components/menu/Menu";
import styles from "../../scss/layout/Navigation.module.scss";

const Navigation = ({ children }) => {
  return (
    <div className={styles.layout}>
      <div className={styles.menu}>
        <Menu />
      </div>
      <div className={styles.content}>{children}</div>
    </div>
  );
};

Navigation.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Navigation;
