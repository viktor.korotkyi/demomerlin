import LinkRef from "../components/UI/LinkRef";
import styles from "../../scss/layout/Footer.module.scss";

const Footer = () => (
  <div className={styles.container}>
    <div className={styles.policeBox}>
      <LinkRef href="/privacy" typeStyle="policyLink" pathUrl="police">
        privacy
      </LinkRef>
      <LinkRef href="/terms-condition" typeStyle="policyLink" pathUrl="police">
        terms
      </LinkRef>
    </div>
  </div>
);

export default Footer;
