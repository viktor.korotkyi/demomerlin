import gql from "graphql-tag";

const request = {
  Upload: gql`
    mutation Upload($input: UploadInput!) {
      Upload(input: $input) {
        src
      }
    }
  `,
  UpdateTemplate: gql`
    mutation UpdateTemplate($input: UpdateTemplateInput!) {
      UpdateTemplate(input: $input) {
        templateId
        portalId
        name
        title
        width
        height
        elements
        date
        backgroundImage
        typeEditor
        code
      }
    }
  `,
  Templates: gql`
    query Templates($input: TemplatesInput!) {
      Templates(input: $input) {
        templates {
          templateId
          portalId
          name
          title
          width
          height
          elements
          date
          backgroundImage
          typeEditor
        }
      }
    }
  `,
  DeleteTemplate: gql`
    mutation DeleteTemplate($input: GetTemplateInput!) {
      DeleteTemplate(input: $input) {
        success
      }
    }
  `,
  Template: gql`
    query Template($input: GetTemplateInput!) {
      Template(input: $input) {
        templateId
        title
        name
        width
        height
        elements
        date
        backgroundImage
        typeEditor
        code
      }
    }
  `,
  login: gql`
    mutation login($input: LoginInput!) {
      login(input: $input) {
        auth {
          token
          refreshToken
        }
        success {
          error {
            message
          }
          success
        }
      }
    }
  `,
  Portals: gql`
    query Portals($input: PortalsInput!) {
      Portals(input: $input) {
        key
        portals {
          portalId
          name
          logo {
            alt
            src
          }
        }
      }
    }
  `,
  UpdatePortal: gql`
    mutation UpdatePortal($input: InputPortal!) {
      UpdatePortal(input: $input) {
        portalId
        name
        logo {
          src
          alt
        }
      }
    }
  `,
};

export default request;
