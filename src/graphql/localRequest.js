import gql from "graphql-tag";

const localRequest = {
  ID_PORTAL: gql`
    query idPortal {
      idPortal @client
      namePortal @client
    }
  `,
  IS_LOGGED_IN: gql`
    query IsUserLoggedIn {
      token @client
      refreshToken @client
    }
  `,
  TYPE_SETTING: gql`
    query typeSetting {
      typeSetting @client
    }
  `,
  ID_TEMPLATE: gql`
    query idTemplate {
      idTemplate @client
    }
  `,
};

export default localRequest;
