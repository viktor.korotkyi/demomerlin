import { gql } from "@apollo/client";

const typeDefs = gql`
  type User {
    id: ID!
    name: String!
    status: String!
  }

  type Country {
    code: String!
  }

  type Query {
    viewer: User
    country: Country
  }
`;

export default typeDefs;
