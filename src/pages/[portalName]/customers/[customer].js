import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import {
  makeStyles,
  Typography,
  Grid,
  Toolbar,
  Button,
  withStyles,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from "@material-ui/core";
import { green } from "@material-ui/core/colors";
import Navigation from "../../../layouts/Navigation";
import users from "../../../utils/mockUsers";

const useStyles = makeStyles({
  formControl: {
    minWidth: 120,
  },
});

const ColorButton = withStyles((theme) => ({
  root: {
    color: theme.palette.getContrastText(green[500]),
    backgroundColor: green[500],
    "&:hover": {
      backgroundColor: green[700],
    },
  },
}))(Button);

const Customer = () => {
  const router = useRouter();
  const classes = useStyles();
  const [user, setUser] = useState(null);
  const [inputValue, setInputValue] = useState("");
  const [userType, setUserType] = useState("");

  const handleChangeUserType = (event) => {
    setUserType(event.target.value);
  };

  const handleChangeInputValue = (event) => {
    setInputValue(event.target.value);
  };

  useEffect(() => {
    setUser(users.find((currentUser) => currentUser.id === router.query.customer));
  }, []);

  useEffect(() => {
    if (user) {
      setUserType(user.userType);
      setInputValue(user.firstName);
    }
  }, [user]);

  return (
    <Navigation>
      <Grid container direction="column">
        <Toolbar>
          <Grid item xs={8}>
            <Typography variant="h3" component="h3">
              Edit customer&nbsp;
              {router.query.customer}
            </Typography>
          </Grid>
          <Grid item xs={4} container>
            <Grid item xs={8}>
              <ColorButton variant="contained" color="primary">
                Save
              </ColorButton>
            </Grid>
            <Grid item xs={4}>
              <Button variant="contained">Cancel</Button>
            </Grid>
          </Grid>
        </Toolbar>
        <Grid item xs={12} container alignItems="flex-end">
          <Grid item xs={3}>
            <TextField label="Edit name" value={inputValue} onChange={handleChangeInputValue} />
          </Grid>
          <Grid item xs={5}>
            <Button variant="contained" color="primary">
              Edit name
            </Button>
          </Grid>
          <FormControl className={classes.formControl}>
            <InputLabel id="select-user-type">User type</InputLabel>
            <Select labelId="select-user-type" value={userType} onChange={handleChangeUserType}>
              <MenuItem value="Admin">Admin</MenuItem>
              <MenuItem value="Customer">Customer</MenuItem>
              <MenuItem value="Other">Other</MenuItem>
            </Select>
          </FormControl>
        </Grid>
      </Grid>
    </Navigation>
  );
};

export default Customer;
