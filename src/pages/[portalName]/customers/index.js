import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import { TableCell, TableRow } from "@material-ui/core";
import Navigation from "../../../layouts/Navigation";
import Table from "../../../components/table/Table";
import styles from "../../../../scss/components/customers/Customers.module.scss";
import usePagination from "../../../hooks/usePagination";
import users from "../../../utils/mockUsers";
import { filterSearchItem } from "../../../utils/storageWork";
import Link from "../../../components/UI/Link";
import SearchField from "../../../components/UI/SearchField";

const headers = ["Name", "User type", "Options"];

const Customers = () => {
  const router = useRouter();
  const [paginationPage, handlePaginationPage] = usePagination();
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [searchField, setSearchField] = useState("");
  const [customers, setCustomers] = useState(users);

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    handlePaginationPage(null, 0);
  };

  const search = (event) => {
    setSearchField(event.target.value);
  };

  useEffect(() => {
    const arrayFilter = filterSearchItem(users, searchField);
    setCustomers(arrayFilter);
  }, [searchField]);

  return (
    <Navigation>
      <div className={styles.container}>
        <SearchField label="Search customer" value={searchField} onChange={search} />
        <Table
          heading="Customers list"
          headers={headers}
          count={customers.length}
          rowsPerPage={rowsPerPage}
          page={paginationPage}
          onChangePage={handlePaginationPage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        >
          {customers
            .slice(paginationPage * rowsPerPage, paginationPage * rowsPerPage + rowsPerPage)
            .map((user) => (
              <TableRow key={user.id} hover>
                <TableCell>{user.firstName}</TableCell>
                <TableCell>{user.userType}</TableCell>
                <TableCell>
                  <Link href={`/${router.query.portalName}/customers/${user.id}`} label="Edit" />
                </TableCell>
              </TableRow>
            ))}
        </Table>
      </div>
    </Navigation>
  );
};

export default Customers;
