import PropTypes from "prop-types";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import packageList from "../../../utils/packagesList";
import constants from "../../../constants/storage";
import useModal from "../../../hooks/useModal";
import PackageView from "../../../components/pages/PackageView";

const Package = ({ snackBarOpen }) => {
  const router = useRouter();
  const [name, setName] = useState(
    () => packageList.find((packageItem) => packageItem.id === router.query.package)?.value
  );
  const [avatar, setAvatar] = useState(null);
  const [chosenValue, setChosenValue] = useState([]);
  const [openDialogOnCancel, setDialogStateOnClick] = useState(false);
  const [disableSave, setDisableSave] = useState(false);
  const [selectedTimezone, setSelectedTimezone] = useState({});
  const [modalState, modalHandler] = useModal();
  const [editTemplatePricing, setEditTemplatePricing] = useState({
    cost: "",
    imageCost: "",
    perInchcost: "",
  });

  const handleChosenValue = (event) => {
    setChosenValue(event.target.value);
  };

  const handleName = (event) => {
    setName(event.target.value);
  };

  const handleDialogOnCancel = () => {
    setDialogStateOnClick(false);
  };

  const handleUploadAvatar = (event) => {
    if (event.target.files && event.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener("load", () => setAvatar(reader.result));
      reader.readAsDataURL(event.target.files[0]);
    }
  };

  const handleNavigationBack = () => {
    const {
      query: { portalName },
    } = router;
    router.push("/[portalName]/packages", `/${portalName}/packages`);
  };

  const handleCancelButtonClick = () => {
    handleNavigationBack();
  };

  const handleDeleteButtonClick = () => {
    setDialogStateOnClick(true);
  };

  const handleSetupTemplate = (event) => {
    const { id, value } = event.target;
    setEditTemplatePricing({ ...editTemplatePricing, [id]: value });
  };

  const handleUpdateAlert = () => {
    snackBarOpen(constants.deletePublication, constants.placementTopCenter);
    handleNavigationBack();
  };

  const handleSaveButtonClick = () => {
    snackBarOpen(constants.savePublication, constants.placementTopCenter);
  };

  const handleTemplateEdit = () => {
    modalHandler();
  };

  const handleTemplateCancel = () => {
    modalHandler();
  };

  const handleTemplateUpdate = () => {};

  useEffect(() => {
    if (!name || !avatar) {
      setDisableSave(true);
    } else {
      setDisableSave(false);
    }
  }, [name, avatar]);

  return (
    <PackageView
      name={name}
      handleName={handleName}
      avatar={avatar}
      handleUploadAvatar={handleUploadAvatar}
      disableSave={disableSave}
      setSelectedTimezone={setSelectedTimezone}
      selectedTimezone={selectedTimezone}
      modalState={modalState}
      modalHandler={modalHandler}
      chosenValue={chosenValue}
      handleChosenValue={handleChosenValue}
      handleSaveButtonClick={handleSaveButtonClick}
      handleCancelButtonClick={handleCancelButtonClick}
      handleDeleteButtonClick={handleDeleteButtonClick}
      handleDialogOnCancel={handleDialogOnCancel}
      handleSetupTemplate={handleSetupTemplate}
      handleTemplateCancel={handleTemplateCancel}
      handleTemplateEdit={handleTemplateEdit}
      handleTemplateUpdate={handleTemplateUpdate}
      handleUpdateAlert={handleUpdateAlert}
      openDialogOnCancel={openDialogOnCancel}
      editTemplatePricing={editTemplatePricing}
    />
  );
};

Package.defaultProps = {
  snackBarOpen: () => {},
};

Package.propTypes = {
  snackBarOpen: PropTypes.func,
};

export default Package;
