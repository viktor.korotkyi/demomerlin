import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import styles from "../../../../scss/components/packages/Packages.module.scss";
import SearchField from "../../../components/UI/SearchField";
import Table from "../../../components/table/Table";
import TableRows from "../../../components/table/TableRows";
import Navigation from "../../../layouts/Navigation";
import { filterSearchItem } from "../../../utils/storageWork";
import arrayPackages from "../../../utils/packagesData";
import usePagination from "../../../hooks/usePagination";

const headers = ["Name", "Date", "Options"];

const Packages = () => {
  const [searchField, setSearchField] = useState("");
  const [packages, setPackages] = useState(arrayPackages.packagesList);
  const [rowsPerPage, setRowsPerPage] = useState(5);

  const router = useRouter();
  const [paginationPage, handlePaginationPage] = usePagination();

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    handlePaginationPage(null, 0);
  };

  const edit = (id) => {
    const {
      query: { portalName },
    } = router;

    router.push("/[portalName]/packages/[package]", `/${portalName}/packages/${id}`);
  };

  const search = (event) => {
    setSearchField(event.target.value);
  };

  useEffect(() => {
    const arrayFilter = filterSearchItem(arrayPackages.packagesList, searchField);
    setPackages(arrayFilter);
  }, [searchField]);

  return (
    <Navigation>
      <div className={styles.container}>
        <SearchField label="Search packages" value={searchField} onChange={search} />
        <Table
          heading="Packages list"
          headers={headers}
          count={packages.length}
          rowsPerPage={rowsPerPage}
          page={paginationPage}
          onChangePage={handlePaginationPage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        >
          <TableRows
            paginationPage={paginationPage}
            rowsPerPage={rowsPerPage}
            items={packages}
            edit={edit}
          />
        </Table>
      </div>
    </Navigation>
  );
};

export default Packages;
