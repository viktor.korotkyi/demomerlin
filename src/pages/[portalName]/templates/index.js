import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { useQuery } from "@apollo/client";
import { initializeApollo } from "../../../apollo/client";
import Navigation from "../../../layouts/Navigation";
import SearchField from "../../../components/UI/SearchField";
import Table from "../../../components/table/Table";
import { filterSearchItem } from "../../../utils/storageWork";
import localRequest from "../../../graphql/localRequest";
import request from "../../../graphql/request";
import constants from "../../../constants/storage";
import tablesConstants from "../../../constants/tablesConstants";
import ButtonColor from "../../../components/UI/ButtonColor";
import usePagination from "../../../hooks/usePagination";
import styles from "../../../../scss/components/templates/Templates.module.scss";
import TableRows from "../../../components/table/TableRows";

const Templates = () => {
  const apolloClient = initializeApollo();
  const router = useRouter();
  const idPortal = useQuery(localRequest.ID_PORTAL);
  const token = useQuery(localRequest.IS_LOGGED_IN);
  const { data, refetch } = useQuery(request.Templates, {
    variables: {
      input: { limit: 100, portalId: `${idPortal.data && idPortal.data.idPortal}` },
    },
    context: { headers: { Authorization: `Bearer ${token.data && token.data.token}` } },
  });

  const [paginationPage, handlePaginationPage] = usePagination();
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [searchField, setSearchField] = useState("");
  const [templates, setTemplates] = useState([]);

  useEffect(() => {
    refetch({
      variables: {
        input: { limit: 100, portalId: `${idPortal.data && idPortal.data.idPortal}` },
      },
      context: { headers: { Authorization: `Bearer ${token.data && token.data.token}` } },
    });
  }, []);

  useEffect(() => {
    if (data) {
      setTemplates(data && data.Templates.templates);
    } else {
      setTemplates([]);
    }
  }, [data]);

  const search = (event) => {
    setSearchField(event.target.value);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    handlePaginationPage(null, 0);
  };

  const edit = (id) => {
    const templateId = id || constants.newTemplates;
    apolloClient.cache.writeQuery({
      query: localRequest.ID_TEMPLATE,
      data: { idTemplate: templateId },
    });
    router.push(`/${router.query.portalName}/templates/${templateId}`);
  };

  useEffect(() => {
    if (data) {
      const arrayFilter = filterSearchItem(data.Templates.templates, searchField);
      setTemplates(arrayFilter);
    }
  }, [searchField]);

  return (
    <Navigation>
      <div className={styles.container}>
        <div className={styles.header}>
          <h3>Templates list</h3>
          <ButtonColor click={edit} label="Create template" color="primary" />
        </div>
        <SearchField label="Search customer" onChange={search} value={searchField} />
        <Table
          heading="Customers list"
          headers={tablesConstants.headerstemplatesPage}
          count={templates.length}
          rowsPerPage={rowsPerPage}
          page={paginationPage}
          onChangePage={handlePaginationPage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        >
          <TableRows
            paginationPage={paginationPage}
            rowsPerPage={rowsPerPage}
            items={templates}
            edit={edit}
          />
        </Table>
      </div>
    </Navigation>
  );
};

export default Templates;
