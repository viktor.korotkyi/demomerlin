import PropTypes from "prop-types";
import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import { useQuery, useMutation } from "@apollo/client";
import { EditorState, convertFromRaw } from "draft-js";
import Navigation from "../../../layouts/Navigation";
import localRequest from "../../../graphql/localRequest";
import request from "../../../graphql/request";
import AlertDialog from "../../../components/UI/AlertDialog";
import constants from "../../../constants/storage";
import typeEditor from "../../../constants/editor";
import ViewTemplateSetting from "../../../components/templates/ViewTemplateSetting";
import listTemplates from "../../../utils/defaultTemplateList";
import templateSize from "../../../utils/templateSize";
import { filterArray } from "../../../utils/storageWork";

const Template = ({ snackBarOpen }) => {
  const router = useRouter();
  const token = useQuery(localRequest.IS_LOGGED_IN);
  const idTemplate = useQuery(localRequest.ID_TEMPLATE);
  const idPortal = useQuery(localRequest.ID_PORTAL);
  const getTemplate = useQuery(request.Template, {
    variables: {
      input: {
        templateId: `${idTemplate.data && idTemplate.data.idTemplate}`,
        portalId: `${idPortal.data && idPortal.data.idPortal}`,
      },
    },
    context: { headers: { Authorization: `Bearer ${token.data && token.data.token}` } },
  });

  const [updateTemplate] = useMutation(request.UpdateTemplate);
  const emptyContentState = convertFromRaw({
    blocks: [],
    entityMap: {},
  });
  const [deleteTemplate] = useMutation(request.DeleteTemplate);
  const [open, setOpen] = useState(false);
  const [chosedTemplate, setChosedTemplate] = useState("");
  const [sizeTemplate, setSizeTemplate] = useState(undefined);
  const [openCode, setOpenCode] = useState(false);
  const [dataTemplate, newDataTemplate] = useState({
    width: constants.width,
    height: constants.height,
    typeEditor: constants.typeTextEditor,
    code: {},
    elements: [],
  });
  const [editorState, setEditorState] = useState(EditorState.createWithContent(emptyContentState));
  const [dataTemplateCodeEditor, setDataTemplateCodeEditor] = useState({});

  const handlerTemplateCodeEditor = (data) => {
    setDataTemplateCodeEditor(data);
  };

  useEffect(() => {
    setOpenCode(false);
    if (getTemplate.data && getTemplate.data.Template.typeEditor === typeEditor.typeHtmlCss) {
      setOpenCode(true);
    }
  }, [getTemplate]);

  useEffect(() => {
    const template = filterArray(templateSize, chosedTemplate);
    setSizeTemplate(template[0]);
  }, [chosedTemplate]);

  const onChange = (state) => {
    setEditorState(state);
  };

  const cancelBtn = () => {
    const portal = router.asPath.split("/")[1];
    router.push("/[portalName]/templates", `/${portal}/templates`);
  };

  const deleteBtn = () => {
    snackBarOpen(constants.deletePublication, constants.placementTopCenter);
    deleteTemplate({
      variables: {
        input: {
          templateId: idTemplate.data.idTemplate,
          portalId: idPortal.data && idPortal.data.idPortal,
        },
      },
      context: { headers: { Authorization: `Bearer ${token.data.token}` } },
    });
    setOpen(true);
  };

  const saveBtn = () => {
    snackBarOpen(constants.savePublication, constants.placementTopCenter);
    updateTemplate({
      variables: { input: dataTemplateCodeEditor },
      context: { headers: { Authorization: `Bearer ${token.data.token}` } },
    });
    cancelBtn();
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleUpdateAlert = () => {
    snackBarOpen(constants.deletePublication, constants.placementTopCenter);
    const portal = router.asPath.split("/")[1];
    router.push("/[portalName]/templates", `/${portal}/templates`);
  };

  const handlerDataTemplate = (data, name, dataObj) => {
    if (dataObj !== undefined) {
      newDataTemplate({
        ...dataTemplate,
        width: dataObj.width,
        height: dataObj.height,
        code: editorState,
      });
    } else {
      newDataTemplate({ ...dataTemplate, code: editorState, [name]: data });
    }
  };

  useEffect(() => {
    if (sizeTemplate) {
      handlerDataTemplate(null, null, sizeTemplate);
    }
  }, [sizeTemplate]);

  const handleSelect = (event) => {
    const { value } = event.target;
    setChosedTemplate(value);
  };

  const openCodeEditor = () => {
    setOpenCode(!openCode);
  };

  return (
    <Navigation>
      <ViewTemplateSetting
        dataTemplate={dataTemplate}
        handlerDataTemplate={handlerDataTemplate}
        saveBtn={saveBtn}
        cancelBtn={cancelBtn}
        deleteBtn={deleteBtn}
        handleSelect={handleSelect}
        listTemplates={listTemplates}
        openCodeEditor={openCodeEditor}
        openCode={openCode}
        editorState={editorState}
        onChange={onChange}
        sizeTemplate={sizeTemplate}
        handlerTemplateCodeEditor={handlerTemplateCodeEditor}
        dataTemplateCodeEditor={getTemplate.data && getTemplate.data.Template}
        portalId={idPortal.data && idPortal.data.idPortal}
      />
      <AlertDialog
        open={open}
        handleClose={handleClose}
        title="Edit template"
        handleUpdateAlert={handleUpdateAlert}
        decription="Do you want delete this template"
      />
    </Navigation>
  );
};

Template.defaultProps = {
  snackBarOpen: () => {},
};

Template.propTypes = {
  snackBarOpen: PropTypes.func,
};
export default Template;
