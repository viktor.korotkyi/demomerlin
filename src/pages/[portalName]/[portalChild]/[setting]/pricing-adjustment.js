import { useState } from "react";
import { makeStyles, Button, Paper, Typography } from "@material-ui/core";
import Navigation from "../../../../layouts/Navigation";
import styles from "../../../../../scss/components/PricingAdjustmentPage/PricingAdjustmentPage.module.scss";
import EditList from "../../../../components/general/EditList";
import ButtonGroupForm from "../../../../components/general/ButtonGroupForm";

const useStyles = makeStyles((theme) => ({
  priceButton: {
    marginTop: theme.spacing(3),
  },
}));

const priceAdjustmentNames = [
  {
    id: 1,
    name: "Tribune",
  },
  {
    id: 2,
    name: "Obits",
  },
  {
    id: 3,
    name: "More",
  },
  {
    id: 4,
    name: "Many more",
  },
  {
    id: 5,
    name: "Other",
  },
];

const PricingAdjustment = () => {
  const classes = useStyles();
  const [readyPriceAdjustmentList, setReadyPriceAdjustmentList] = useState(priceAdjustmentNames);
  const [newPriceAdjustmentsList, setNewPriceAdjustmentsList] = useState([]);

  const createNewPriceAdjustment = () => {
    setNewPriceAdjustmentsList((prevUsers) => [
      ...prevUsers,
      { id: prevUsers.length + 1, name: "Empty", edit: false },
    ]);
  };

  const handlePriceAdjustmentItemEdit = (dispatch) => (id) => {
    dispatch((prevList) =>
      prevList.map((prevListItem) =>
        prevListItem.id === id ? { ...prevListItem, edit: !prevListItem.edit } : prevListItem
      )
    );
  };

  const handlePriceAdjustmentName = (dispatch) => (id, newValue) => {
    dispatch((list) =>
      list.map((listItem) =>
        listItem.id === id ? { ...listItem, name: newValue, edit: !listItem.edit } : listItem
      )
    );
  };

  return (
    <Navigation>
      <Paper elevation={0} className={styles.container}>
        <Typography variant="h5">Pricing adjustment</Typography>
        <EditList
          label="Name field"
          items={readyPriceAdjustmentList}
          handleEdit={handlePriceAdjustmentItemEdit(setReadyPriceAdjustmentList)}
          handleSave={handlePriceAdjustmentName(setReadyPriceAdjustmentList)}
        />
        <ButtonGroupForm />
        <Button
          variant="contained"
          className={classes.priceButton}
          onClick={createNewPriceAdjustment}
        >
          Add adjustment price
        </Button>
        <EditList
          label="Name field"
          items={newPriceAdjustmentsList}
          handleEdit={handlePriceAdjustmentItemEdit(setNewPriceAdjustmentsList)}
          handleSave={handlePriceAdjustmentName(setNewPriceAdjustmentsList)}
        />
      </Paper>
    </Navigation>
  );
};

export default PricingAdjustment;
