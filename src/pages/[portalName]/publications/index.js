import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import styles from "../../../../scss/components/publications/Publications.module.scss";
import ButtonColor from "../../../components/UI/ButtonColor";
import SearchField from "../../../components/UI/SearchField";
import Table from "../../../components/table/Table";
import TableRows from "../../../components/table/TableRows";
import Navigation from "../../../layouts/Navigation";
import { filterSearchItem, randomId } from "../../../utils/storageWork";
import arrayPackages from "../../../utils/packagesData";
import { initializeApollo } from "../../../apollo/client";
import usePagination from "../../../hooks/usePagination";
import localRequest from "../../../graphql/localRequest";
import constants from "../../../constants/storage";

const headers = ["Name", "Date", "Options"];

const Publications = () => {
  const apolloClient = initializeApollo();
  const [searchField, setSearchField] = useState("");
  const [publications, setPublications] = useState(arrayPackages.packagesList);
  const [rowsPerPage, setRowsPerPage] = useState(5);

  const router = useRouter();
  const [paginationPage, handlePaginationPage] = usePagination();

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    handlePaginationPage(null, 0);
  };

  const edit = (id) => {
    // idRandom in the future we will receive from the server
    const idRandom = randomId();
    const portal = router.asPath.split("/")[1];

    apolloClient.cache.writeQuery({
      query: localRequest.TYPE_SETTING,
      data: { typeSetting: constants.typePublications },
    });

    router.push(
      "/[portalName]/publications/[publication]",
      `/${portal}/publications/${id || idRandom}`
    );
  };

  const search = (event) => {
    setSearchField(event.target.value);
  };

  useEffect(() => {
    const arrayFilter = filterSearchItem(arrayPackages.packagesList, searchField);
    setPublications(arrayFilter);
  }, [searchField]);

  return (
    <Navigation>
      <div className={styles.container}>
        <div className={styles.header}>
          <ButtonColor click={edit} label="Create publication" color="primary" />
        </div>
        <SearchField label="Search publication" value={searchField} onChange={search} />
        <Table
          heading="Publication list"
          headers={headers}
          count={publications.length}
          rowsPerPage={rowsPerPage}
          page={paginationPage}
          onChangePage={handlePaginationPage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        >
          <TableRows
            paginationPage={paginationPage}
            rowsPerPage={rowsPerPage}
            items={publications}
            edit={edit}
          />
        </Table>
      </div>
    </Navigation>
  );
};

export default Publications;
