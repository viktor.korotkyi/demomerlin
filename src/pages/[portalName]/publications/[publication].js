import PropTypes from "prop-types";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { useQuery } from "@apollo/client";
import Navigation from "../../../layouts/Navigation";
import localRequest from "../../../graphql/localRequest";
import {
  getEmptyFormDataBool,
  days,
  createDaysObj,
  createSetupItems,
} from "../../../utils/storageWork";
import ViewPublications from "../../../components/setting/ViewPublications";
import packageList from "../../../utils/packagesList";
import EditPackage from "../../../components/setting/EditPackage";
import listTemplate from "../../../utils/listTemplate";
import AlertDialog from "../../../components/UI/AlertDialog";
import constants from "../../../constants/storage";

const Publication = ({ snackBarOpen }) => {
  const router = useRouter();
  const typeItem = useQuery(localRequest.TYPE_SETTING);
  const [view, setView] = useState({
    publications: false,
    packages: false,
    templates: false,
  });
  const [viewSettingPackages, setViewPackages] = useState(false);
  const [input, setInput] = useState({
    name: "",
    logo: "",
    select: [],
  });
  const [selectedTimezone, setSelectedTimezone] = useState("");
  const [disableSave, setDisableSave] = useState(false);
  const [checkBox, setCheckBox] = useState({
    days: "",
    check: false,
  });
  const [viewSetup, setViewSetup] = useState({});
  const [valueSetup, setValueSetup] = useState({});
  const [editPackagesInfo, setEditPackagesInfo] = useState({
    pricePerInch: "",
    valueTribune: "",
    selectedAdjustmentPrice: "",
    valueCheckPriceDay: false,
  });

  const [templateCost, setTemplateCost] = useState("");
  const [templateImageCost, setTemplateImageCost] = useState("");
  const [templatePerInchCost, setTemplatePerInchCost] = useState("");
  const [open, setOpen] = useState(false);

  useEffect(() => {
    const objDays = createDaysObj(days);
    setViewSetup(objDays);
  }, []);

  const handleChange = (event) => {
    setCheckBox({ ...checkBox, check: event.target.checked });
  };

  useEffect(() => {
    const objDays = createSetupItems(days);
    setValueSetup(objDays);
  }, []);

  const handleSetup = (event) => {
    const { id, value, type } = event.target;
    if (type === "time") {
      setValueSetup({ ...valueSetup, [id]: { cost: valueSetup[id].cost, time: value } });
    } else {
      setValueSetup({ ...valueSetup, [id]: { cost: value, time: valueSetup[id].time } });
    }
  };

  const onChangeInput = (event) => {
    setCheckBox({ ...checkBox, days: event.target.value });
  };

  const handlePublishDay = (event) => {
    const { id } = event.target;
    setViewSetup({ ...viewSetup, [id]: !viewSetup[id] });
  };

  useEffect(() => {
    if (!input.name || !input.logo) {
      setDisableSave(true);
    } else {
      setDisableSave(false);
    }
  }, [input]);

  const handleView = (id) => {
    const object = getEmptyFormDataBool(view);
    setView({ ...object, [id]: !object[id] });
  };

  const handleInput = (event) => {
    const { id, value } = event.target;
    setInput({ ...input, [id]: value });
  };

  useEffect(() => {
    if (typeItem.data && typeItem.data.typeSetting) {
      handleView(typeItem.data && typeItem.data.typeSetting);
    }
  }, [typeItem]);

  const uploadFile = (event) => {
    if (event.target.files && event.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener("load", () => setInput({ ...input, logo: reader.result }));
      reader.readAsDataURL(event.target.files[0]);
    }
  };

  const handleSelect = (event) => {
    setInput({ ...input, select: event.target.value });
  };

  const editChoosedPackages = () => {
    setView({ ...view, publications: false });
    setViewPackages(true);
  };

  const handlePackageInfo = (event) => {
    const { id, value, checked } = event.target;
    if (id === constants.valueCheckPriceDay) {
      setEditPackagesInfo({ ...editChoosedPackages, [id]: checked });
    } else {
      setEditPackagesInfo({ ...editChoosedPackages, [id]: value });
    }
  };

  const editTemplate = () => {};

  const cancelBtn = () => {
    const portal = router.asPath.split("/")[1];
    router.push("/[portalName]/publications", `/${portal}/publications`);
  };

  const deleteBtn = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleUpdateAlert = () => {
    snackBarOpen(constants.deletePublication, constants.placementTopCenter);
    const portal = router.asPath.split("/")[1];
    router.push("/[portalName]/publications", `/${portal}/publications`);
  };

  const saveBtn = () => {
    snackBarOpen(constants.savePublication, constants.placementTopCenter);
  };

  const cancelPackageInfo = () => {
    setView({ ...view, publications: true });
    setViewPackages(false);
  };

  const updatePackageInfo = () => {};

  const cancelTemplateEdit = () => {};

  const updateTemplateEdit = ({ cost, imageCost, perInchCost }) => {
    setTemplateCost(cost);
    setTemplateImageCost(imageCost);
    setTemplatePerInchCost(perInchCost);
  };

  return (
    <Navigation>
      {view.publications && (
        <ViewPublications
          header={typeItem.data && typeItem.data.typeSetting}
          name={input.name}
          handleInput={handleInput}
          fileUpload={input.logo}
          uploadFile={uploadFile}
          listSelectPackages={packageList}
          handleSelect={handleSelect}
          selectedTimezone={selectedTimezone}
          setSelectedTimezone={setSelectedTimezone}
          cancelBtn={cancelBtn}
          deleteBtn={deleteBtn}
          disabled={disableSave}
          saveBtn={saveBtn}
          handleCheck={handleChange}
          onChangeInput={onChangeInput}
          panelDays={checkBox.days}
          handlePublishDay={handlePublishDay}
          listDaysPub={days}
          viewSetup={viewSetup}
          handleSetup={handleSetup}
          valueSetup={valueSetup}
          editChoosedPackages={editChoosedPackages}
          listChoosedPackages={input.select}
        />
      )}
      {viewSettingPackages && (
        <EditPackage
          editPackagesInfo={editPackagesInfo}
          onChange={handlePackageInfo}
          listAdjustment={packageList}
          editTemplate={editTemplate}
          listTemplate={listTemplate}
          cancelPackageInfo={cancelPackageInfo}
          updatePackageInfo={updatePackageInfo}
          cost={templateCost}
          imageCost={templateImageCost}
          perInchCost={templatePerInchCost}
          cancelTemplateEdit={cancelTemplateEdit}
          updateTemplateEdit={updateTemplateEdit}
        />
      )}
      <AlertDialog
        open={open}
        handleClose={handleClose}
        title="Edit Element"
        handleUpdateAlert={handleUpdateAlert}
        decription="Do you want delete this packages"
      />
    </Navigation>
  );
};

Publication.defaultProps = {
  snackBarOpen: () => {},
};

Publication.propTypes = {
  snackBarOpen: PropTypes.func,
};
export default Publication;
