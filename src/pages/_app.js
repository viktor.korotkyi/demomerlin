import React from "react";
import Apollo from "../layouts/Apollo/Apollo";
import "../../scss/main.scss";

const App = (props) => <Apollo {...props} />;

export default App;
