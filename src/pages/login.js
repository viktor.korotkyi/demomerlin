import PropTypes from "prop-types";
import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import LinkMaterial from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import Authentication from "../auth/authentication";

const Login = ({ token }) => {
  const { loginUser } = Authentication();
  const router = useRouter();
  const [input, setInput] = useState({
    email: "",
    password: "",
    remember: false,
  });

  useEffect(() => {
    if (token === undefined) {
      router.push("/login");
    }
  }, [token]);

  const handleInput = (event) => {
    const { id, value } = event.target;
    setInput({ ...input, [id]: value });
  };

  const handleChange = (event) => {
    setInput({ ...input, remember: event.target.checked });
  };

  const submit = (event) => {
    event.preventDefault();
    loginUser(input);
  };

  return (
    <Container component="main" maxWidth="xs" style={{ paddingTop: "15%" }}>
      <Typography component="h1" variant="h5">
        Sign in
      </Typography>
      <form onSubmit={submit}>
        <TextField
          onChange={handleInput}
          variant="outlined"
          margin="normal"
          required
          fullWidth
          id="email"
          label="Email Address"
          name="email"
          autoComplete="email"
          autoFocus
        />
        <TextField
          onChange={handleInput}
          variant="outlined"
          margin="normal"
          required
          fullWidth
          name="password"
          label="Password"
          type="password"
          id="password"
          autoComplete="current-password"
        />
        <FormControlLabel
          control={<Checkbox value="remember" color="primary" onChange={handleChange} />}
          label="Remember me"
        />
        <Button type="submit" fullWidth variant="contained" color="primary">
          Sign In
        </Button>
        <Grid container style={{ marginTop: "20px" }}>
          <Grid item xs>
            <Link href="/login" passHref>
              <LinkMaterial variant="body2">Forgot password?</LinkMaterial>
            </Link>
          </Grid>
          <Grid item>
            <Link href="/login" passHref>
              <LinkMaterial variant="body2">Do not have an account? Sign Up</LinkMaterial>
            </Link>
          </Grid>
        </Grid>
      </form>
    </Container>
  );
};

Login.defaultProps = {
  token: "",
};

Login.propTypes = {
  token: PropTypes.string,
};

export default Login;
