import PropTypes from "prop-types";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { useMutation, useQuery } from "@apollo/client";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import { initializeApollo } from "../apollo/client";
import localRequest from "../graphql/localRequest";
import request from "../graphql/request";
import LoaderPage from "../components/general/LoaderPage";
import ViewListPortal from "../components/mainPage/ViewListPortal";
import ViewCreatePortal from "../components/mainPage/ViewCreatePortal";
import constants from "../constants/storage";
import Authentication from "../auth/authentication";
import ErrorHandling from "../errorHandling/ErrorHandling";
import initialFormPortal from "../constants/getInitialFormPortal";

const Index = ({ snackBarOpen }) => {
  const apolloClient = initializeApollo();
  const router = useRouter();
  const { getToken } = Authentication();
  const token = getToken();
  const { getError } = ErrorHandling();
  const [openCreatePortal, setOpenCreatePortal] = useState(false);
  const [disabledSavebtn, setDisabledSavebtn] = useState(true);
  const [form, setForm] = useState(initialFormPortal);

  const [updatePortal] = useMutation(request.UpdatePortal);
  const { data, refetch, loading, error } = useQuery(request.Portals, {
    variables: { input: { limit: 0 } },
    context: { headers: { Authorization: `Bearer ${token}` } },
  });

  useEffect(() => {
    refetch({
      variables: { input: { limit: 0 } },
      context: { headers: { Authorization: `Bearer ${token}` } },
    });
  }, [token]);

  useEffect(() => {
    if (error) {
      const err = getError(error);
      snackBarOpen(`${err}`, constants.placementTopCenter);
    }
  }, [error]);

  const handlerDisableBtn = () => {
    setDisabledSavebtn(form.name === "");
  };

  useEffect(() => {
    handlerDisableBtn();
  }, [form]);

  const choosePortal = (id, name) => {
    apolloClient.cache.writeQuery({
      query: localRequest.ID_PORTAL,
      data: {
        idPortal: id,
        namePortal: name,
      },
    });
    router.push("/[portalName]/publications", `/${name}/publications`);
  };

  const resetState = () => {
    setForm(initialFormPortal);
  };

  const openCreatePortalView = () => {
    setOpenCreatePortal(!openCreatePortal);
  };

  const createPortal = () => {
    resetState();
    openCreatePortalView();
  };

  const deletePortal = () => {};

  const savePortal = () => {
    updatePortal({
      variables: { input: form },
      context: { headers: { Authorization: `Bearer ${token}` } },
    });
    openCreatePortalView();
  };

  const handlerForm = (event) => {
    const { id, value, checked } = event.target;
    if (id === constants.activePortal || id === constants.enableMLS) {
      setForm({ ...form, [id]: checked });
    } else {
      setForm({ ...form, [id]: value });
    }
  };

  const handlerFormElements = () => {};

  const uploadFile = (event) => {
    if (event.target.files && event.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener("load", () =>
        setForm({
          ...form,
          logo: { src: reader.result, alt: constants.image },
        })
      );
      reader.readAsDataURL(event.target.files[0]);
    }
  };

  const saveData = (dataPortal) => {
    Object.keys(form).forEach((item) => {
      if (item === "logo") {
        setForm((form2) => ({ ...form2, logo: { src: dataPortal.logo.src, alt: "" } }));
      } else if (dataPortal[item]) {
        setForm((form2) => ({ ...form2, [item]: dataPortal[item] }));
      }
    });
    openCreatePortalView();
  };

  const filterArray = (array, item) => array.filter((el) => el.portalId === item);

  const editPortal = (id) => {
    const currentPortal = filterArray(data && data.Portals.portals, id)[0];
    saveData(currentPortal);
  };

  return !loading ? (
    <Container component="main" maxWidth="md" style={{ paddingTop: "5%" }}>
      <Typography component="h1" variant="h4" style={{ marginBottom: "50px" }}>
        Goliath admin
      </Typography>
      {openCreatePortal ? (
        <ViewCreatePortal
          cancelCreate={createPortal}
          deletePortal={deletePortal}
          disabledSavebtn={disabledSavebtn}
          savePortal={savePortal}
          form={form}
          handlerForm={handlerForm}
          uploadFile={uploadFile}
          handlerFormElements={handlerFormElements}
        />
      ) : (
        <ViewListPortal
          data={data && data.Portals.portals}
          choosePortal={choosePortal}
          openCreatePortalView={createPortal}
          editPortal={editPortal}
        />
      )}
    </Container>
  ) : (
    <LoaderPage />
  );
};

Index.defaultProps = {
  snackBarOpen: () => {},
};

Index.propTypes = {
  snackBarOpen: PropTypes.func,
};

export default Index;
