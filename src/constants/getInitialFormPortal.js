const getInitialFormPortal = {
  portalId: "",
  name: "",
  logo: {
    alt: "",
    src: "",
  },
  domain: "",
  priceAdjustments: [],
  userRoles: [],
  customerServiceEmail: "",
  actions: [],
  enableMLS: false,
  activePortal: false,
};

export default getInitialFormPortal;
