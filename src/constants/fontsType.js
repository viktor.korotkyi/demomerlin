const fontsType = {
  headerOne: "header-one",
  headerTwo: "header-two",
  headerThree: "header-three",
  headerFour: "header-four",
  headerFive: "header-five",
  headerSix: "header-six",
};

export default fontsType;
