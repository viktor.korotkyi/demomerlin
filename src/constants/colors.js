const colors = {
  primary: "primary",
  secondary: "secondary",
  default: "default",
  green: "#64dd17",
};

export default colors;
