const listInputs = [
  {
    name: "portalId",
    label: "Enter portal ID",
    type: "text",
  },
  {
    name: "name",
    label: "Enter portal name",
    type: "text",
  },
  {
    name: "logo",
    label: "Upload Image",
    type: "image",
  },
  {
    name: "domain",
    label: "Enter portal domain",
    type: "text",
  },
  {
    name: "priceAdjustments",
    label: "Price Adjustments",
    type: "boxInputs",
    elements: [
      {
        name: "id",
        label: "Enter price ID",
        type: "text",
      },
      {
        name: "name",
        label: "Enter price name",
        type: "text",
      },
      {
        name: "price",
        label: "Enter price cost",
        type: "number",
      },
      {
        name: "frequency",
        label: "Choose frequency",
        type: "selector",
      },
    ],
  },
  {
    name: "useRoles",
    label: "Choose role",
    type: "boxInputs",
    elements: [
      {
        name: "id",
        label: "Enter user ID",
        type: "text",
      },
      {
        name: "name",
        label: "Enter user name",
        type: "text",
      },
      {
        name: "proofingReview",
        label: "Select a rule to change",
        type: "check",
      },
    ],
  },
  {
    name: "customerServiceEmail",
    label: "Enter service email",
    type: "text",
  },
  {
    name: "actions",
    label: "Enter action",
    type: "boxInputs",
    elements: [
      {
        name: "when",
        label: "Action when",
        type: "text",
      },
      {
        name: "then",
        label: "Action then",
        type: "text",
      },
    ],
  },
  {
    name: "enableMLS",
    label: "Turn on MLS",
    type: "check",
  },
  {
    name: "activePortal",
    label: "Active portal",
    type: "check",
  },
];

export default listInputs;
