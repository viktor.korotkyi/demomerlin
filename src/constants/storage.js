const storage = {
  storageToken: "tokenUser",
  storageOrder: "orderInfo",
  typePublications: "publications",
  typePackages: "packages",
  typeTemplates: "templates",
  typeCustomers: "customers",
  valueCheckPriceDay: "valueCheckPriceDay",
  deletePublication: "Delete publication",
  savePublication: "Save publication",
  placementTopCenter: { vertical: "top", horizontal: "center" },
  wrapperId: "editor",
  elements: "elements",
  underline: "UNDERLINE",
  bold: "BOLD",
  italic: "ITALIC",
  marginTop: "marginTop",
  marginRight: "marginRight",
  marginLeft: "marginLeft",
  marginBottom: "marginBottom",
  minSize: "100",
  unitPx: "px",
  defaultMax: "1450",
  defaultMin: "50",
  widthBlock: "widthBlock",
  heightBlock: "heightBlock",
  image: "image",
  photoImage: "photoImage",
  width: "1450",
  height: "500",
  newTemplates: "new-template",
  createPortal: "New portal",
  boxInputs: "boxInputs",
  check: "check",
  text: "text",
  number: "number",
  src: "src",
  activePortal: "activePortal",
  enableMLS: "enableMLS",
  edit: "edit",
  portalId: "portalId",
  name: "name",
  typename: "__typename",
};

export default storage;
