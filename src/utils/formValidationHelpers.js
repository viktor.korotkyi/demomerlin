const isOnlyNumberIn = (text) => {
  const [zeroCharValue, nineCharValue] = [48, 57];
  for (let i = 0; i < text.length; i += 1) {
    const currentSymbol = text.charCodeAt(i);
    if (currentSymbol < zeroCharValue || currentSymbol > nineCharValue) {
      return false;
    }
  }

  return true;
};

export default isOnlyNumberIn;
