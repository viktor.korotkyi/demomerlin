const packagesList = [
  {
    id: "the-mercury-news",
    value: "The Mercury News",
    label: "The Mercury News",
  },
  {
    id: "hills-newspapers-weekly",
    value: "Hills Newspapers (weekly)",
    label: "Hills Newspapers (weekly)",
  },
  {
    id: "montclarion-package-weekly",
    value: "Montclarion Package (weekly)",
    label: "Montclarion Package (weekly)",
  },
  {
    id: "new-montclarion",
    value: "NewMontclarion",
    label: "NewMontclarion",
  },
];

export default packagesList;
