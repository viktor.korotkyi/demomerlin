const templateSize = [
  {
    idTemplate: "template1",
    width: "300",
    height: "400",
    type: "Template 300x400",
  },
  {
    idTemplate: "template2",
    width: "500",
    height: "400",
    type: "Template 500x400",
  },
];

export default templateSize;
