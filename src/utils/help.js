const portalsAray = [
  {
    id: "id1230",
    name: "Northern California",
    logo: "/static/images/norcal.png",
  },
  {
    id: "id1240",
    name: "Southern California",
    logo: "/static/images/usc.png",
  },
  {
    id: "id1250",
    name: "Denver",
    logo: "/static/images/denver-logo.png",
  },
];

export default portalsAray;
