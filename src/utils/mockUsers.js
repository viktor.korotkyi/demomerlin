const users = [
  {
    id: "1",
    firstName: "Alex Bragin22",
    userType: "Admin",
  },
  {
    id: "2",
    firstName: "Alex Bragin33",
    userType: "Customer",
  },
  {
    id: "3",
    firstName: "Alex Bragin1",
    userType: "Customer",
  },
  {
    id: "4",
    firstName: "Alex Braginzz",
    userType: "Customer",
  },
  {
    id: "5",
    firstName: "Alex Bragin44",
    userType: "Customer",
  },
  {
    id: "6",
    firstName: "Alex Bragin123",
    userType: "Admin",
  },
  {
    id: "7",
    firstName: "Alex Bragin231",
    userType: "Customer",
  },
];

export default users;
