const listTemplate = [
  {
    id: "template1",
    value: "template1",
    label: "template1",
  },
  {
    id: "template2",
    value: "template2",
    label: "template2",
  },
  {
    id: "template3",
    value: "template3",
    label: "template3",
  },
  {
    id: "template4",
    value: "template4",
    label: "template4",
  },
];

export default listTemplate;
