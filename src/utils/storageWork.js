import constants from "../constants/editor";

export const filterSearchItem = (array, searchName) => {
  const newArray = array.filter((el) => {
    const name = el.templateId ? el.templateId.toLowerCase() : el.templateId;
    return name ? name.indexOf(searchName.toLowerCase()) !== -1 : null;
  });
  return newArray.sort((a, b) => {
    const nameA = a.templateId.toLowerCase();
    const nameB = b.templateId.toLowerCase();
    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }
    return 0;
  });
};

export const createDaysObj = (dataType) => {
  const newObject = {};
  if (dataType) {
    dataType.forEach((item) => {
      newObject[item] = false;
    });
  }
  return newObject;
};

export const createSetupItems = (dataType) => {
  const newObject = {};
  if (dataType) {
    dataType.forEach((item) => {
      newObject[item] = {
        cost: "",
        time: "",
      };
    });
  }
  return newObject;
};

export const setToLocalStorage = (key, data) => {
  window.localStorage.setItem(key, data);
};

export const getFromLocalStorage = (key) => {
  return window.localStorage.getItem(key);
};

export const getEmptyFormDataBool = (formData) => {
  const emptyData = {};
  Object.keys(formData).forEach((key) => {
    emptyData[key] = false;
  });
  return emptyData;
};

export const days = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"];

export const randomId = () => Math.floor(Math.random() * (100 - 1 + 1)) + 1;

export const filterArray = (data, item) => data.filter((el) => el.type === item);

export const sliceArray = (start, end, array) => {
  return array.slice(start, end + 1);
};

export const convertMargin = (data) => {
  switch (data) {
    case constants.marginTopCss:
      return constants.marginTop;
    case constants.marginRightCss:
      return constants.marginRight;
    case constants.marginLeftCss:
      return constants.marginLeft;
    case constants.marginBottomCss:
      return constants.marginBottom;
    default:
      return null;
  }
};

export const getDataAttributes = (data, item, typeAttribute) =>
  data.children[`.${item.className}`] &&
  data.children[`.${item.className}`].attributes[typeAttribute];

export const splitStyleSize = (data) => (data && data.length > 0 ? data.split("px")[0] : "0");
