import { useRouter } from "next/router";
import { useMutation, useApolloClient, useQuery } from "@apollo/client";
import { getFromLocalStorage, setToLocalStorage } from "../utils/storageWork";
import localRequest from "../graphql/localRequest";
import request from "../graphql/request";
import constants from "../constants/storage";

const Authentication = () => {
  const client = useApolloClient();
  const router = useRouter();
  const [loginUserMutation] = useMutation(request.login, {
    update(cache, { data }) {
      const tokenUser = {
        token: data.login.auth.token,
        refreshToken: data.login.auth.refreshToken,
      };
      setToLocalStorage(constants.storageToken, JSON.stringify(tokenUser));
      cache.writeQuery({
        query: localRequest.IS_LOGGED_IN,
        data: { token: data.login.auth.token, refreshToken: data.login.auth.refreshToken },
      });
      router.push("/");
    },
  });

  const loginUser = (dataUser) => {
    loginUserMutation({ variables: { input: dataUser } });
  };

  const logout = () => {
    localStorage.removeItem(constants.storageToken);
    localStorage.removeItem(constants.storageOrder);
    client.writeQuery({
      query: localRequest.IS_LOGGED_IN,
      data: { token: "", refreshToken: "" },
    });
    router.push("/login");
  };

  const refresh = () => {
    let newToken;
    if (typeof window !== "undefined") {
      newToken = JSON.parse(getFromLocalStorage(constants.storageToken));
    }
    if (newToken !== null) {
      client.writeQuery({
        query: localRequest.IS_LOGGED_IN,
        data: { token: newToken.token, refreshToken: newToken.refreshToken },
      });
    } else {
      router.push("/login");
    }
  };

  const getToken = () => {
    const getTokens = useQuery(localRequest.IS_LOGGED_IN);
    const token = getTokens.data ? getTokens.data.token : "";
    return token;
  };

  return {
    loginUser,
    logout,
    refresh,
    getToken,
  };
};

export default Authentication;
