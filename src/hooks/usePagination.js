import { useState } from "react";

const usePagination = () => {
  const [paginationIndex, setPaginationIndex] = useState(0);

  const handlePagination = (event, page) => {
    setPaginationIndex(Number(page));
  };

  return [paginationIndex, handlePagination];
};

export default usePagination;
