import { useState } from "react";

const useModal = () => {
  const [modalState, setModalState] = useState(false);

  const handleModalState = () => {
    setModalState(!modalState);
  };

  return [modalState, handleModalState];
};

export default useModal;
