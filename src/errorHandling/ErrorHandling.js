const ErrorHandling = () => {
  const getError = (error) => {
    let dataError = "";
    if (error.graphQLErrors.length > 0) {
      const { messageError } = error.graphQLErrors.map(({ message }) => message);
      dataError = messageError;
    } else if (error.networkError) {
      dataError = error.networkError.result.errors[0].message;
    }
    return dataError;
  };
  return { getError };
};

export default ErrorHandling;
