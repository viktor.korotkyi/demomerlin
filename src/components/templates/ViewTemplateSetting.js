import PropTypes from "prop-types";
import ButtonBoxCreate from "../general/ButtonBoxCreate";
import styles from "../../../scss/components/templates/ViewTemplateSetting.module.scss";
import ViewEditor from "./ViewEditor";
import ViewCodeEditor from "./ViewCodeEditor";

const ViewTemplateSetting = ({
  cancelBtn,
  deleteBtn,
  disabled,
  saveBtn,
  dataTemplate,
  handlerDataTemplate,
  openCodeEditor,
  handleSelect,
  listTemplates,
  openCode,
  editorState,
  onChange,
  sizeTemplate,
  handlerTemplateCodeEditor,
  dataTemplateCodeEditor,
  portalId,
}) => (
  <div className={styles.container}>
    <div className={styles.header}>
      <h3>Template edit</h3>
      <ButtonBoxCreate
        cancelBtn={cancelBtn}
        deleteBtn={deleteBtn}
        disabled={disabled}
        saveBtn={saveBtn}
        widthContainer="45%"
      />
    </div>
    {openCode ? (
      <ViewCodeEditor
        openCodeEditor={openCodeEditor}
        handlerTemplateCodeEditor={handlerTemplateCodeEditor}
        dataTemplateCodeEditor={dataTemplateCodeEditor}
        portalId={portalId}
      />
    ) : (
      <ViewEditor
        openCodeEditor={openCodeEditor}
        handleSelect={handleSelect}
        listTemplates={listTemplates}
        handlerDataTemplate={handlerDataTemplate}
        dataTemplate={dataTemplate}
        editorState={editorState}
        onChange={onChange}
        sizeTemplate={sizeTemplate}
      />
    )}
  </div>
);

ViewTemplateSetting.defaultProps = {
  cancelBtn: () => {},
  deleteBtn: () => {},
  saveBtn: () => {},
  handlerDataTemplate: () => {},
  openCodeEditor: () => {},
  handleSelect: () => {},
  dataTemplate: {},
  disabled: false,
  openCode: false,
  listTemplates: [],
  editorState: {},
  sizeTemplate: {},
  codeEditor: {},
  dataTemplateCodeEditor: {},
  onChange: () => {},
  handlerTemplateCodeEditor: () => {},
  portalId: "",
};

ViewTemplateSetting.propTypes = {
  cancelBtn: PropTypes.func,
  deleteBtn: PropTypes.func,
  saveBtn: PropTypes.func,
  handlerDataTemplate: PropTypes.func,
  openCodeEditor: PropTypes.func,
  handleSelect: PropTypes.func,
  dataTemplate: PropTypes.shape({
    width: PropTypes.string,
    height: PropTypes.string,
  }),
  disabled: PropTypes.bool,
  openCode: PropTypes.bool,
  listTemplates: PropTypes.arrayOf(PropTypes.string),
  editorState: PropTypes.shape({}),
  onChange: PropTypes.func,
  handlerTemplateCodeEditor: PropTypes.func,
  sizeTemplate: PropTypes.shape({}),
  codeEditor: PropTypes.shape({}),
  dataTemplateCodeEditor: PropTypes.shape({}),
  portalId: PropTypes.string,
};
export default ViewTemplateSetting;
