import PropTypes from "prop-types";
import ButtonColor from "../UI/ButtonColor";
import constants from "../../constants/template";
import colors from "../../constants/colors";
import styles from "../../../scss/components/templates/ViewCodeEditor.module.scss";
import CodeEditor from "../codeEditor/CodeEditor";

const ViewCodeEditor = ({
  openCodeEditor,
  handlerTemplateCodeEditor,
  dataTemplateCodeEditor,
  portalId,
}) => (
  <div className={styles.container}>
    <ButtonColor click={openCodeEditor} label={constants.close} color={colors.primary} />
    <CodeEditor
      handlerTemplateCodeEditor={handlerTemplateCodeEditor}
      dataTemplateCodeEditor={dataTemplateCodeEditor}
      portalId={portalId}
    />
  </div>
);

ViewCodeEditor.defaultProps = {
  openCodeEditor: () => {},
  handlerTemplateCodeEditor: () => {},
  dataTemplateCodeEditor: {},
  portalId: "",
};

ViewCodeEditor.propTypes = {
  openCodeEditor: PropTypes.func,
  handlerTemplateCodeEditor: PropTypes.func,
  dataTemplateCodeEditor: PropTypes.shape({}),
  portalId: PropTypes.string,
};
export default ViewCodeEditor;
