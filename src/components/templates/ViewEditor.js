import PropTypes from "prop-types";
import Editor from "../editorPage/Editor";
import ViewContent from "../general/ViewContent";
import OpenCodeEditor from "./OpenCodeEditor";
import styles from "../../../scss/components/templates/ViewEditor.module.scss";

const ViewEditor = ({
  openCodeEditor,
  handleSelect,
  listTemplates,
  handlerDataTemplate,
  dataTemplate,
  editorState,
  onChange,
  sizeTemplate,
}) => (
  <div className={styles.container}>
    <OpenCodeEditor
      openCodeEditor={openCodeEditor}
      handleSelect={handleSelect}
      list={listTemplates}
    />

    <div className={styles.editorContainer}>
      <Editor
        handlerDataTemplate={handlerDataTemplate}
        widthBlock={dataTemplate.width}
        heightBlock={dataTemplate.height}
        editorState={editorState}
        onChange={onChange}
        sizeTemplate={sizeTemplate}
      />
      <ViewContent content={dataTemplate} />
    </div>
  </div>
);

ViewEditor.defaultProps = {
  handlerDataTemplate: () => {},
  openCodeEditor: () => {},
  handleSelect: () => {},
  dataTemplate: {},
  listTemplates: [],
  editorState: {},
  onChange: () => {},
  sizeTemplate: {},
};

ViewEditor.propTypes = {
  handlerDataTemplate: PropTypes.func,
  openCodeEditor: PropTypes.func,
  handleSelect: PropTypes.func,
  dataTemplate: PropTypes.shape({
    width: PropTypes.string,
    height: PropTypes.string,
  }),
  listTemplates: PropTypes.arrayOf(PropTypes.string),
  editorState: PropTypes.shape({}),
  onChange: PropTypes.func,
  sizeTemplate: PropTypes.shape({}),
};

export default ViewEditor;
