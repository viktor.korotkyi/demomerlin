import PropTypes from "prop-types";
import ButtonColor from "../UI/ButtonColor";
import constants from "../../constants/template";
import colors from "../../constants/colors";
import styles from "../../../scss/components/templates/OpenCodeEditor.module.scss";
import SelectList from "../UI/SelectList";

const OpenCodeEditor = ({ openCodeEditor, handleSelect, list }) => (
  <div className={styles.container}>
    <SelectList
      id={constants.choosed}
      handleSelect={handleSelect}
      label={constants.chooseTemplate}
      list={list}
      name={constants.chooseTemplate}
    />
    <div className={styles.btnBox}>
      <ButtonColor click={openCodeEditor} label={constants.openCodeEditor} color={colors.primary} />
    </div>
  </div>
);

OpenCodeEditor.defaultProps = {
  openCodeEditor: () => {},
  handleSelect: () => {},
  list: [],
};

OpenCodeEditor.propTypes = {
  openCodeEditor: PropTypes.func,
  handleSelect: PropTypes.func,
  list: PropTypes.arrayOf(PropTypes.string),
};

export default OpenCodeEditor;
