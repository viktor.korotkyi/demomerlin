import PropTypes from "prop-types";
import React, { useState, useEffect, createRef } from "react";
import { Controlled as CodeMirror } from "react-codemirror2";
import Pusher from "pusher-js";
import pushid from "pushid";
import { toJSON } from "cssjson";
import styles from "../../../scss/components/codeEditor/CodeEditor.module.scss";
import codeTemplate from "./CodeTemlate";
import constants from "../../constants/editor";
import {
  sliceArray,
  convertMargin,
  getDataAttributes,
  splitStyleSize,
} from "../../utils/storageWork";

import "codemirror/lib/codemirror.css";
import "codemirror/theme/material.css";

const CodeEditor = ({ handlerTemplateCodeEditor, dataTemplateCodeEditor, portalId }) => {
  const iframe = createRef(null);
  const codeMirrorOptions = {
    theme: "material",
    lineNumbers: true,
    scrollbarStyle: null,
    lineWrapping: true,
  };
  const pusher = new Pusher(process.env.pusherKey, {
    cluster: "eu",
    forceTLS: true,
  });
  const channel = pusher.subscribe("editor");
  const [id, setId] = useState("");
  const [html, setHtml] = useState("");
  const [css, setCss] = useState("");
  const [newDataTemplate, setNewDataTemplate] = useState({});

  useEffect(() => {
    if (dataTemplateCodeEditor.code !== undefined) {
      setCss(dataTemplateCodeEditor.code.cssCode);
    }
    if (dataTemplateCodeEditor.code !== undefined) {
      setHtml(dataTemplateCodeEditor.code.htmlCode);
    }
  }, [dataTemplateCodeEditor]);

  useEffect(() => {
    handlerTemplateCodeEditor(newDataTemplate);
  }, [newDataTemplate]);

  useEffect(() => {
    setId(pushid());
    channel.bind("text-update", (data) => {
      if (data.id === id) return;
      setHtml(data.html);
      setCss(data.css);
    });
  }, []);

  const runCode = () => {
    const { current } = iframe;
    const document = current.contentDocument;
    const documentContents = codeTemplate(html, css);
    document.open();
    document.write(documentContents);
    document.close();
  };

  const getMargin = (objListStyles, className) => {
    const marginsList = constants.defaultMargin;
    if (objListStyles.children[`.${className}`]) {
      Object.keys(objListStyles.children[`.${className}`].attributes).forEach((el) => {
        const index = marginsList.findIndex((item) => item.type === convertMargin(el));
        marginsList[index] = {
          type: convertMargin(el),
          value: objListStyles.children[`.${className}`].attributes[el],
        };
      });
    }
    return marginsList;
  };

  const checkMargin = (data, className, typeMargin) => {
    return data.children[`.${className}`].attributes[typeMargin] || 0;
  };

  const getItemVerticalMargins = (objListStyles, el) => {
    if (objListStyles.children[`.${el.className}`]) {
      const top = checkMargin(objListStyles, el.className, constants.marginTopCss)
        ? Number(
            objListStyles.children[`.${el.className}`].attributes[constants.marginTopCss].split(
              "px"
            )[0]
          )
        : 0;
      const bottom = checkMargin(objListStyles, el.className, constants.marginBottomCss)
        ? Number(
            objListStyles.children[`.${el.className}`].attributes[constants.marginBottomCss].split(
              "px"
            )[0]
          )
        : 0;
      return top + bottom;
    }
    return 0;
  };

  const getSumAllHightItem = (arrayItem, objListStyles) =>
    arrayItem.map((el) => {
      if (el.tagName === constants.img) {
        return objListStyles.children[`.${el.className}`]
          ? Number(objListStyles.children[`.${el.className}`].attributes.height.split("px")[0])
          : 0;
      }
      return objListStyles.children[`.${el.className}`]
        ? Number(
            objListStyles.children[`.${el.className}`].attributes[constants.fontSizeCss].split(
              "px"
            )[0]
          )
        : 0;
    });

  const hightItemStarted = (idItem, arrayInputs, objListStyles) => {
    const sizeItem = objListStyles.children[`.${arrayInputs[idItem].className}`];

    if (arrayInputs[idItem].className === constants.img) {
      return sizeItem ? sizeItem.attributes.height.split("px")[0] : 0;
    }
    return sizeItem ? Number(sizeItem.attributes[constants.fontSizeCss]?.split("px")[0]) : 0;
  };

  const getCoordinates = (arrayInputs, objListStyles, idItem) => {
    const array = sliceArray(0, idItem, arrayInputs);
    const arrayMargins = array.map((el) => getItemVerticalMargins(objListStyles, el));
    const sumMArgins =
      arrayMargins.length > 0 ? arrayMargins.reduce((total, amount) => total + amount) : 0;

    const arrayHightAllItems = getSumAllHightItem(array, objListStyles).reduce(
      (total, amount) => total + amount
    );
    const sizeRemovedItem = hightItemStarted(idItem, arrayInputs, objListStyles);
    return sumMArgins + arrayHightAllItems - sizeRemovedItem;
  };

  const createElements = (arrayInputs, documentData, objListStyles) => {
    const listElements = arrayInputs.map((item, index) => {
      if (item.tagName === "img") {
        const src = documentData.getElementById(`${item.id}`).getAttribute("src");
        const width = getDataAttributes(objListStyles, item, "width");
        const height = getDataAttributes(objListStyles, item, "height");

        return {
          id: [item.tagName, index].join("."),
          type: "image",
          y: getCoordinates(arrayInputs, objListStyles, index),
          x: Number(splitStyleSize(getMargin(objListStyles, item.className)[2].value)),
          url: src,
          width: Number(splitStyleSize(width)),
          height: Number(splitStyleSize(height)),
          alignment: getDataAttributes(objListStyles, item, "align-self"),
          margin: getMargin(objListStyles, item.className),
          z: item.getAttribute("alt") === "photo",
        };
      }
      const fontSize = getDataAttributes(objListStyles, item, `${constants.fontSizeCss}`);
      return {
        id: [item.tagName, index].join("."),
        type: documentData.getElementById(`${item.id}`)?.getAttribute("description-field"),
        y: getCoordinates(arrayInputs, objListStyles, index),
        x: 0,
        inlineStyleRanges: [],
        fontFamily: getDataAttributes(objListStyles, item, `${constants.fontFamilyCss}`),
        fontSize: Number(splitStyleSize(fontSize)),
        textAlign: getDataAttributes(objListStyles, item, "align-self"),
        textDecoration: getDataAttributes(objListStyles, item, `${constants.textDecorationCss}`),
        fontWeight: getDataAttributes(objListStyles, item, `${constants.fontWeightCss}`),
        fontStyle: getDataAttributes(objListStyles, item, `${constants.fontStyleCss}`),
        text: item.innerHTML,
        margin: getMargin(objListStyles, item.className),
        date: item.className === "date",
        fixedText: documentData.getElementById(`${item.id}`).getAttribute("data-fixed") || false,
      };
    });
    return listElements;
  };

  const createDataTemplate = () => {
    const newTemplate = {
      templateId: "",
      portalId: "",
      width: "",
      height: "",
      elements: [],
    };
    const parser = new DOMParser();
    const documentData = parser.parseFromString(html, "application/xml");
    const objListStyles = toJSON(css);
    const wrapperItem = documentData.querySelector(".container-template");
    const arrayInputs = [];
    if (wrapperItem) {
      const count = wrapperItem.id.split("container")[1];
      for (let i = 1; i <= Number(count); i += 1) {
        const input = documentData.querySelector(`#input${i}`);
        arrayInputs.push(input);
      }
      if (objListStyles.children[`.${wrapperItem.className}`]) {
        const { width, height } = objListStyles.children[`.${wrapperItem.className}`].attributes;
        newTemplate.templateId = dataTemplateCodeEditor.templateId
          ? dataTemplateCodeEditor.templateId
          : "";
        newTemplate.portalId = portalId;
        newTemplate.width = Number(splitStyleSize(width));
        newTemplate.height = Number(splitStyleSize(height));
        newTemplate.backgroundImage =
          objListStyles.children[`.${wrapperItem.className}`].attributes.background;
        newTemplate.typeEditor = constants.typeHtmlCss;
        newTemplate.code = {
          htmlCode: html,
          cssCode: css,
        };
      }

      if (arrayInputs) {
        const listElements = createElements(arrayInputs, documentData, objListStyles);
        newTemplate.elements = listElements;
      }
    }
    return newTemplate;
  };

  useEffect(() => {
    runCode();
    const dataTemplate = createDataTemplate();
    setNewDataTemplate(dataTemplate);
  }, [css, html]);

  return (
    <div className={styles.editorContainer}>
      <section className={styles.playground}>
        <div className={styles.codeEditor}>
          <div className={styles.editorHeader}>HTML</div>
          <CodeMirror
            value={html}
            options={{
              mode: "htmlmixed",
              ...codeMirrorOptions,
            }}
            onBeforeChange={(editor, data, htmlData) => {
              setHtml(htmlData);
            }}
          />
        </div>
        <div className={styles.codeEditor}>
          <div className={styles.editorHeader}>CSS</div>
          <CodeMirror
            value={css}
            options={{
              mode: "css",
              ...codeMirrorOptions,
            }}
            onBeforeChange={(editor, data, cssData) => {
              setCss(cssData);
            }}
          />
        </div>
      </section>
      <section className={styles.result}>
        <iframe title="result" className={styles.iframe} ref={iframe} />
      </section>
    </div>
  );
};

CodeEditor.defaultProps = {
  handlerTemplateCodeEditor: () => {},
  dataTemplateCodeEditor: {},
  portalId: "",
};

CodeEditor.propTypes = {
  handlerTemplateCodeEditor: PropTypes.func,
  dataTemplateCodeEditor: PropTypes.shape({
    templateId: PropTypes.string,
    code: PropTypes.shape({
      cssCode: PropTypes.string,
      htmlCode: PropTypes.string,
    }),
  }),
  portalId: PropTypes.string,
};

export default CodeEditor;
