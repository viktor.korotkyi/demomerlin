import { useState } from "react";
import { Button, makeStyles, Paper, Typography } from "@material-ui/core";
import styles from "../../../scss/components/PricingAdjustmentPage/PricingAdjustmentPage.module.scss";
import EditList from "../general/EditList";
import ButtonGroupForm from "../general/ButtonGroupForm";

const userRoles = [
  {
    id: 1,
    name: "Franklins Funeral Home",
    userType: "Funeral Home",
    canSkipProofingReview: true,
  },
  {
    id: 2,
    name: "Franklins11 Funeral Home",
    userType: "Funeral Home",
    canSkipProofingReview: false,
  },
  {
    id: 3,
    name: "Franklins22 Funeral Home",
    userType: "Funeral Home",
    canSkipProofingReview: true,
  },
];

const useStyles = makeStyles((theme) => ({
  priceButton: {
    marginTop: theme.spacing(3),
  },
}));

const UserRoles = () => {
  const classes = useStyles();
  const [newRoles, setNewRoles] = useState(userRoles);

  const createNewRole = () => {
    setNewRoles((prevUsers) => [
      ...prevUsers,
      { id: prevUsers.length + 1, name: "", canSkipProofingReview: false },
    ]);
  };

  return (
    <Paper elevation={0} className={styles.container}>
      <Typography variant="h5">User roles</Typography>
      <EditList label="Role name" items={userRoles} />
      <ButtonGroupForm />
      <Button variant="contained" className={classes.priceButton} onClick={createNewRole}>
        Add user role
      </Button>
      <EditList label="Role name" items={newRoles} />
    </Paper>
  );
};

export default UserRoles;
