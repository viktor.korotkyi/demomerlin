import PropTypes from "prop-types";
import ButtonBoxCreate from "../general/ButtonBoxCreate";
import styles from "../../../scss/components/mainPage/ViewCreatePortal.module.scss";
import FormCreatePortal from "./FormCreatePortal";
import listInputs from "../../constants/ListInputsCreatePortal";

const ViewCreatePortal = ({
  cancelCreate,
  savePortal,
  deletePortal,
  disabledSavebtn,
  form,
  handlerForm,
  uploadFile,
  handlerFormElements,
}) => (
  <div className={styles.container}>
    <ButtonBoxCreate
      cancelBtn={cancelCreate}
      deleteBtn={deletePortal}
      saveBtn={savePortal}
      disabled={disabledSavebtn}
    />
    <FormCreatePortal
      listInputs={listInputs}
      form={form}
      handlerForm={handlerForm}
      uploadFile={uploadFile}
      handlerFormElements={handlerFormElements}
    />
  </div>
);

ViewCreatePortal.defaultProps = {
  cancelCreate: () => {},
  savePortal: () => {},
  deletePortal: () => {},
  handlerForm: () => {},
  uploadFile: () => {},
  handlerFormElements: () => {},
  disabledSavebtn: false,
  form: {},
};

ViewCreatePortal.propTypes = {
  cancelCreate: PropTypes.func,
  savePortal: PropTypes.func,
  deletePortal: PropTypes.func,
  handlerForm: PropTypes.func,
  uploadFile: PropTypes.func,
  handlerFormElements: PropTypes.func,
  disabledSavebtn: PropTypes.bool,
  form: PropTypes.shape({}),
};

export default ViewCreatePortal;
