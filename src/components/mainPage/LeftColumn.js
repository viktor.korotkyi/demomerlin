import PropTypes from "prop-types";
import constants from "../../constants/storage";
import staticFiles from "../../constants/staticFiles";
import ButtonUpload from "../UI/ButtonUpload";
import PriceBoxAdjustments from "./PriceBoxAdjustments";
import TextField from "../UI/TextField";
import styles from "../../../scss/components/mainPage/LeftColumn.module.scss";

const LeftColumn = ({ listInputs, form, handlerFormElements, uploadFile, handlerForm }) => (
  <div className={styles.leftColumn}>
    {listInputs.slice(0, 5).map((item, index) => {
      if (item.type === constants.boxInputs) {
        return (
          <PriceBoxAdjustments
            form={form}
            handlerFormElements={handlerFormElements}
            item={item}
            key={[index, item.id]}
          />
        );
      }
      if (item.type === constants.image) {
        return (
          <ButtonUpload
            key={[index, item.id]}
            onClick={uploadFile}
            id={constants.src}
            image={form.logo.src}
            imageUpload={staticFiles.imageUpload}
          />
        );
      }
      return (
        <div className={styles.box} key={[item.name, index].join("_")}>
          <TextField
            defaultValue={form[item.name]}
            label={item.label}
            id={item.name}
            onChange={handlerForm}
          />
        </div>
      );
    })}
  </div>
);

LeftColumn.defaultProps = {
  listInputs: [],
  form: {},
  handlerForm: () => {},
  uploadFile: () => {},
  handlerFormElements: () => {},
};

LeftColumn.propTypes = {
  listInputs: PropTypes.arrayOf(PropTypes.shape({})),
  form: PropTypes.shape({
    logo: PropTypes.shape({
      src: PropTypes.string,
    }),
  }),
  handlerForm: PropTypes.func,
  uploadFile: PropTypes.func,
  handlerFormElements: PropTypes.func,
};

export default LeftColumn;
