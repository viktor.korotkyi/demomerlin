import PropTypes from "prop-types";
import styles from "../../../scss/components/mainPage/FormCreatePortal.module.scss";
import LeftColumn from "./LeftColumn";
import RightColumn from "./RightColumn";

const FormCreatePortal = ({ listInputs, form, handlerForm, uploadFile, handlerFormElements }) => (
  <div className={styles.container}>
    <LeftColumn
      form={form}
      handlerForm={handlerForm}
      handlerFormElements={handlerFormElements}
      listInputs={listInputs}
      uploadFile={uploadFile}
    />
    <RightColumn form={form} handlerForm={handlerForm} listInputs={listInputs} />
  </div>
);

FormCreatePortal.defaultProps = {
  listInputs: [],
  form: {},
  handlerForm: () => {},
  uploadFile: () => {},
  handlerFormElements: () => {},
};

FormCreatePortal.propTypes = {
  listInputs: PropTypes.arrayOf(PropTypes.shape({})),
  form: PropTypes.shape({
    logo: PropTypes.shape({
      src: PropTypes.string,
    }),
  }),
  handlerForm: PropTypes.func,
  uploadFile: PropTypes.func,
  handlerFormElements: PropTypes.func,
};

export default FormCreatePortal;
