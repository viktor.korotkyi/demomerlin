import PropTypes from "prop-types";
import TextField from "../UI/TextField";
import constants from "../../constants/storage";
import CheckBox from "../UI/CheckBox";
import styles from "../../../scss/components/mainPage/RightColumn.module.scss";

const RightColumn = ({ listInputs, handlerForm, form }) => (
  <div className={styles.rightColumn}>
    {listInputs.slice(5).map((item, index) => {
      if (item.type === constants.image || item.type === constants.boxInputs) {
        return null;
      }
      if (item.type === constants.check) {
        return (
          <div className={styles.box} key={[item.name, index].join("_")}>
            <CheckBox id={item.name} label={item.label} handleChange={handlerForm} />
          </div>
        );
      }
      return (
        <div className={styles.box} key={[item.name, index].join("_")}>
          <TextField
            defaultValue={form[item.name]}
            label={item.label}
            id={item.name}
            onChange={handlerForm}
          />
        </div>
      );
    })}
  </div>
);

RightColumn.defaultProps = {
  listInputs: [],
  form: {},
  handlerForm: () => {},
};

RightColumn.propTypes = {
  listInputs: PropTypes.arrayOf(PropTypes.shape({})),
  form: PropTypes.shape({
    logo: PropTypes.shape({
      src: PropTypes.string,
    }),
  }),
  handlerForm: PropTypes.func,
};

export default RightColumn;
