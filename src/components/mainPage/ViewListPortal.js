import PropTypes from "prop-types";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import CardImage from "../general/Card";
import ButtonColor from "../UI/ButtonColor";
import constants from "../../constants/storage";
import colors from "../../constants/colors";

const ViewListPortal = ({ data, choosePortal, openCreatePortalView, editPortal }) => (
  <Container component="main" maxWidth="md" style={{ paddingTop: "5%" }}>
    <ButtonColor
      click={openCreatePortalView}
      label={constants.createPortal}
      color={colors.primary}
    />
    <Grid
      container
      spacing={1}
      justify="space-between"
      alignItems="center"
      style={{ paddingTop: 20 }}
    >
      {data.map((item, index) => (
        <Grid item key={[item.name, index].join("_")}>
          <CardImage
            clickCard={choosePortal}
            header={item.name}
            id={item.portalId}
            logo={item.logo.src}
            editPortal={editPortal}
          />
        </Grid>
      ))}
    </Grid>
  </Container>
);

ViewListPortal.defaultProps = {
  data: [],
  choosePortal: () => {},
  openCreatePortalView: () => {},
  editPortal: () => {},
};

ViewListPortal.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape({})),

  choosePortal: PropTypes.func,
  openCreatePortalView: PropTypes.func,
  editPortal: PropTypes.func,
};

export default ViewListPortal;
