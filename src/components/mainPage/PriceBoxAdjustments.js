import PropTypes from "prop-types";
import TextField from "../UI/TextField";
import styles from "../../../scss/components/mainPage/PriceBoxAdjustments.module.scss";

const PriceBoxAdjustments = ({ item, form, handlerFormElements }) => (
  <div className={styles.priceAdjustments}>
    <h3>{item.label}</h3>
    {item.elements.map((el, i) => (
      <TextField
        key={[el.name, i].join("_")}
        defaultValue={form[item.name].elements && form[item.name].elements[i][el.name]}
        label={el.label}
        id={el.name}
        onChange={(event) => handlerFormElements(event, item.name)}
        type={el.type}
      />
    ))}
  </div>
);

PriceBoxAdjustments.defaultProps = {
  item: {},
  form: {},
  handlerFormElements: () => {},
};

PriceBoxAdjustments.propTypes = {
  item: PropTypes.shape({
    name: PropTypes.string,
    label: PropTypes.string,
    elements: PropTypes.arrayOf(PropTypes.shape({})),
  }),
  form: PropTypes.shape({
    logo: PropTypes.shape({
      src: PropTypes.string,
    }),
  }),
  handlerFormElements: PropTypes.func,
};

export default PriceBoxAdjustments;
