import PropTypes from "prop-types";
import { makeStyles, Modal } from "@material-ui/core";
import Navigation from "../../layouts/Navigation";
import styles from "../../../scss/components/setting/ViewPublications.module.scss";
import PanelEditItem from "../settingComponents/PanelEditItem";
import packageList from "../../utils/packagesList";
import PanelSelectedItems from "../settingComponents/PanelSelectedItems";
import listTemplate from "../../utils/listTemplate";
import EditTemplate from "../setting/EditTemplate";
import ButtonBoxCreate from "../general/ButtonBoxCreate";
import SelectTimeZone from "../general/SelectTimeZone";
import AlertDialog from "../UI/AlertDialog";

const useStyles = makeStyles({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
});

const PackageView = ({
  avatar,
  handleName,
  handleChosenValue,
  name,
  handleUploadAvatar,
  chosenValue,
  handleTemplateEdit,
  modalHandler,
  modalState,
  editTemplatePricing,
  handleSetupTemplate,
  handleTemplateCancel,
  handleTemplateUpdate,
  handleDeleteButtonClick,
  disableSave,
  handleSaveButtonClick,
  handleCancelButtonClick,
  selectedTimezone,
  setSelectedTimezone,
  openDialogOnCancel,
  handleDialogOnCancel,
  handleUpdateAlert,
}) => {
  const classes = useStyles();
  return (
    <Navigation>
      <div className={styles.container}>
        <PanelEditItem
          fileUpload={avatar}
          handleInput={handleName}
          handleSelect={handleChosenValue}
          header="package"
          listSelectPackages={packageList}
          name={name}
          uploadFile={handleUploadAvatar}
          selectedItems={chosenValue}
          editChoosedPackages={null}
        >
          <div>
            <h3>Templates</h3>
            <div>
              <PanelSelectedItems edit={handleTemplateEdit} list={listTemplate} />
            </div>
            <Modal open={modalState} onClose={modalHandler} className={classes.modal}>
              <div>
                <EditTemplate
                  editTemplatePricing={editTemplatePricing}
                  handleSetupTemplate={handleSetupTemplate}
                  cancelTemplateEdit={handleTemplateCancel}
                  updateTemplateEdit={handleTemplateUpdate}
                />
              </div>
            </Modal>
          </div>
        </PanelEditItem>
        <div className={styles.setting}>
          <ButtonBoxCreate
            cancelBtn={handleCancelButtonClick}
            deleteBtn={handleDeleteButtonClick}
            disabled={disableSave}
            saveBtn={handleSaveButtonClick}
          />
          <div className={styles.zoneBox}>
            <SelectTimeZone
              selectedTimezone={selectedTimezone}
              setSelectedTimezone={setSelectedTimezone}
            />
          </div>
        </div>
      </div>
      <AlertDialog
        open={openDialogOnCancel}
        handleClose={handleDialogOnCancel}
        title="Edit Element"
        handleUpdateAlert={handleUpdateAlert}
        decription="Do you really want to delete this package?"
      />
    </Navigation>
  );
};

PackageView.defaultProps = {
  avatar: "",
};

PackageView.propTypes = {
  editTemplatePricing: PropTypes.shape({}).isRequired,
  avatar: PropTypes.string,
  name: PropTypes.string.isRequired,
  chosenValue: PropTypes.arrayOf(PropTypes.string).isRequired,
  selectedTimezone: PropTypes.string.isRequired,
  modalState: PropTypes.bool.isRequired,
  disableSave: PropTypes.bool.isRequired,
  openDialogOnCancel: PropTypes.bool.isRequired,
  handleChosenValue: PropTypes.func.isRequired,
  handleName: PropTypes.func.isRequired,
  handleUploadAvatar: PropTypes.func.isRequired,
  handleTemplateEdit: PropTypes.func.isRequired,
  modalHandler: PropTypes.func.isRequired,
  handleSetupTemplate: PropTypes.func.isRequired,
  handleTemplateCancel: PropTypes.func.isRequired,
  handleTemplateUpdate: PropTypes.func.isRequired,
  handleDeleteButtonClick: PropTypes.func.isRequired,
  handleSaveButtonClick: PropTypes.func.isRequired,
  handleCancelButtonClick: PropTypes.func.isRequired,
  setSelectedTimezone: PropTypes.func.isRequired,
  handleDialogOnCancel: PropTypes.func.isRequired,
  handleUpdateAlert: PropTypes.func.isRequired,
};

export default PackageView;
