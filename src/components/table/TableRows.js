import { TableCell, TableRow } from "@material-ui/core";
import PropTypes from "prop-types";
import React from "react";
import ButtonNoBackground from "../UI/ButtonNoBackground";

const TableRows = ({ paginationPage, rowsPerPage, items, edit }) =>
  items
    .slice(paginationPage * rowsPerPage, paginationPage * rowsPerPage + rowsPerPage)
    .map((rowItem, index) => (
      <TableRow key={[rowItem.templateId, index].join("_")} hover>
        <TableCell>{rowItem.templateId}</TableCell>
        <TableCell>{rowItem.date}</TableCell>
        <TableCell>
          <ButtonNoBackground
            click={() => edit(rowItem.templateId)}
            id={rowItem.templateId}
            label="Edit"
          />
        </TableCell>
      </TableRow>
    ));

TableRows.defaultProps = {
  edit: () => {},
};

TableRows.propTypes = {
  paginationPage: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
  items: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  edit: PropTypes.func,
};

export default TableRows;
