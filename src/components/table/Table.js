import React from "react";
import PropTypes from "prop-types";
import { TablePagination } from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import TableContainer from "./TableContainer";
import commonVariables from "../../constants/commonVariables";
import Toolbar from "../general/Toolbar";

const Table = ({
  heading,
  headers,
  children,
  count,
  rowsPerPage,
  page,
  onChangePage,
  onChangeRowsPerPage,
}) => {
  return (
    <Paper>
      <Toolbar>{heading}</Toolbar>
      <TableContainer headers={headers}>{children}</TableContainer>
      <TablePagination
        rowsPerPageOptions={commonVariables.rowsPerPageOptions}
        component="div"
        count={count}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={onChangePage}
        onChangeRowsPerPage={onChangeRowsPerPage}
      />
    </Paper>
  );
};

Table.propTypes = {
  heading: PropTypes.string.isRequired,
  count: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
  page: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  onChangeRowsPerPage: PropTypes.func.isRequired,
  headers: PropTypes.arrayOf(PropTypes.string).isRequired,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]).isRequired,
};

export default Table;
