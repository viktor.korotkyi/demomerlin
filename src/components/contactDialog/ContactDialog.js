import { useEffect, useState } from "react";
import PropTypes from "prop-types";
import {
  FormControlLabel,
  Checkbox,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
  Tooltip,
} from "@material-ui/core";

const tooltipTitle =
  "This feature cannot be turned off after it has been enabled. Please contact Goliath Team if you have any questions";

const ContactDialog = ({ isActive, checkBoxText, modalText }) => {
  const [checked, setChecked] = useState(isActive);
  const [disabled, setDisabled] = useState(false);
  const [openDialog, setOpenDialog] = useState(false);
  const handleChecked = () => {
    setChecked(!checked);
  };

  const handleDialog = () => {
    setOpenDialog(!openDialog);
  };

  const handleCancelDialog = () => {
    setChecked(false);
    setDisabled(false);
    setOpenDialog(false);
  };

  useEffect(() => {
    if (!disabled && checked) {
      setDisabled(true);
    }
  }, [checked]);

  useEffect(() => {
    if (checked) {
      setOpenDialog(true);
    }
  }, [checked]);

  return (
    <div>
      <Tooltip title={tooltipTitle}>
        <FormControlLabel
          control={() => (
            <Checkbox
              checked={checked}
              disabled={disabled}
              onChange={handleChecked}
              name="dialog opener"
              color="primary"
            />
          )}
          label={checkBoxText}
        />
      </Tooltip>
      <Dialog
        open={openDialog}
        onClose={handleCancelDialog}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle>Read this dialog window</DialogTitle>
        <DialogContent>
          <DialogContentText>{modalText}</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCancelDialog} color="primary">
            Disagree
          </Button>
          <Button onClick={handleDialog} color="primary" autoFocus>
            Agree
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

ContactDialog.propTypes = {
  checkBoxText: PropTypes.string.isRequired,
  modalText: PropTypes.string.isRequired,
  isActive: PropTypes.bool.isRequired,
};

export default ContactDialog;
