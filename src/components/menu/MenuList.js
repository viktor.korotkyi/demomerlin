import PropTypes from "prop-types";
import LinkRef from "../UI/LinkRef";
import styles from "../../../scss/components/menu/MenuList.module.scss";

const MenuList = ({ list, path, pathActive }) => (
  <ul className={styles.mainMenu}>
    {list.map((item, index) => (
      <LinkRef
        key={[item.name, index].join("_")}
        href={item.name !== "view all portals" ? `/[portalName]${item.url}` : `${item.url}`}
        asPath={item.name !== "view all portals" ? `/${path}${item.url}` : `${item.url}`}
        pathActive={pathActive}
        pathUrl={`${item.url}`}
        typeStyle="menu"
      >
        <li>{item.name}</li>
      </LinkRef>
    ))}
  </ul>
);

MenuList.defaultProps = {
  list: [],
  path: "",
  pathActive: "",
};

MenuList.propTypes = {
  list: PropTypes.arrayOf(PropTypes.shape({})),
  path: PropTypes.string,
  pathActive: PropTypes.string,
};

export default MenuList;
