import { useEffect } from "react";
import { useRouter } from "next/router";
import { useQuery } from "@apollo/client";
import localRequest from "../../graphql/localRequest";
import list from "./ListUrl";
import secondList from "./SecondList";
import MenuList from "./MenuList";
import styles from "../../../scss/components/menu/Menu.module.scss";

const Menu = () => {
  const router = useRouter();
  const rootPath = router.asPath.split("/")[1];
  const pathActive = router.asPath.split("/")[2];
  const idPortal = useQuery(localRequest.ID_PORTAL);
  useEffect(() => {
    if (!idPortal.data) {
      router.push("/");
    }
  }, []);

  return (
    <div className={styles.container}>
      <h3>{idPortal.data && idPortal.data.namePortal}</h3>
      <MenuList list={list} path={rootPath} pathActive={`/${pathActive}`} />
      <div className={styles.secondMenu}>
        <MenuList list={secondList} path={rootPath} />
      </div>
    </div>
  );
};

export default Menu;
