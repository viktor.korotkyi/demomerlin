const ListUrl = [
  {
    name: "publications",
    url: "/publications",
  },
  {
    name: "packages",
    url: "/packages",
  },
  {
    name: "templates",
    url: "/templates",
  },
  {
    name: "orders",
    url: "/orders",
  },
  {
    name: "customers",
    url: "/customers",
  },
];

export default ListUrl;
