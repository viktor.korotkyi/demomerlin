import PropTypes from "prop-types";
import styles from "../../../scss/components/settingComponents/PublishBox.module.scss";
import PublishDay from "./PublishDays";

const PublishBox = ({ listDays, handlePublishDay, viewSetup, handleSetup, valueSetup }) => (
  <div className={styles.container}>
    {listDays.map((item, index) => (
      <PublishDay
        key={[index, item].join("_")}
        handleChange={handlePublishDay}
        viewSetup={viewSetup[item]}
        label={item}
        handleSetup={handleSetup}
        valueSetup={valueSetup[item]}
      />
    ))}
  </div>
);

PublishBox.defaultProps = {
  listDays: [],
  handlePublishDay: () => {},
  handleSetup: () => {},
  viewSetup: {},
  valueSetup: {},
};

PublishBox.propTypes = {
  listDays: PropTypes.arrayOf(PropTypes.string),
  handlePublishDay: PropTypes.func,
  handleSetup: PropTypes.func,
  viewSetup: PropTypes.shape({}),
  valueSetup: PropTypes.shape({}),
};
export default PublishBox;
