import PropTypes from "prop-types";
import styles from "../../../scss/components/settingComponents/PublishDay.module.scss";
import CheckBox from "../UI/CheckBox";
import TextField from "../UI/TextField";
import TimeField from "../UI/TimeField";

const PublishDay = ({ viewSetup, label, handleChange, handleSetup, valueSetup }) => (
  <div className={styles.container}>
    <div className={styles.main}>
      <span>{label}</span>
      <CheckBox handleChange={handleChange} value={label} id={label} />
    </div>
    {viewSetup && (
      <div className={styles.view}>
        <TimeField id={label} label="Publish Time" onChange={handleSetup} />
        <TextField
          onChange={handleSetup}
          id={label}
          label="Order Placement Cutoff Time"
          defaultValue={valueSetup.cost}
          type="number"
        />
      </div>
    )}
  </div>
);

PublishDay.defaultProps = {
  viewSetup: false,
  label: "",
  valueSetup: "",
  handleChange: () => {},
  handleSetup: () => {},
};

PublishDay.propTypes = {
  viewSetup: PropTypes.bool,
  label: PropTypes.string,
  handleChange: PropTypes.func,
  handleSetup: PropTypes.func,
  valueSetup: PropTypes.shape({
    cost: PropTypes.string,
  }),
};

export default PublishDay;
