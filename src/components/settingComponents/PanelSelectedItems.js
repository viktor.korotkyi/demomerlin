import PropTypes from "prop-types";
import PanelItem from "./PanelItem";
import styles from "../../../scss/components/settingComponents/PanelSelectedPackages.module.scss";

const PanelSelectedItems = ({ list, edit }) => (
  <div className={styles.container}>
    {list.map((item, index) => {
      if (typeof list[0] === "string") {
        return (
          <PanelItem
            key={[index, item].join("_")}
            firstName={item}
            edit={() => edit(item)}
            id={item}
          />
        );
      }
      return (
        <PanelItem
          key={[index, item.label].join("_")}
          firstName={item.label}
          edit={() => edit(item.label)}
          id={item.id}
        />
      );
    })}
  </div>
);

PanelSelectedItems.defaultProps = {
  list: [],
  edit: () => {},
};

PanelSelectedItems.propTypes = {
  list: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.shape({})),
    PropTypes.arrayOf(PropTypes.string),
  ]),
  edit: PropTypes.func,
};

export default PanelSelectedItems;
