import PropTypes from "prop-types";
import TextField from "../UI/TextField";
import styles from "../../../scss/components/settingComponents/PanelDays.module.scss";
import CheckBox from "../UI/CheckBox";

const PanelDays = ({ onChange, id, defaultValue, handleChange }) => (
  <div className={styles.container}>
    <TextField
      id={id}
      label="Publishing minimum days"
      defaultValue={defaultValue}
      onChange={onChange}
      type="number"
    />
    <CheckBox label="Require Sequential Scheduling" handleChange={handleChange} />
  </div>
);

PanelDays.defaultProps = {
  onChange: () => {},
  handleChange: () => {},
  id: "",
  defaultValue: "",
};

PanelDays.propTypes = {
  onChange: PropTypes.func,
  handleChange: PropTypes.func,
  id: PropTypes.string,
  defaultValue: PropTypes.string,
};

export default PanelDays;
