import PropTypes from "prop-types";
import TextInput from "../UI/TextField";
import ButtonUpload from "../UI/ButtonUpload";
import staticFiles from "../../constants/staticFiles";
import styles from "../../../scss/components/settingComponents/PanelEditItem.module.scss";
import PanelSelectedItems from "./PanelSelectedItems";

const PanelEditItem = ({
  header,
  name,
  handleInput,
  uploadFile,
  fileUpload,
  selectedItems,
  editChoosedPackages,
  children,
}) => (
  <div className={styles.info}>
    <h3>{`Edit ${header}`}</h3>
    <TextInput id="name" label="Name" defaultValue={name} onChange={handleInput} />
    <ButtonUpload
      onClick={uploadFile}
      id="logo"
      image={fileUpload}
      imageUpload={staticFiles.imageUpload}
    />
    <div className={styles.select}>{children}</div>
    <PanelSelectedItems edit={editChoosedPackages} list={selectedItems} />
  </div>
);

PanelEditItem.defaultProps = {
  header: "",
  name: "",
  fileUpload: "",
  handleInput: () => {},
  uploadFile: () => {},
  editChoosedPackages: () => {},
  selectedItems: [],
};

PanelEditItem.propTypes = {
  header: PropTypes.string,
  name: PropTypes.string,
  fileUpload: PropTypes.string,
  handleInput: PropTypes.func,
  uploadFile: PropTypes.func,
  editChoosedPackages: PropTypes.func,
  selectedItems: PropTypes.arrayOf(PropTypes.string),
  children: PropTypes.node.isRequired,
};

export default PanelEditItem;
