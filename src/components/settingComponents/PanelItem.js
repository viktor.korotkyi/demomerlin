import PropTypes from "prop-types";
import styles from "../../../scss/components/UI/PanelItem.module.scss";
import ButtonNoBackground from "../UI/ButtonNoBackground";

const PanelItem = ({ firstName, creationDate, edit, id }) => (
  <div className={styles.item}>
    {firstName && <span>{firstName}</span>}
    {creationDate && <span>{creationDate}</span>}
    {!firstName && !creationDate && <span>Empty field</span>}
    <ButtonNoBackground click={edit} id={id} label="Edit" />
  </div>
);
PanelItem.defaultProps = {
  firstName: "",
  creationDate: "",
  id: "",
  edit: () => {},
};

PanelItem.propTypes = {
  firstName: PropTypes.string,
  creationDate: PropTypes.string,
  id: PropTypes.string,
  edit: PropTypes.func,
};

export default PanelItem;
