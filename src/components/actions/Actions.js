import { useState } from "react";
import { Paper, Typography, Button, makeStyles } from "@material-ui/core";
import APIActions from "../../constants/actions";
import EditToggledItem from "../general/EditToggledItem";
import ActionList from "./ActionList";

const useStyles = makeStyles((theme) => ({
  formControl: {
    marginTop: theme.spacing(1),
    minWidth: 120,
  },
  flex: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "flex-start",
    alignItems: "flex-end",
  },
  topMargin: {
    marginTop: theme.spacing(2),
  },
  linkMargin: {
    marginLeft: theme.spacing(3),
  },
}));

const actions = [
  {
    id: 1,
    event: APIActions.SUBMITTED,
    destinationURL: "https://whateverEndpoint.com",
  },
  {
    id: 2,
    event: APIActions.PROOFED,
    destinationURL: "https://whateverEndpoint.com",
  },
  {
    id: 3,
    event: APIActions.PROOFED,
    destinationURL: "https://whateverEndpoint.com",
  },
];

const Actions = () => {
  const classes = useStyles();
  const [actionList, setActionList] = useState(actions);
  const [isButtonClicked, setIsButtonClicked] = useState(false);
  const [isProofToggled, setIsProofToggled] = useState(false);
  const [destinationURLInput, setDestinationURLInput] = useState("");

  const handleChangeSelectValue = (id) => (event) => {
    setActionList((prevActionList) =>
      prevActionList.map((item) => (item.id === id ? { ...item, event: event.target.value } : item))
    );
  };

  const handleDeleteAction = (id) => () =>
    setActionList((prevActionList) => prevActionList.filter((item) => id !== item.id));

  const handleProofToggler = () => {
    setIsProofToggled(!isProofToggled);
  };

  const handleButtonClick = () => {
    setIsButtonClicked(!isButtonClicked);
  };

  const handleDestinationURLInput = (event) => {
    setDestinationURLInput(event.target.value);
  };

  const handleCancelButtonClick = () => {
    handleButtonClick();

    setDestinationURLInput("");
    setIsProofToggled(false);
  };

  const handleSaveButtonClick = () => {
    const event = isProofToggled ? APIActions.PROOFED : APIActions.SUBMITTED;

    setActionList((prevActionList) => [
      ...prevActionList,
      { id: prevActionList.length + 1, event, destinationURL: destinationURLInput },
    ]);

    setDestinationURLInput("");
    setIsProofToggled(false);

    handleButtonClick();
  };

  return (
    <Paper elevation={0}>
      <Typography variant="h5">APIs connections</Typography>
      <ActionList
        handleChangeSelectValue={handleChangeSelectValue}
        actionList={actionList}
        handleDeleteAction={handleDeleteAction}
      />
      {isButtonClicked ? (
        <EditToggledItem
          textFieldDefaultValue={destinationURLInput}
          handleTextField={handleDestinationURLInput}
          handleSwitch={handleProofToggler}
          switchChecked={isProofToggled}
          handleSave={handleSaveButtonClick}
          handleCancel={handleCancelButtonClick}
          saveButtonDisabled={!destinationURLInput.trim()}
          switchLeftTitle="Submit"
          switchRightTitle="Proof"
        />
      ) : (
        <Button onClick={handleButtonClick} className={classes.topMargin}>
          + Add new API
        </Button>
      )}
    </Paper>
  );
};

export default Actions;
