import PropTypes from "prop-types";
import { List, makeStyles, Paper, Typography } from "@material-ui/core";
import ActionItem from "./ActionItem";

const useStyles = makeStyles((theme) => ({
  flex: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "flex-start",
    alignItems: "flex-end",
  },
  topMargin: {
    marginTop: theme.spacing(2),
  },
  linkMargin: {
    marginLeft: theme.spacing(3),
  },
}));

const ActionList = ({ actionList, handleDeleteAction, handleChangeSelectValue }) => {
  const classes = useStyles();

  return (
    <List>
      <Paper elevation={0} className={classes.flex}>
        <Typography variant="subtitle1">Send info on event</Typography>
        <Typography variant="subtitle1" className={`${classes.topMargin} ${classes.linkMargin}`}>
          Destination URL
        </Typography>
      </Paper>
      {actionList.map((actionItem) => (
        <ActionItem
          key={actionItem.id}
          handleChangeSelectValue={handleChangeSelectValue}
          handleDeleteAction={handleDeleteAction}
          actionItem={actionItem}
        />
      ))}
    </List>
  );
};

ActionList.propTypes = {
  actionList: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      event: PropTypes.string,
      destinationURL: PropTypes.string,
    })
  ).isRequired,
  handleDeleteAction: PropTypes.func.isRequired,
  handleChangeSelectValue: PropTypes.func.isRequired,
};

export default ActionList;
