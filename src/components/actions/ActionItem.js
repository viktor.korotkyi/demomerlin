import PropTypes from "prop-types";
import {
  Button,
  FormControl,
  InputLabel,
  makeStyles,
  MenuItem,
  Select,
  ListItem,
} from "@material-ui/core";
import { Delete } from "@material-ui/icons";
import APIActions from "../../constants/actions";
import Link from "../UI/Link";

const useStyles = makeStyles((theme) => ({
  formControl: {
    marginTop: theme.spacing(1),
    minWidth: 120,
  },
  flex: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "flex-start",
    alignItems: "flex-end",
  },
  linkMargin: {
    marginLeft: theme.spacing(3),
  },
}));

const ActionItem = ({ actionItem, handleChangeSelectValue, handleDeleteAction }) => {
  const classes = useStyles();
  return (
    <ListItem className={classes.flex}>
      <FormControl className={classes.formControl}>
        <InputLabel id={`action-api-label-${actionItem.id}`} />
        <Select
          labelId={`action-api-label-${actionItem.id}`}
          value={actionItem.event}
          onChange={handleChangeSelectValue(actionItem.id)}
        >
          <MenuItem value={APIActions.SUBMITTED}>Submitted</MenuItem>
          <MenuItem value={APIActions.PROOFED}>Proofed</MenuItem>
        </Select>
      </FormControl>
      <Link
        label={actionItem.destinationURL}
        href={actionItem.destinationURL}
        className={classes.linkMargin}
      />
      <Button onClick={handleDeleteAction(actionItem.id)}>
        <Delete />
      </Button>
    </ListItem>
  );
};

ActionItem.propTypes = {
  actionItem: PropTypes.shape({
    id: PropTypes.number,
    event: PropTypes.string,
    destinationURL: PropTypes.string,
  }).isRequired,
  handleDeleteAction: PropTypes.func.isRequired,
  handleChangeSelectValue: PropTypes.func.isRequired,
};

export default ActionItem;
