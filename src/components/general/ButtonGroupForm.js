import { useState } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core";
import SearchField from "../UI/SearchField";
import ButtonGroup from "./ButtonGroup";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  buttons: {
    marginRight: theme.spacing(4),
  },
}));

const ButtonGroupForm = ({ label, handleCancel, handleSave, defaultInputValue }) => {
  const classes = useStyles();
  const [inputValue, setInputValue] = useState(defaultInputValue);
  const [isButtonDisabled, setButtonState] = useState(true);

  const handleButtonState = () => {
    setButtonState(!isButtonDisabled);
  };

  const onInputChange = (event) => {
    setButtonState(true);
    setInputValue(event.target.value);
  };

  const onSave = () => {
    handleSave(inputValue);
  };

  return (
    <div className={classes.root}>
      <SearchField
        label={label}
        value={inputValue}
        onChange={onInputChange}
        debounceFunc={handleButtonState}
        debounceDelay={1500}
      />
      <ButtonGroup
        isSaveButtonDisabled={isButtonDisabled}
        handleSave={onSave}
        handleCancel={handleCancel}
      />
    </div>
  );
};

ButtonGroupForm.defaultProps = {
  defaultInputValue: "",
  label: "Search field",
  handleCancel: () => {},
  handleSave: () => {},
};

ButtonGroupForm.propTypes = {
  defaultInputValue: PropTypes.string,
  label: PropTypes.string,
  handleCancel: PropTypes.func,
  handleSave: PropTypes.func,
};

export default ButtonGroupForm;
