import PropTypes from "prop-types";
import { Grid, makeStyles, Paper, Switch, Typography } from "@material-ui/core";
import TextField from "../UI/TextField";
import ButtonGroup from "./ButtonGroup";

const useStyles = makeStyles((theme) => ({
  topMargin: {
    marginTop: theme.spacing(2),
  },
  linkMargin: {
    marginLeft: theme.spacing(3),
  },
}));

const EditToggledItem = ({
  handleTextField,
  textFieldDefaultValue,
  switchChecked,
  handleSwitch,
  switchLeftTitle,
  switchRightTitle,
  handleSave,
  handleCancel,
  saveButtonDisabled,
}) => {
  const classes = useStyles();

  return (
    <Paper elevation={0} className={classes.topMargin}>
      <TextField
        label="New api url"
        type="text"
        onChange={handleTextField}
        defaultValue={textFieldDefaultValue}
      />
      <Typography component="div">
        <Grid component="label" container alignItems="center" spacing={1}>
          <Grid item>{switchLeftTitle}</Grid>
          <Grid item>
            <Switch checked={switchChecked} onChange={handleSwitch} />
          </Grid>
          <Grid item>{switchRightTitle}</Grid>
        </Grid>
      </Typography>
      <ButtonGroup
        handleSave={handleSave}
        handleCancel={handleCancel}
        isSaveButtonDisabled={saveButtonDisabled}
      />
    </Paper>
  );
};

EditToggledItem.defaultProps = {
  switchLeftTitle: "off",
  switchRightTitle: "on",
};

EditToggledItem.propTypes = {
  switchLeftTitle: PropTypes.string,
  switchRightTitle: PropTypes.string,
  textFieldDefaultValue: PropTypes.string.isRequired,
  switchChecked: PropTypes.bool.isRequired,
  saveButtonDisabled: PropTypes.bool.isRequired,
  handleTextField: PropTypes.func.isRequired,
  handleSwitch: PropTypes.func.isRequired,
  handleSave: PropTypes.func.isRequired,
  handleCancel: PropTypes.func.isRequired,
};

export default EditToggledItem;
