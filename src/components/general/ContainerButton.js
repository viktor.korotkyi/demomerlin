import PropTypes from "prop-types";
import styles from "../../../scss/components/general/ContainerButton.module.scss";
import ButtonColor from "../UI/ButtonColor";
import colors from "../../constants/colors";

const ContainerButton = ({ clickFirst, clickSecond }) => (
  <div className={styles.container}>
    <ButtonColor click={clickFirst} label="Update" color={colors.primary} />
    <ButtonColor click={clickSecond} label="Cancel" color={colors.secondary} />
  </div>
);

ContainerButton.defaultProps = {
  clickFirst: () => {},
  clickSecond: () => {},
};

ContainerButton.propTypes = {
  clickFirst: PropTypes.func,
  clickSecond: PropTypes.func,
};

export default ContainerButton;
