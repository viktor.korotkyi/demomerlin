import PropTypes from "prop-types";
import Logo from "./Logo";
import LinkRef from "../UI/LinkRef";
import styles from "../../../scss/components/general/LogoFooter.module.scss";

const LogoFooter = ({ privacy, terms, urlLogo, href, logoStyle, logo }) => (
  <div className={`${styles.logoFooter} ${logo && styles[logo]}`}>
    <Logo href={href} styleType={logoStyle} urlLogo={urlLogo} />
    <div className={styles.policeBox}>
      <LinkRef href="/privacy" typeStyle="policyLink">
        {privacy}
      </LinkRef>
      <LinkRef href="/privacy" typeStyle="policyLink">
        {terms}
      </LinkRef>
    </div>
  </div>
);

LogoFooter.defaultProps = {
  privacy: "",
  terms: "",
  urlLogo: "",
  href: "",
  logoStyle: "",
  logo: "",
};

LogoFooter.propTypes = {
  privacy: PropTypes.string,
  terms: PropTypes.string,
  urlLogo: PropTypes.string,
  href: PropTypes.string,
  logoStyle: PropTypes.string,
  logo: PropTypes.string,
};

export default LogoFooter;
