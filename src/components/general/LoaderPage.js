import Container from "@material-ui/core/Container";
import { Box, makeStyles } from "@material-ui/core";
import { Skeleton } from "@material-ui/lab";
import helpItemArray from "../../utils/help";

const LoaderPage = () => {
  const useStyles = makeStyles((theme) => ({
    card: {
      width: "250px",
      height: "350px",
      marginTop: theme.spacing(2),
      marginBottom: theme.spacing(2),
    },
    container: {
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
      marginTop: "50px",
    },
    mainText: {
      marginTop: theme.spacing(3),
    },
  }));

  const classes = useStyles();

  return (
    <Container component="main" maxWidth="md">
      <Box>
        <Skeleton variant="text" height={50} width="60%" className={classes.mainText} />
      </Box>
      <Container maxWidth="md" className={classes.container} disableGutters>
        {helpItemArray.map((item, index) => (
          <Skeleton key={[item.name, index].join("_")} variant="rect" className={classes.card} />
        ))}
      </Container>
    </Container>
  );
};

export default LoaderPage;
