import PropTypes from "prop-types";
import moment from "moment-timezone";
import { FormControl, InputLabel, MenuItem, Select, makeStyles } from "@material-ui/core";
import styles from "../../../scss/components/general/SelectTimeZone.module.scss";

const timeZonesOfUSCountries = moment.tz.zonesForCountry("US");

const useStyles = makeStyles((theme) => ({
  formControl: {
    marginTop: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

const SelectTimeZone = ({ selectedTimezone, setSelectedTimezone }) => {
  const classes = useStyles();

  return (
    <div className={styles.box}>
      <span>Publishing Schedule</span>
      <FormControl variant="outlined" className={classes.formControl}>
        <InputLabel id="timezone-select-outlined-label">Timezone</InputLabel>
        <Select
          labelId="timezone-select-outlined-label"
          value={selectedTimezone}
          onChange={(event) => setSelectedTimezone(event.target.value)}
          label="Timezone"
        >
          {timeZonesOfUSCountries.map((countryTimezone) => {
            const UTCOffset = moment.tz(countryTimezone).format("Z z");
            return (
              <MenuItem key={countryTimezone} value={countryTimezone}>
                {`${countryTimezone} ${UTCOffset}`}
              </MenuItem>
            );
          })}
        </Select>
      </FormControl>
    </div>
  );
};

SelectTimeZone.defaultProps = {
  selectedTimezone: "",
  setSelectedTimezone: () => {},
};

SelectTimeZone.propTypes = {
  setSelectedTimezone: PropTypes.func,
  selectedTimezone: PropTypes.string,
};

export default SelectTimeZone;
