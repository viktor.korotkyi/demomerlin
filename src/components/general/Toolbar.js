import React from "react";
import PropTypes from "prop-types";
import { Toolbar as MaterialToolbar } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  title: {
    flex: "1 1 100%",
  },
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
}));

const Toolbar = ({ children }) => {
  const classes = useStyles();
  return (
    <MaterialToolbar className={classes.root}>
      <Typography className={classes.title} variant="h6" component="div">
        {children}
      </Typography>
    </MaterialToolbar>
  );
};

Toolbar.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]).isRequired,
};

export default Toolbar;
