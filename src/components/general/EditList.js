import PropTypes from "prop-types";
import { List, ListItem, ListItemText, makeStyles } from "@material-ui/core";
import styles from "../../../scss/components/PricingAdjustmentPage/PricingAdjustmentPage.module.scss";
import ButtonGroupForm from "./ButtonGroupForm";

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(2),
  },
}));

const EditList = ({ label, items, handleEdit, handleDelete, handleSave }) => {
  const classes = useStyles();

  const handleButtonClick = (id) => {
    return () => handleEdit(id);
  };

  const onEdit = (id) => {
    return () => handleEdit(id);
  };

  const onSave = (id) => {
    return (value) => {
      handleSave(id, value);
    };
  };

  return (
    <List className={`${classes.root} ${styles["list-container"]}`}>
      {items.map((item) => (
        <ListItem className={styles["list-item"]} key={item.id}>
          {!item.edit ? (
            <ListItemText primary={item.name} />
          ) : (
            <ButtonGroupForm
              label={label}
              defaultInputValue={item.name}
              handleCancel={handleButtonClick(item.id)}
              handleSave={onSave(item.id)}
            />
          )}
          {!item.edit && (
            <div className={styles["buttons-container"]}>
              <ListItem button onClick={onEdit(item.id)}>
                <ListItemText primary="edit" />
              </ListItem>
              <ListItem button onClick={handleDelete}>
                <ListItemText primary="delete" />
              </ListItem>
            </div>
          )}
        </ListItem>
      ))}
    </List>
  );
};

EditList.defaultProps = {
  handleEdit: () => {},
  handleDelete: () => {},
  handleSave: () => {},
};

EditList.propTypes = {
  label: PropTypes.string.isRequired,
  items: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  handleEdit: PropTypes.func,
  handleDelete: PropTypes.func,
  handleSave: PropTypes.func,
};

export default EditList;
