import PropTypes from "prop-types";
import { useRouter } from "next/router";
import TextField from "../UI/TextField";
import CheckBox from "../UI/CheckBox";
import SelectList from "../UI/SelectList";
import styles from "../../../scss/components/general/PricingAdjustment.module.scss";
import Link from "../UI/Link";

const PricingAdjustment = ({ onChange, valueTribune, listAdjustment }) => {
  const {
    query: { portalName, portalChild, setting },
  } = useRouter();
  return (
    <div className={styles.container}>
      <h3>Pricing Adjustment</h3>
      <div className={styles.priceType}>
        <TextField
          onChange={onChange}
          id="valueTribune"
          label="Tribune"
          defaultValue={valueTribune}
          type="number"
        />
        <CheckBox handleChange={onChange} id="valueCheckPriceDay" label="Pricing Per Day" />
      </div>
      <div className={styles["priceAdjustment-container"]}>
        <SelectList
          handleSelect={onChange}
          id="selectedAdjustmentPrice"
          list={listAdjustment}
          label="Add Price Adjustment"
        />
        <Link href={`/${portalName}/${portalChild}/${setting}/pricing-adjustment`} label="Edit" />
      </div>
    </div>
  );
};

PricingAdjustment.defaultProps = {
  onChange: () => {},
  valueTribune: "",
  listAdjustment: [],
};

PricingAdjustment.propTypes = {
  onChange: PropTypes.func,
  valueTribune: PropTypes.string,
  listAdjustment: PropTypes.arrayOf(PropTypes.shape({})),
};

export default PricingAdjustment;
