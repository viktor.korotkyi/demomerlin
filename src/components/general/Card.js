import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import ButtonNoBackground from "../UI/ButtonNoBackground";
import constants from "../../constants/storage";

const useStyles = makeStyles({
  root: {
    width: 250,
  },
  media: {
    backgroundSize: "contain",
    height: 250,
  },
});

const CardImage = ({ logo, header, clickCard, id, editPortal }) => {
  const classes = useStyles();
  return (
    <Card className={classes.root}>
      <CardActionArea onClick={() => clickCard(id, header)}>
        <CardMedia className={classes.media} image={logo} title="Contemplative Reptile" />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {header}
          </Typography>
        </CardContent>
      </CardActionArea>
      <ButtonNoBackground click={() => editPortal(id)} label={constants.edit} />
    </Card>
  );
};

CardImage.defaultProps = {
  logo: "",
  header: "",
  id: "",
  clickCard: () => {},
  editPortal: () => {},
};

CardImage.propTypes = {
  logo: PropTypes.string,
  header: PropTypes.string,
  id: PropTypes.string,
  clickCard: PropTypes.func,
  editPortal: PropTypes.func,
};

export default CardImage;
