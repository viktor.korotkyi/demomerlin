import PropTypes from "prop-types";
import constants from "../../constants/storage";
import { filterArray } from "../../utils/storageWork";
import TextTemplate from "./TextTemplate";
import styles from "../../../scss/components/general/ViewContent.module.scss";

const ViewContent = ({ content }) => (
  <div className={styles.container}>
    <div className={styles.header}>
      <h3>live preview</h3>
    </div>
    <div className={styles.content}>
      <div
        className={styles.template}
        style={{ width: Number(content.width), height: Number(content.height) }}
      >
        {content.elements.map((item, index) => {
          if (item.type === constants.image) {
            return (
              <img
                key={[item.id, index].join("_")}
                src={item.url}
                style={{
                  width: Number(item.width),
                  height: Number(item.height),
                  marginTop: Number(filterArray(item.margin, constants.marginTop)[0].value),
                  marginRight: Number(filterArray(item.margin, constants.marginRight)[0].value),
                  marginLeft: Number(filterArray(item.margin, constants.marginLeft)[0].value),
                  marginBottom: Number(filterArray(item.margin, constants.marginBottom)[0].value),
                  alignSelf: `${item.alignment}`,
                  y: item.y,
                  x: item.x,
                }}
                alt={constants.photoImage}
              />
            );
          }
          return <TextTemplate key={[item.id, index].join("_")} item={item} />;
        })}
      </div>
    </div>
  </div>
);

ViewContent.defaultProps = {
  content: {
    elements: [],
  },
};

ViewContent.propTypes = {
  content: PropTypes.shape({
    width: PropTypes.string,
    height: PropTypes.string,
    elements: PropTypes.arrayOf(PropTypes.shape({})),
  }),
};
export default ViewContent;
