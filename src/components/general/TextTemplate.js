import PropTypes from "prop-types";
import constants from "../../constants/storage";
import { filterArray } from "../../utils/storageWork";
import fontsType from "../../constants/fontsType";

const TextTemplate = ({ item }) => {
  const styleSettings = {
    fontFamily: item.fontFamily,
    fontSize: Number(item.fontSize),
    fontStyle: item.fontStyle,
    fontWeight: item.fontWeight,
    y: item.y,
    x: item.x,
    alignSelf: item.textAlign,
    textDecoration: item.textDecoration,
    marginTop: Number(filterArray(item.margin, constants.marginTop)[0].value),
    marginRight: Number(filterArray(item.margin, constants.marginRight)[0].value),
    marginLeft: Number(filterArray(item.margin, constants.marginLeft)[0].value),
    marginBottom: Number(filterArray(item.margin, constants.marginBottom)[0].value),
  };
  const renderText = (type) => {
    switch (type) {
      case fontsType.headerOne:
        return <h1 style={styleSettings}>{item.text}</h1>;
      case fontsType.headerTwo:
        return <h2 style={styleSettings}>{item.text}</h2>;
      case fontsType.headerThree:
        return <h3 style={styleSettings}>{item.text}</h3>;
      case fontsType.headerFour:
        return <h4 style={styleSettings}>{item.text}</h4>;
      case fontsType.headerFive:
        return <h5 style={styleSettings}>{item.text}</h5>;
      case fontsType.headerSix:
        return <h6 style={styleSettings}>{item.text}</h6>;
      default:
        return <span style={styleSettings}>{item.text}</span>;
    }
  };

  return <>{renderText(item.type)}</>;
};

TextTemplate.defaultProps = {
  item: {},
};

TextTemplate.propTypes = {
  item: PropTypes.shape({
    text: PropTypes.string,
    y: PropTypes.number,
    x: PropTypes.number,
    fontFamily: PropTypes.string,
    textAlign: PropTypes.string,
    type: PropTypes.string,
    textDecoration: PropTypes.string,
    fontWeight: PropTypes.string,
    fontStyle: PropTypes.string,
    fontSize: PropTypes.string,
    margin: PropTypes.arrayOf(PropTypes.shape({})),
  }),
};

export default TextTemplate;
