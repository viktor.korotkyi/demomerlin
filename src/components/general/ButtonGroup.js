import PropTypes from "prop-types";
import { Button, makeStyles, ButtonGroup as MaterialButtonGroup } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  buttons: {
    marginRight: theme.spacing(4),
  },
}));

const ButtonGroup = ({
  handleCancel,
  handleSave,
  isSaveButtonDisabled,
  className,
  successLabel,
  cancelLabel,
}) => {
  const classes = useStyles();

  return (
    <MaterialButtonGroup
      variant="contained"
      color="primary"
      className={`${classes.buttons} ${className}`}
    >
      <Button
        disabled={isSaveButtonDisabled}
        variant="contained"
        type="submit"
        color="primary"
        onClick={handleSave}
      >
        {successLabel}
      </Button>
      <Button variant="contained" color="secondary" onClick={handleCancel}>
        {cancelLabel}
      </Button>
    </MaterialButtonGroup>
  );
};

ButtonGroup.defaultProps = {
  isSaveButtonDisabled: false,
  successLabel: "Save",
  cancelLabel: "Cancel",
  className: "",
  handleSave: () => {},
  handleCancel: () => {},
};

ButtonGroup.propTypes = {
  isSaveButtonDisabled: PropTypes.bool,
  successLabel: PropTypes.string,
  cancelLabel: PropTypes.string,
  className: PropTypes.string,
  handleSave: PropTypes.func,
  handleCancel: PropTypes.func,
};

export default ButtonGroup;
