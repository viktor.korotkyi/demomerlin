import PropTypes from "prop-types";
import LinkRef from "../UI/LinkRef";

const Logo = ({ href, text, urlLogo, styleType }) => (
  <LinkRef href={href} typeStyle={styleType}>
    <img src={urlLogo} alt="logo" />
    {text && <span>{text}</span>}
  </LinkRef>
);

Logo.defaultProps = {
  text: "",
  styleType: "",
  urlLogo: "",
  href: "",
};

Logo.propTypes = {
  text: PropTypes.string,
  styleType: PropTypes.string,
  urlLogo: PropTypes.string,
  href: PropTypes.string,
};

export default Logo;
