import PropTypes from "prop-types";
import styles from "../../../scss/components/general/ButtonBoxCreate.module.scss";
import ButtonColor from "../UI/ButtonColor";
import colors from "../../constants/colors";

const ButtonBoxCreate = ({ saveBtn, cancelBtn, deleteBtn, disabled, widthContainer }) => (
  <div className={styles.container} style={{ width: `${widthContainer}` }}>
    <div className={styles.boxBtn}>
      <ButtonColor
        click={saveBtn}
        label="Save"
        disabled={disabled}
        colorBack={colors.green}
        color={colors.primary}
      />
      <ButtonColor click={cancelBtn} label="Cancel" color={colors.default} />
    </div>
    <ButtonColor click={deleteBtn} label="Delete" color={colors.secondary} />
  </div>
);

ButtonBoxCreate.defaultProps = {
  saveBtn: () => {},
  cancelBtn: () => {},
  deleteBtn: () => {},
  disabled: false,
  widthContainer: "",
};

ButtonBoxCreate.propTypes = {
  saveBtn: PropTypes.func,
  cancelBtn: PropTypes.func,
  deleteBtn: PropTypes.func,
  disabled: PropTypes.bool,
  widthContainer: PropTypes.string,
};

export default ButtonBoxCreate;
