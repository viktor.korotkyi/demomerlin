import PropTypes from "prop-types";
import MultiSelect from "../UI/MultiSelect";
import styles from "../../../scss/components/general/SelectContainer.module.scss";

const SelectContainer = ({
  titleSelect,
  listSelect,
  placeholderSelect,
  handleSelect,
  selectedItems,
  minWidth,
  maxWidth,
  width,
}) => (
  <div className={styles.selectBox}>
    <span className={styles.titleSelect}>{titleSelect}</span>
    <MultiSelect
      list={listSelect}
      placeholder={placeholderSelect}
      handleSelect={handleSelect}
      selectedItems={selectedItems}
      maxWidth={maxWidth}
      minWidth={minWidth}
      width={width}
    />
  </div>
);

SelectContainer.defaultProps = {
  titleSelect: "",
  placeholderSelect: "",
  listSelect: [],
  selectedItems: [],
  handleSelect: () => {},
  minWidth: 0,
  maxWidth: 0,
  width: 0,
};

SelectContainer.propTypes = {
  titleSelect: PropTypes.string,
  placeholderSelect: PropTypes.string,
  selectedItems: PropTypes.arrayOf(PropTypes.string),
  listSelect: PropTypes.arrayOf(PropTypes.shape({})),
  handleSelect: PropTypes.func,
  minWidth: PropTypes.number,
  maxWidth: PropTypes.number,
  width: PropTypes.number,
};

export default SelectContainer;
