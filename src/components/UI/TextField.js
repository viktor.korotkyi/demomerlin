import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";

const TextInput = ({ onChange, id, label, defaultValue, type }) => {
  const useStyles = makeStyles(() => ({
    root: {
      "& > *": {
        margin: 0,
        width: "25ch",
      },
    },
  }));

  const classes = useStyles();
  return (
    <form className={classes.root} noValidate autoComplete="off">
      <TextField
        id={id}
        label={label}
        defaultValue={defaultValue}
        onChange={onChange}
        type={type}
      />
    </form>
  );
};

TextInput.defaultProps = {
  onChange: () => {},
  id: "",
  label: "",
  defaultValue: "",
  type: "",
};

TextInput.propTypes = {
  onChange: PropTypes.func,
  id: PropTypes.string,
  label: PropTypes.string,
  defaultValue: PropTypes.string,
  type: PropTypes.string,
};

export default TextInput;
