import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";

const ButtonColor = ({ click, label, disabled, color, colorBack, id }) => {
  const useStyles = makeStyles(() => ({
    root: {
      "& > *": {
        margin: 0,
      },
    },
  }));

  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Button
        variant="contained"
        color={color}
        onClick={() => click(id)}
        disabled={disabled}
        style={{ background: `${colorBack}` }}
      >
        {label}
      </Button>
    </div>
  );
};

ButtonColor.defaultProps = {
  click: () => {},
  label: "",
  disabled: false,
  color: "default",
  colorBack: "",
  id: "",
};

ButtonColor.propTypes = {
  click: PropTypes.func,
  label: PropTypes.string,
  color: PropTypes.string,
  disabled: PropTypes.bool,
  colorBack: PropTypes.string,
  id: PropTypes.string,
};

export default ButtonColor;
