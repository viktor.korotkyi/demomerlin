import PropTypes from "prop-types";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";

const CheckBox = ({ handleChange, label, id }) => {
  return (
    <FormControlLabel
      control={<Checkbox color="primary" onChange={handleChange} id={id} />}
      label={label}
    />
  );
};

CheckBox.defaultProps = {
  handleChange: () => {},
  label: "",
  id: "",
};

CheckBox.propTypes = {
  handleChange: PropTypes.func,
  label: PropTypes.string,
  id: PropTypes.string,
};

export default CheckBox;
