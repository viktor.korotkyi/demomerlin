import PropTypes from "prop-types";
import React from "react";
import Link from "next/link";

import styles from "../../../scss/components/UI/LinkRef.module.scss";

const LinkRef = ({ children, typeStyle, href, asPath, pathActive, pathUrl }) => {
  const active = pathUrl === pathActive ? "activeLink" : "";
  return (
    <Link href={href} as={asPath} passHref>
      <span className={`${styles[typeStyle]} ${active && styles[active]}`}>
        <a>{children}</a>
      </span>
    </Link>
  );
};
LinkRef.defaultProps = {
  typeStyle: "",
  href: "",
  asPath: "",
  pathActive: "",
  pathUrl: "",
};

LinkRef.propTypes = {
  children: PropTypes.node.isRequired,
  typeStyle: PropTypes.string,
  href: PropTypes.string,
  asPath: PropTypes.string,
  pathActive: PropTypes.string,
  pathUrl: PropTypes.string,
};

export default LinkRef;
