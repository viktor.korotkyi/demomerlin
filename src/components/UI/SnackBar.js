import PropTypes from "prop-types";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import { makeStyles } from "@material-ui/core/styles";

const SnackbarMessedger = ({ open, vertical, horizontal, handleClose, message }) => {
  const Alert = (props) => {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  };

  const useStyles = makeStyles((theme) => ({
    root: {
      width: "100%",
      "& > * + *": {
        marginTop: theme.spacing(2),
      },
    },
  }));
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Snackbar
        anchorOrigin={{ vertical, horizontal }}
        open={open}
        onClose={handleClose}
        key={vertical + horizontal}
        autoHideDuration={2000}
      >
        <Alert onClose={handleClose} severity="success">
          {message}
        </Alert>
      </Snackbar>
    </div>
  );
};

SnackbarMessedger.defaultProps = {
  handleClose: () => {},
  horizontal: "",
  vertical: "",
  message: "",
  open: false,
};

SnackbarMessedger.propTypes = {
  handleClose: PropTypes.func,
  open: PropTypes.bool,
  vertical: PropTypes.string,
  horizontal: PropTypes.string,
  message: PropTypes.string,
};

export default SnackbarMessedger;
