import PropTypes from "prop-types";
import NextLink from "next/link";
import { Link as MaterialLink, makeStyles } from "@material-ui/core";
import ButtonNoBackground from "./ButtonNoBackground";

const useStyles = makeStyles({
  noHoverTextDecoration: {
    "&:hover": {
      textDecoration: "none",
    },
  },
});

const Link = ({ href, label, className }) => {
  const classes = useStyles();

  return (
    <NextLink href={href}>
      <MaterialLink className={`${classes.noHoverTextDecoration} ${className}`}>
        <ButtonNoBackground label={label} />
      </MaterialLink>
    </NextLink>
  );
};

Link.defaultProps = {
  className: "",
};

Link.propTypes = {
  href: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  className: PropTypes.string,
};

export default Link;
