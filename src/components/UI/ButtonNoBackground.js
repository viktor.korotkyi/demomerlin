import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";

const ButtonNoBackground = ({ click, id, label }) => {
  const useStyles = makeStyles(() => ({
    root: {
      "& > *": {
        margin: 0,
        padding: 0,
      },
    },
  }));
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Button color="primary" onClick={() => click(id)}>
        {label}
      </Button>
    </div>
  );
};

ButtonNoBackground.defaultProps = {
  click: () => {},
  id: "",
  label: "",
};

ButtonNoBackground.propTypes = {
  click: PropTypes.func,
  id: PropTypes.string,
  label: PropTypes.string,
};

export default ButtonNoBackground;
