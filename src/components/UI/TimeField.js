import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";

const TimeField = ({ id, label, onChange }) => {
  const useStyles = makeStyles((theme) => ({
    container: {
      display: "flex",
      flexWrap: "wrap",
    },
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      width: 200,
    },
  }));

  const classes = useStyles();

  return (
    <form className={classes.container} noValidate>
      <TextField
        id={id}
        label={label}
        type="time"
        defaultValue="00:00"
        onChange={onChange}
        className={classes.textField}
        InputLabelProps={{
          shrink: true,
        }}
        inputProps={{
          step: 300,
        }}
      />
    </form>
  );
};

TimeField.defaultProps = {
  id: "",
  label: "",
  onChange: "",
};

TimeField.propTypes = {
  id: PropTypes.string,
  label: PropTypes.string,
  onChange: () => {},
};

export default TimeField;
