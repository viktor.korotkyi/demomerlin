import PropTypes from "prop-types";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

const AlertDialog = ({ open, handleClose, title, handleUpdateAlert, decription }) => (
  <div>
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">{decription}</DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} color="primary">
          Cancel
        </Button>
        <Button onClick={handleUpdateAlert} color="primary" autoFocus>
          Update
        </Button>
      </DialogActions>
    </Dialog>
  </div>
);

AlertDialog.defaultProps = {
  open: false,
  handleClose: () => {},
  handleUpdateAlert: () => {},
  decription: "",
  title: "",
};

AlertDialog.propTypes = {
  open: PropTypes.bool,
  handleClose: PropTypes.func,
  handleUpdateAlert: PropTypes.func,
  decription: PropTypes.string,
  title: PropTypes.string,
};

export default AlertDialog;
