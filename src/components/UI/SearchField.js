import { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";

const SearchField = ({ onChange, label, value, debounceFunc, debounceDelay }) => {
  const useStyles = makeStyles((theme) => ({
    root: {
      "& > *": {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
        width: "25ch",
      },
    },
  }));

  const [isFirstRender, setIsFirstRenderState] = useState(true);

  useEffect(() => {
    if (isFirstRender) {
      setIsFirstRenderState(false);
    }
  }, [isFirstRender]);

  useEffect(() => {
    if (value.trim() && !isFirstRender && debounceDelay > 0) {
      const timeoutRef = setTimeout(() => {
        debounceFunc();
      }, debounceDelay);

      return () => clearTimeout(timeoutRef);
    }

    return null;
  }, [value]);

  const classes = useStyles();
  return (
    <form className={classes.root} noValidate autoComplete="off">
      <TextField label={label} value={value} color="primary" onChange={onChange} />
    </form>
  );
};

SearchField.defaultProps = {
  debounceDelay: null,
  label: "Search field",
  value: "",
  onChange: () => {},
  debounceFunc: () => {},
};

SearchField.propTypes = {
  debounceDelay: PropTypes.number,
  value: PropTypes.string,
  label: PropTypes.string,
  onChange: PropTypes.func,
  debounceFunc: PropTypes.func,
};

export default SearchField;
