import PropTypes from "prop-types";
import styles from "../../../scss/components/UI/ButtonUpload.module.scss";

const ButtonUpload = ({ onClick, id, image, imageUpload }) => (
  <div>
    <label htmlFor={id} className={styles.container}>
      {image ? (
        <img src={image} alt="dummy" />
      ) : (
        <img className={styles.buttonUpload} src={imageUpload} alt="upload" />
      )}
    </label>
    <input id={id} type="file" style={{ display: "none" }} onChange={onClick} />
  </div>
);

ButtonUpload.defaultProps = {
  onClick: () => {},
  image: "",
  id: "",
  imageUpload: "",
};

ButtonUpload.propTypes = {
  onClick: PropTypes.func,
  image: PropTypes.string,
  id: PropTypes.string,
  imageUpload: PropTypes.string,
};

export default ButtonUpload;
