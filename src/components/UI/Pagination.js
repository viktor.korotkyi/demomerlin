import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Pagination from "@material-ui/lab/Pagination";

const PaginationRounded = ({ count, onChange }) => {
  const useStyles = makeStyles((theme) => ({
    root: {
      "& > *": {
        marginTop: theme.spacing(2),
      },
    },
  }));

  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Pagination
        count={count}
        variant="outlined"
        shape="rounded"
        color="primary"
        onChange={onChange}
      />
    </div>
  );
};

PaginationRounded.defaultProps = {
  count: 1,
  onChange: () => {},
};

PaginationRounded.propTypes = {
  count: PropTypes.number,
  onChange: PropTypes.func,
};

export default PaginationRounded;
