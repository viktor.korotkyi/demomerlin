import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import NativeSelect from "@material-ui/core/NativeSelect";

const SelectList = ({ handleSelect, id, name, list, label }) => {
  const useStyles = makeStyles((theme) => ({
    formControl: {
      margin: 0,
      minWidth: 120,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  }));

  const classes = useStyles();

  return (
    <div>
      <FormControl className={classes.formControl} onChange={handleSelect}>
        <InputLabel htmlFor="uncontrolled-native">{label}</InputLabel>
        <NativeSelect
          defaultValue={30}
          inputProps={{
            name,
            id,
          }}
        >
          {list.map((item, index) => (
            <option value={item} key={[index, item.label].join("_")}>
              {item}
            </option>
          ))}
        </NativeSelect>
      </FormControl>
    </div>
  );
};

SelectList.defaultProps = {
  handleSelect: () => {},
  id: "",
  name: "",
  label: "",
  list: [],
};

SelectList.propTypes = {
  handleSelect: PropTypes.func,
  id: PropTypes.string,
  name: PropTypes.string,
  label: PropTypes.string,
  list: PropTypes.arrayOf(PropTypes.string),
};

export default SelectList;
