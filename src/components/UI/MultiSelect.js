import PropTypes from "prop-types";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import multiSelect from "../../constants/multiSelect";

const MultiSelect = ({
  placeholder,
  list,
  handleSelect,
  selectedItems,
  minWidth,
  maxWidth,
  width,
}) => {
  const useStyles = makeStyles((theme) => ({
    formControl: {
      margin: theme.spacing(1),
      minWidth,
      maxWidth,
    },
    chips: {
      display: "flex",
      flexWrap: "wrap",
    },
    chip: {
      margin: 2,
    },
    noLabel: {
      marginTop: theme.spacing(3),
    },
  }));

  const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: multiSelect.ITEM_HEIGHT * 4.5 + multiSelect.ITEM_PADDING_TOP,
        width,
      },
    },
  };

  const getStyles = (name, personName, theme) => {
    return {
      fontWeight:
        personName.indexOf(name) === -1
          ? theme.typography.fontWeightRegular
          : theme.typography.fontWeightMedium,
    };
  };

  const classes = useStyles();
  const theme = useTheme();

  return (
    <div>
      <FormControl className={classes.formControl}>
        <InputLabel id={placeholder}>{placeholder}</InputLabel>
        <Select
          labelId={placeholder}
          id={placeholder}
          multiple
          value={selectedItems}
          onChange={handleSelect}
          input={<Input />}
          MenuProps={MenuProps}
        >
          {list.map((item, index) => (
            <MenuItem
              key={[item.value, index].join("_")}
              value={item.value}
              style={getStyles(item.value, selectedItems, theme)}
            >
              {item.value}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  );
};

MultiSelect.defaultProps = {
  placeholder: "",
  list: [],
  selectedItems: [],
  handleSelect: () => {},
  minWidth: 0,
  maxWidth: 0,
  width: 0,
};

MultiSelect.propTypes = {
  placeholder: PropTypes.string,
  list: PropTypes.arrayOf(PropTypes.shape({})),
  selectedItems: PropTypes.arrayOf(PropTypes.string),
  handleSelect: PropTypes.func,
  minWidth: PropTypes.number,
  maxWidth: PropTypes.number,
  width: PropTypes.number,
};

export default MultiSelect;
