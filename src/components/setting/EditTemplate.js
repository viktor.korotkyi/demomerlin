import { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { makeStyles, TextField } from "@material-ui/core";
import ButtonGroup from "../general/ButtonGroup";
import styles from "../../../scss/components/setting/EditTemplate.module.scss";
import NumberFormatCustom from "../general/NumberFormatCustom";

const useStyles = makeStyles({
  buttonGroup: {
    marginRight: 0,
  },
});

const EditTemplate = ({ cost, imageCost, perInchCost, updateTemplateEdit, cancelTemplateEdit }) => {
  const [inputCostValue, setInputCostValue] = useState(cost);
  const [inputImageCostValue, setInputImageCostValue] = useState(imageCost);
  const [inputPerInchCostValue, setInputPerInchCostValue] = useState(perInchCost);
  const classes = useStyles();

  const handleSaveButtonClick = () => {
    updateTemplateEdit({
      cost: inputCostValue,
      imageCost: inputImageCostValue,
      perInchCost: inputPerInchCostValue,
    });
  };

  const onInputChange = (dispatch) => (event) => dispatch(event.target.value);

  const handleCancelButtonClick = () => cancelTemplateEdit();

  useEffect(() => {
    setInputCostValue(cost);
    setInputImageCostValue(imageCost);
    setInputPerInchCostValue(perInchCost);
  }, [cost, imageCost, perInchCost]);

  return (
    <div className={styles.container}>
      <div className={styles.box}>
        <h3>Add Template Pricing</h3>
        <TextField
          label="Cost"
          value={inputCostValue}
          onChange={onInputChange(setInputCostValue)}
          id="cost"
          InputProps={{
            inputComponent: NumberFormatCustom,
          }}
        />
        <TextField
          label="Image cost"
          value={inputImageCostValue}
          onChange={onInputChange(setInputImageCostValue)}
          id="image-cost"
          InputProps={{
            inputComponent: NumberFormatCustom,
          }}
        />
        <TextField
          label="Per inch cost"
          value={inputPerInchCostValue}
          onChange={onInputChange(setInputPerInchCostValue)}
          id="per-inch-cost"
          InputProps={{
            inputComponent: NumberFormatCustom,
          }}
        />
      </div>
      <ButtonGroup
        handleSave={handleSaveButtonClick}
        handleCancel={handleCancelButtonClick}
        className={classes.buttonGroup}
        successLabel="Update"
      />
    </div>
  );
};

EditTemplate.defaultProps = {
  updateTemplateEdit: () => {},
  cancelTemplateEdit: () => {},
};

EditTemplate.propTypes = {
  cost: PropTypes.string.isRequired,
  imageCost: PropTypes.string.isRequired,
  perInchCost: PropTypes.string.isRequired,
  updateTemplateEdit: PropTypes.func,
  cancelTemplateEdit: PropTypes.func,
};

export default EditTemplate;
