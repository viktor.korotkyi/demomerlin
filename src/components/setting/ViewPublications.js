import PropTypes from "prop-types";
import styles from "../../../scss/components/setting/ViewPublications.module.scss";
import ButtonBoxCreate from "../general/ButtonBoxCreate";
import SelectTimeZone from "../general/SelectTimeZone";
import PanelDays from "../settingComponents/PanelDays";
import PanelEditItem from "../settingComponents/PanelEditItem";
import PublishBox from "../settingComponents/PublishBox";
import Actions from "../actions/Actions";
import SelectContainer from "../general/SelectContainer";

const ViewPublications = ({
  header,
  name,
  handleInput,
  uploadFile,
  fileUpload,
  listSelectPackages,
  handleSelect,
  selectedTimezone,
  setSelectedTimezone,
  cancelBtn,
  deleteBtn,
  disabled,
  saveBtn,
  onChangeInput,
  panelDays,
  handleCheck,
  viewSetup,
  handlePublishDay,
  listDaysPub,
  handleSetup,
  valueSetup,
  editChoosedPackages,
  listChoosedPackages,
}) => (
  <div className={styles.container}>
    <PanelEditItem
      fileUpload={fileUpload}
      handleInput={handleInput}
      handleSelect={handleSelect}
      header={header}
      listSelectPackages={listSelectPackages}
      name={name}
      uploadFile={uploadFile}
      selectedItems={listChoosedPackages}
      editChoosedPackages={editChoosedPackages}
    >
      <SelectContainer
        titleSelect="Packages"
        listSelect={listSelectPackages}
        placeholderSelect="Select packages"
        handleSelect={handleSelect}
        selectedItems={listChoosedPackages}
        maxWidth={250}
        minWidth={150}
        width={250}
      />
    </PanelEditItem>
    <div className={styles.setting}>
      <ButtonBoxCreate
        cancelBtn={cancelBtn}
        deleteBtn={deleteBtn}
        disabled={disabled}
        saveBtn={saveBtn}
      />
      <div className={styles.zoneBox}>
        <SelectTimeZone
          selectedTimezone={selectedTimezone}
          setSelectedTimezone={setSelectedTimezone}
        />
      </div>
      <PanelDays
        id="days"
        onChange={onChangeInput}
        defaultValue={panelDays}
        handleChange={handleCheck}
      />
      <PublishBox
        handlePublishDay={handlePublishDay}
        listDays={listDaysPub}
        viewSetup={viewSetup}
        handleSetup={handleSetup}
        valueSetup={valueSetup}
      />
    </div>
    <Actions />
  </div>
);

ViewPublications.defaultProps = {
  header: "",
  name: "",
  fileUpload: "",
  handleInput: () => {},
  uploadFile: () => {},
  handleSelect: () => {},
  listSelectPackages: [],
  selectedTimezone: "",
  setSelectedTimezone: () => {},
  cancelBtn: () => {},
  deleteBtn: () => {},
  disabled: false,
  viewSetup: {},
  saveBtn: () => {},
  onChangeInput: () => {},
  panelDays: "",
  handleCheck: () => {},
  handlePublishDay: () => {},
  handleSetup: () => {},
  listDaysPub: [],
  valueSetup: {},
  editChoosedPackages: () => {},
  listChoosedPackages: [],
};

ViewPublications.propTypes = {
  header: PropTypes.string,
  name: PropTypes.string,
  fileUpload: PropTypes.string,
  handleInput: PropTypes.func,
  uploadFile: PropTypes.func,
  handleSelect: PropTypes.func,
  listSelectPackages: PropTypes.arrayOf(PropTypes.shape({})),
  listDaysPub: PropTypes.arrayOf(PropTypes.string),
  setSelectedTimezone: PropTypes.func,
  selectedTimezone: PropTypes.string,
  cancelBtn: PropTypes.func,
  deleteBtn: PropTypes.func,
  disabled: PropTypes.bool,
  viewSetup: PropTypes.shape({}),
  saveBtn: PropTypes.func,
  onChangeInput: PropTypes.func,
  panelDays: PropTypes.string,
  handleCheck: PropTypes.func,
  handlePublishDay: PropTypes.func,
  handleSetup: PropTypes.func,
  valueSetup: PropTypes.shape({}),
  editChoosedPackages: PropTypes.func,
  listChoosedPackages: PropTypes.arrayOf(PropTypes.string),
};
export default ViewPublications;
