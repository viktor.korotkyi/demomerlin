import { useState } from "react";
import PropTypes from "prop-types";
import { makeStyles, Modal } from "@material-ui/core";
import TextField from "../UI/TextField";
import styles from "../../../scss/components/setting/EditPackage.module.scss";
import PricingAdjustment from "../general/PricingAdjustment";
import PanelSelectedItems from "../settingComponents/PanelSelectedItems";
import ContainerButton from "../general/ContainerButton";
import EditTemplate from "./EditTemplate";

const useStyles = makeStyles({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
});

const EditPackage = ({
  cost,
  imageCost,
  perInchCost,
  onChange,
  editPackagesInfo,
  listAdjustment,
  editTemplate,
  listTemplate,
  updatePackageInfo,
  cancelPackageInfo,
  cancelTemplateEdit,
  updateTemplateEdit,
}) => {
  const [modalState, setModalState] = useState(false);
  const classes = useStyles();

  const handleModalState = () => {
    setModalState((prevModalState) => !prevModalState);
  };

  const editTemplateItem = (item) => {
    handleModalState();
    editTemplate(item);
  };

  const updateTemplateEditing = (costValues) => {
    handleModalState();
    updateTemplateEdit(costValues);
  };

  const cancelTemplateEditing = () => {
    handleModalState();
    cancelTemplateEdit();
  };

  return (
    <div className={styles.container}>
      <h3>Edit Package Pricing Info</h3>
      <TextField
        onChange={onChange}
        id="costPerInch"
        label="Price Per Inch"
        defaultValue={editPackagesInfo.pricePerInch}
        type="number"
      />
      <PricingAdjustment
        onChange={onChange}
        valueTribune={editPackagesInfo.valueTribune}
        listAdjustment={listAdjustment}
      />
      <h3 className={styles.headerList}>Templates</h3>
      <div className={styles.box}>
        <PanelSelectedItems edit={editTemplateItem} list={listTemplate} />
      </div>
      <ContainerButton clickFirst={updatePackageInfo} clickSecond={cancelPackageInfo} />
      <Modal open={modalState} onClose={handleModalState} className={classes.modal}>
        <div>
          <EditTemplate
            cost={cost}
            imageCost={imageCost}
            perInchCost={perInchCost}
            cancelTemplateEdit={cancelTemplateEditing}
            updateTemplateEdit={updateTemplateEditing}
          />
        </div>
      </Modal>
    </div>
  );
};

EditPackage.defaultProps = {
  listAdjustment: [],
  listTemplate: [],
  editPackagesInfo: {},
  onChange: () => {},
  editTemplate: () => {},
  updatePackageInfo: () => {},
  cancelPackageInfo: () => {},
  updateTemplateEdit: () => {},
  cancelTemplateEdit: () => {},
};

EditPackage.propTypes = {
  cost: PropTypes.string.isRequired,
  imageCost: PropTypes.string.isRequired,
  perInchCost: PropTypes.string.isRequired,
  listAdjustment: PropTypes.arrayOf(PropTypes.shape({})),
  listTemplate: PropTypes.arrayOf(PropTypes.shape({})),
  editPackagesInfo: PropTypes.shape({
    pricePerInch: PropTypes.string,
    valueTribune: PropTypes.string,
  }),
  onChange: PropTypes.func,
  editTemplate: PropTypes.func,
  updatePackageInfo: PropTypes.func,
  cancelPackageInfo: PropTypes.func,
  updateTemplateEdit: PropTypes.func,
  cancelTemplateEdit: PropTypes.func,
};

export default EditPackage;
