import { EditorState, RichUtils, Modifier } from "draft-js";
import { Map } from "immutable";
import customInlineStylesMap from "../../../constants/customInlineStylesMap";
import constants from "../../../constants/editor";
/**
 * Function returns collection of currently selected blocks.
 */
export function getSelectedBlocksMap(editorState) {
  const selectionState = editorState.getSelection();
  const contentState = editorState.getCurrentContent();
  const startKey = selectionState.getStartKey();
  const endKey = selectionState.getEndKey();
  const blockMap = contentState.getBlockMap();
  return blockMap
    .toSeq()
    .skipUntil((_, k) => k === startKey)
    .takeUntil((_, k) => k === endKey)
    .concat([[endKey, blockMap.get(endKey)]]);
}

/**
 * Function returns collection of currently selected blocks.
 */
export function getSelectedBlocksList(editorState) {
  return getSelectedBlocksMap(editorState).toList();
}

/**
 * Function returns the first selected block.
 */
export function getSelectedBlock(editorState) {
  if (editorState) {
    return getSelectedBlocksList(editorState).get(0);
  }
  return undefined;
}

export function getSelectedBlocksType(editorState) {
  const blocks = getSelectedBlocksList(editorState);
  const hasMultipleBlockTypes = blocks.some((block) => block.type !== blocks.get(0).type);
  if (!hasMultipleBlockTypes) {
    return blocks.get(0).type;
  }
  return undefined;
}

/**
 * Function will return currently selected blocks meta data.
 */
export function getSelectedBlocksMetadata(editorState) {
  let metaData = new Map({});
  const selectedBlocks = getSelectedBlocksList(editorState);
  if (selectedBlocks && selectedBlocks.size > 0) {
    for (let i = 0; i < selectedBlocks.size; i += 1) {
      const data = selectedBlocks.get(i).getData();
      if (!data || data.size === 0) {
        metaData = metaData.clear();
        break;
      }
      if (i === 0) {
        metaData = data;
      } else {
        const array = metaData;
        metaData = array.filter((el, index) => !data.get(index) || data.get(index) !== el);
        if (metaData.size === 0) {
          metaData = metaData.clear();
          break;
        }
      }
    }
  }
  return metaData;
}

/**
 * Function will add block level meta-data.
 */
export function setBlockData(editorState, data) {
  const newContentState = Modifier.mergeBlockData(
    editorState.getCurrentContent(),
    editorState.getSelection(),
    data
  );
  return EditorState.push(editorState, newContentState, "change-block-data");
}

/**
 * Function to check if a block is of type list.
 */
export function isListBlock(block) {
  if (block) {
    const blockType = block.getType();
    return blockType === "unordered-list-item" || blockType === "ordered-list-item";
  }
  return false;
}

/**
 * Function returns the block just before the selected block.
 */
export function getBlockBeforeSelectedBlock(editorState) {
  if (editorState) {
    const selectedBlock = getSelectedBlock(editorState);
    const contentState = editorState.getCurrentContent();
    const blockList = contentState.getBlockMap().toSeq().toList();
    let previousIndex = 0;
    blockList.forEach((block, index) => {
      if (block.get("key") === selectedBlock.get("key")) {
        previousIndex = index - 1;
      }
    });
    if (previousIndex > -1) {
      return blockList.get(previousIndex);
    }
  }
  return undefined;
}

function getBlocks(editorState, adjustment, maxDepth) {
  return getSelectedBlocksMap(editorState).map((block) => {
    let depth = block.getDepth() + adjustment;
    depth = Math.max(0, Math.min(depth, maxDepth));
    return block.set("depth", depth);
  });
}

/**
 * Function to change depth of block(s).
 */
function changeBlocksDepth(editorState, adjustment, maxDepth) {
  const selectionState = editorState.getSelection();
  const contentState = editorState.getCurrentContent();
  let blockMap = contentState.getBlockMap();
  const blocks = getBlocks(editorState, adjustment, maxDepth);
  blockMap = blockMap.merge(blocks);
  return contentState.merge({
    blockMap,
    selectionBefore: selectionState,
    selectionAfter: selectionState,
  });
}

/**
 * Function will check various conditions for changing depth and will accordingly
 * either call function changeBlocksDepth or just return the call.
 */
export function changeDepth(editorState, adjustment, maxDepth) {
  const selection = editorState.getSelection();
  let key;
  if (selection.getIsBackward()) {
    key = selection.getFocusKey();
  } else {
    key = selection.getAnchorKey();
  }
  const content = editorState.getCurrentContent();
  const block = content.getBlockForKey(key);
  const type = block.getType();
  if (type !== "unordered-list-item" && type !== "ordered-list-item") {
    return editorState;
  }
  const blockAbove = content.getBlockBefore(key);
  if (!blockAbove) {
    return editorState;
  }
  const typeAbove = blockAbove.getType();
  if (typeAbove !== type) {
    return editorState;
  }
  const depth = block.getDepth();
  if (adjustment === 1 && depth === maxDepth) {
    return editorState;
  }
  const adjustedMaxDepth = Math.min(blockAbove.getDepth() + 1, maxDepth);
  const withAdjustment = changeBlocksDepth(editorState, adjustment, adjustedMaxDepth);
  return EditorState.push(editorState, withAdjustment, "adjust-depth");
}

function getInlineStyles(editorState) {
  const inlineStyles = {};
  const styleList = editorState.getCurrentInlineStyle().toList().toJS();
  if (styleList) {
    ["BOLD", "ITALIC", "UNDERLINE", "STRIKETHROUGH", "CODE", "SUPERSCRIPT", "SUBSCRIPT"].forEach(
      (style) => {
        inlineStyles[style] = styleList.indexOf(style) >= 0;
      }
    );
    return inlineStyles;
  }
  return inlineStyles;
}

function getInlineStylesBlocks(currentSelection, editorState) {
  const start = currentSelection.getStartOffset();
  const end = currentSelection.getEndOffset();
  const selectedBlocks = getSelectedBlocksList(editorState);
  const inlineStyles = {
    BOLD: true,
    ITALIC: true,
    UNDERLINE: true,
    STRIKETHROUGH: true,
    CODE: true,
    SUPERSCRIPT: true,
    SUBSCRIPT: true,
  };
  if (selectedBlocks.size > 0) {
    for (let i = 0; i < selectedBlocks.size; i += 1) {
      let blockStart = i === 0 ? start : 0;
      let blockEnd = i === selectedBlocks.size - 1 ? end : selectedBlocks.get(i).getText().length;
      if (blockStart === blockEnd && blockStart === 0) {
        blockStart = 1;
        blockEnd = 2;
      } else if (blockStart === blockEnd) {
        blockStart -= 1;
      }
      for (let j = blockStart; j < blockEnd; j += 1) {
        const inlineStylesAtOffset = selectedBlocks.get(i).getInlineStyleAt(j);
        [
          "BOLD",
          "ITALIC",
          "UNDERLINE",
          "STRIKETHROUGH",
          "CODE",
          "SUPERSCRIPT",
          "SUBSCRIPT",
        ].forEach((style) => {
          inlineStyles[style] = inlineStyles[style] && inlineStylesAtOffset.get(style) === style;
        });
      }
    }
    return inlineStyles;
  }
  return inlineStyles;
}

export function getSelectionInlineStyle(editorState) {
  const currentSelection = editorState.getSelection();
  if (currentSelection.isCollapsed()) {
    getInlineStyles(editorState);
  }
  getInlineStylesBlocks(currentSelection, editorState);
  return {};
}

/**
 * Set style.
 */
const addToCustomStyleMap = (styleType, styleKey, style) => {
  customInlineStylesMap[styleType][`${styleType.toLowerCase()}-${style}`] = {
    [styleKey]: style,
  };
};

/**
 * Combined map of all custon inline styles used to initialize editor.
 */
export const getCustomStyleMap = () => ({
  ...customInlineStylesMap.color,
  ...customInlineStylesMap.bgcolor,
  ...customInlineStylesMap.fontSize,
  ...customInlineStylesMap.fontFamily,
  CODE: customInlineStylesMap.CODE,
  SUPERSCRIPT: customInlineStylesMap.SUPERSCRIPT,
  SUBSCRIPT: customInlineStylesMap.SUBSCRIPT,
});

function getContentState(styleType, selection, editorState) {
  return Object.keys(customInlineStylesMap[styleType]).reduce(
    (contentState, s) => Modifier.removeInlineStyle(contentState, selection, s),
    editorState.getCurrentContent()
  );
}

function reduceCurrentStyle(currentStyle, nextEditorState) {
  return currentStyle.reduce((state, s) => RichUtils.toggleInlineStyle(state, s), nextEditorState);
}

function toggleCurrentStyle(nextEditorState, styleType, style) {
  return RichUtils.toggleInlineStyle(nextEditorState, `${styleType.toLowerCase()}-${style}`);
}

/**
 * Function to toggle a custom inline style in current selection current selection.
 */
export function toggleCustomInlineStyle(editorState, styleType, style) {
  const selection = editorState.getSelection();
  const nextContentState = getContentState(styleType, selection, editorState);
  const currentStyle = editorState.getCurrentInlineStyle();
  let nextEditorState = EditorState.push(editorState, nextContentState, "change-inline-style");

  if (selection.isCollapsed()) {
    nextEditorState = reduceCurrentStyle(currentStyle, nextEditorState);
  }

  if (styleType === constants.SUPERSCRIPT || styleType === constants.SUBSCRIPT) {
    if (!currentStyle.has(style)) {
      nextEditorState = RichUtils.toggleInlineStyle(nextEditorState, style);
    }
  } else {
    const styleKey = styleType === constants.bgcolor ? constants.backgroundColor : styleType;
    if (!currentStyle.has(`${styleType}-${style}`)) {
      nextEditorState = toggleCurrentStyle(nextEditorState, styleType, style);
      addToCustomStyleMap(styleType, styleKey, style);
    }
  }

  return nextEditorState;
}

/**
 * Function returns size at a offset.
 */
function getStyleAtOffset(block, stylePrefix, offset) {
  const styles = block.getInlineStyleAt(offset).toList();
  const style = styles.filter((s) => s.startsWith(stylePrefix.toLowerCase()));
  if (style && style.size > 0) {
    return style.get(0);
  }
  return undefined;
}

/**
 * Function returns size at a offset.
 */
function getCurrentInlineStyle(editorState, stylePrefix) {
  const styles = editorState.getCurrentInlineStyle().toList();
  const style = styles.filter((s) => s.startsWith(stylePrefix.toLowerCase()));
  if (style && style.size > 0) {
    return style.get(0);
  }
  return undefined;
}

function collapsedStyles(styles, editorState) {
  const inlineStyles = {};
  styles.forEach((s) => {
    inlineStyles[s] = getCurrentInlineStyle(editorState, s);
  });
  return inlineStyles;
}

function arrayStylesSelectedBlocks(blockStart, blockEnd, styles, i, selectedBlocks) {
  const inlineStyles = {};
  for (let j = blockStart; j < blockEnd; j += 1) {
    if (j === blockStart) {
      styles.forEach((s) => {
        inlineStyles[s] = getStyleAtOffset(selectedBlocks.get(i), s, j);
      });
    } else {
      styles.forEach((s) => {
        if (inlineStyles[s] && inlineStyles[s] !== getStyleAtOffset(selectedBlocks.get(i), s, j)) {
          inlineStyles[s] = undefined;
        }
      });
    }
  }
  return inlineStyles;
}

function newStylesSelectedBlocks(currentSelection, editorState, styles) {
  const inlineStyles = {};
  const start = currentSelection.getStartOffset();
  const end = currentSelection.getEndOffset();
  const selectedBlocks = getSelectedBlocksList(editorState);
  if (selectedBlocks.size > 0) {
    for (let i = 0; i < selectedBlocks.size; i += 1) {
      let blockStart = i === 0 ? start : 0;
      let blockEnd = i === selectedBlocks.size - 1 ? end : selectedBlocks.get(i).getText().length;
      if (blockStart === blockEnd && blockStart === 0) {
        blockStart = 1;
        blockEnd = 2;
      } else if (blockStart === blockEnd) {
        blockStart -= 1;
      }
      arrayStylesSelectedBlocks(blockStart, blockEnd, styles, i, selectedBlocks);
    }
    return inlineStyles;
  }
  return inlineStyles;
}
/**
 * Function returns an object of custom inline styles currently applicable.
 */
export function getSelectionCustomInlineStyle(editorState, styles) {
  if (editorState && styles && styles.length > 0) {
    const currentSelection = editorState.getSelection();
    if (currentSelection.isCollapsed()) {
      return collapsedStyles(styles, editorState);
    }
    return newStylesSelectedBlocks(currentSelection, editorState, styles);
  }
  return {};
}

/**
 * Function to remove all inline styles applied to the selection.
 */
export function removeAllInlineStyles(editorState) {
  const currentStyles = editorState.getCurrentInlineStyle();
  let contentState = editorState.getCurrentContent();
  currentStyles.forEach((style) => {
    contentState = Modifier.removeInlineStyle(contentState, editorState.getSelection(), style);
  });
  return EditorState.push(editorState, contentState, "change-inline-style");
}
