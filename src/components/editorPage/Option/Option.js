import classNames from "classnames";
import PropTypes from "prop-types";

import styles from "../../../../scss/components/editorPage/Option.module.scss";

const Option = ({ disabled, onClick, value, children, className, active }) => {
  const onClickOption = () => {
    if (!disabled) {
      onClick(value);
    }
  };
  return (
    <div
      className={classNames(styles.wrapper, className, {
        [styles.active]: active,
        [styles.disabled]: disabled,
      })}
      onClick={onClickOption}
      onKeyUp={onClickOption}
      role="button"
      tabIndex="0"
    >
      {children}
    </div>
  );
};

Option.propTypes = {
  children: PropTypes.node.isRequired,
  onClick: PropTypes.func.isRequired,
  value: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]).isRequired,
  className: PropTypes.string,
  active: PropTypes.bool,
  disabled: PropTypes.bool,
};

Option.defaultProps = {
  className: "",
  active: false,
  disabled: false,
};

export default Option;
