import React, { Component } from "react";
import classNames from "classnames";
import PropTypes from "prop-types";

import styles from "../../../../scss/components/editorPage/Dropdown.module.scss";

class Dropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      highlighted: -1,
    };
  }

  componentDidUpdate(prevProps) {
    const { expanded } = this.props;
    if (prevProps.expanded && !expanded) {
      this.handleHighlighted();
    }
  }

  handleHighlighted = () => {
    this.setState({
      highlighted: -1,
    });
  };

  onChange = (value) => {
    const { onChange } = this.props;
    if (onChange) {
      onChange(value);
    }
    this.toggleExpansion();
  };

  setHighlighted = (highlighted) => {
    this.setState({
      highlighted,
    });
  };

  toggleExpansion = () => {
    const { doExpand, doCollapse, expanded } = this.props;
    if (expanded) {
      doCollapse();
    } else {
      doExpand();
    }
  };

  render() {
    const { expanded, children, onExpandEvent, className } = this.props;
    const { highlighted } = this.state;
    const options = children.slice(1, children.length);

    return (
      <div className={classNames(styles.wrapper, className)}>
        <button type="button" className={styles.selectedText} onClick={onExpandEvent}>
          {children[0]}
          <div
            className={classNames({
              [styles.close]: expanded,
              [styles.open]: !expanded,
            })}
          />
        </button>
        {expanded ? (
          <ul className={styles.optionWrapper}>
            {React.Children.map(options, (option, index) => {
              return (
                option &&
                React.cloneElement(option, {
                  onSelect: this.onChange,
                  highlighted: highlighted === index,
                  setHighlighted: this.setHighlighted,
                  index,
                })
              );
            })}
          </ul>
        ) : undefined}
      </div>
    );
  }
}

Dropdown.propTypes = {
  children: PropTypes.node.isRequired,
  onChange: PropTypes.func.isRequired,
  expanded: PropTypes.bool.isRequired,
  onExpandEvent: PropTypes.func.isRequired,
  doExpand: PropTypes.func.isRequired,
  doCollapse: PropTypes.func.isRequired,
  className: PropTypes.string,
};

Dropdown.defaultProps = {
  className: "",
};

export default Dropdown;
