import PropTypes from "prop-types";

const SizeInput = ({ valueText, onChange, name, typeInput }) => (
  <div>
    <span>{name}</span>
    <input type={typeInput} name={name} onChange={onChange} value={valueText} />
  </div>
);

SizeInput.defaultProps = {
  valueText: "",
  name: "",
  typeInput: "",
  onChange: () => {},
};

SizeInput.propTypes = {
  valueText: PropTypes.string,
  name: PropTypes.string,
  typeInput: PropTypes.string,
  onChange: PropTypes.func,
};

export default SizeInput;
