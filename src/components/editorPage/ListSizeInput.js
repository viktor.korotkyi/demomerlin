const ListSizeInput = [
  {
    valueText: "widthBlock",
    name: "width",
    typeInput: "text",
  },
  {
    valueText: "heightBlock",
    name: "height",
    typeInput: "text",
  },
];

export default ListSizeInput;
