export default class ModalHandler {
  callBacks = [];

  suggestionCallback = undefined;

  editorFlag = false;

  suggestionFlag = false;

  closeAllModals = (event) => {
    this.callBacks.forEach((callBack) => {
      callBack(event);
    });
  };

  init = (wrapperId) => {
    const wrapper = document.getElementById(wrapperId);
    if (wrapper) {
      wrapper.addEventListener("click", () => {
        this.editorFlag = true;
      });
    }
    if (document) {
      document.addEventListener("click", () => {
        if (!this.editorFlag) {
          this.closeAllModals();
          if (this.suggestionCallback) {
            this.suggestionCallback();
          }
        } else {
          this.editorFlag = false;
        }
      });
    }
  };

  onEditorClick = () => {
    this.closeModals();
    if (!this.suggestionFlag && this.suggestionCallback) {
      this.suggestionCallback();
    } else {
      this.suggestionFlag = false;
    }
  };

  closeModals = (event) => {
    this.closeAllModals(event);
  };

  registerCallBack = (callBack) => {
    this.callBacks.push(callBack);
  };

  deregisterCallBack = (callBack) => {
    this.callBacks = this.callBacks.filter((cb) => cb !== callBack);
  };

  setSuggestionCallback = (callBack) => {
    this.suggestionCallback = callBack;
  };

  removeSuggestionCallback = () => {
    this.suggestionCallback = undefined;
  };

  onSuggestionClick = () => {
    this.suggestionFlag = true;
  };
}
