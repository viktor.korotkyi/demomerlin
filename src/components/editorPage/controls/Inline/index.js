import React, { Component } from "react";
import PropTypes from "prop-types";
import { RichUtils, EditorState, Modifier } from "draft-js";
import { getSelectionInlineStyle } from "../../utils/draft-js";
import constants from "../../../../constants/editor";

import InlineComponent from "./Component";

class Inline extends Component {
  constructor(props) {
    super(props);
    const { editorState } = this.props;
    this.state = {
      currentStyles: editorState ? this.changeKeys(getSelectionInlineStyle(editorState)) : {},
    };
  }

  componentDidUpdate(prevProps) {
    const { editorState } = this.props;
    if (editorState && editorState !== prevProps.editorState) {
      this.setCurrentStyles(editorState);
    }
  }

  setCurrentStyles = (editorState) => {
    this.setState({
      currentStyles: this.changeKeys(getSelectionInlineStyle(editorState)),
    });
  };

  toggleInlineStyle = (style) => {
    const newStyle = style === constants.monospace ? constants.code : style.toUpperCase();
    const { editorState, onChange } = this.props;
    let newState = RichUtils.toggleInlineStyle(editorState, newStyle);
    if (style === constants.subscript || style === constants.superscript) {
      const removeStyle =
        style === constants.subscript ? constants.SUPERSCRIPT : constants.SUBSCRIPT;
      const contentState = Modifier.removeInlineStyle(
        newState.getCurrentContent(),
        newState.getSelection(),
        removeStyle
      );
      newState = EditorState.push(newState, contentState, "change-inline-style");
    }
    if (newState) {
      onChange(newState);
    }
  };

  changeKeys = (style) => {
    if (style) {
      const st = {};
      Object.entries(style).forEach(([key, value]) => {
        st[key === "CODE" ? "monospace" : key.toLowerCase()] = value;
      });
      return st;
    }
    return undefined;
  };

  render() {
    const { config } = this.props;
    const { currentStyles } = this.state;

    return (
      <InlineComponent
        config={config}
        currentState={currentStyles}
        onChange={this.toggleInlineStyle}
      />
    );
  }
}

Inline.propTypes = {
  onChange: PropTypes.func.isRequired,
  editorState: PropTypes.objectOf(PropTypes.object).isRequired,
  config: PropTypes.shape({
    inDropdown: PropTypes.bool,
  }).isRequired,
};

export default Inline;
