import PropTypes from "prop-types";
import Option from "../../../Option/Option";

import styles from "../../../../../../scss/components/editorPage/controls/Inline.module.scss";

const InlineComponent = ({ config, currentState, onChange }) => (
  <div className={styles.wrapper}>
    {config.options.map((style, index) => (
      <Option
        key={[index]}
        value={style}
        onClick={onChange}
        active={currentState[style] === true || (style === "MONOSPACE" && currentState.CODE)}
      >
        <img alt="" src={config[style].icon} />
      </Option>
    ))}
  </div>
);

InlineComponent.propTypes = {
  onChange: PropTypes.func.isRequired,
  config: PropTypes.shape({
    options: PropTypes.arrayOf(PropTypes.string),
  }).isRequired,
  currentState: PropTypes.shape({
    CODE: PropTypes.bool,
  }).isRequired,
};

export default InlineComponent;
