import React, { Component } from "react";
import PropTypes from "prop-types";
import FontFamilyComponent from "./Component";
import { getSelectionCustomInlineStyle, toggleCustomInlineStyle } from "../../utils/draft-js";

class FontFamily extends Component {
  constructor(props) {
    super(props);
    const { editorState, modalHandler } = props;
    this.signalExpanded = false;
    this.state = {
      expanded: false,
      currentFontFamily: editorState
        ? getSelectionCustomInlineStyle(editorState, ["FONTFAMILY"]).FONTFAMILY
        : undefined,
    };
    modalHandler.registerCallBack(this.expandCollapse);
  }

  componentDidUpdate(prevProps) {
    const { editorState } = this.props;
    if (editorState && editorState !== prevProps.editorState) {
      this.setCurrentFontFamily(editorState);
    }
  }

  componentWillUnmount() {
    const { modalHandler } = this.props;
    modalHandler.deregisterCallBack(this.expandCollapse);
  }

  setCurrentFontFamily = (editorState) => {
    this.setState({
      currentFontFamily: getSelectionCustomInlineStyle(editorState, ["FONTFAMILY"]).FONTFAMILY,
    });
  };

  onExpandEvent = () => {
    const { expanded } = this.state;
    this.signalExpanded = !expanded;
    this.setState({
      expanded: !expanded,
    });
  };

  expandCollapse = () => {
    this.setState({
      expanded: this.signalExpanded,
    });
    this.signalExpanded = false;
  };

  doExpand = () => {
    this.setState({
      expanded: true,
    });
  };

  doCollapse = () => {
    this.setState({
      expanded: false,
    });
  };

  toggleFontFamily = (fontFamily) => {
    const { editorState, onChange } = this.props;
    const newState = toggleCustomInlineStyle(editorState, "fontFamily", fontFamily);
    if (newState) {
      onChange(newState);
    }
  };

  render() {
    const { config } = this.props;
    const { expanded, currentFontFamily } = this.state;
    const fontFamily = currentFontFamily && currentFontFamily.substring(11);
    return (
      <FontFamilyComponent
        config={config}
        currentState={{ fontFamily }}
        onChange={this.toggleFontFamily}
        expanded={expanded}
        onExpandEvent={this.onExpandEvent}
        doExpand={this.doExpand}
        doCollapse={this.doCollapse}
      />
    );
  }
}

FontFamily.propTypes = {
  onChange: PropTypes.func.isRequired,
  editorState: PropTypes.objectOf(PropTypes.object).isRequired,
  modalHandler: PropTypes.shape({
    callBacks: PropTypes.arrayOf(PropTypes.func),
    registerCallBack: PropTypes.func,
    deregisterCallBack: PropTypes.func,
  }).isRequired,
  config: PropTypes.shape({
    options: PropTypes.arrayOf(PropTypes.string),
  }).isRequired,
};

export default FontFamily;
