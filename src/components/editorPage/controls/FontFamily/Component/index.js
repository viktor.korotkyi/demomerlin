import React, { Component } from "react";
import PropTypes from "prop-types";

import Dropdown from "../../../Dropdown/Dropdown";
import DropdownOption from "../../../DropdownOption/DropdownOption";

import styles from "../../../../../../scss/components/editorPage/controls/FontFamily.module.scss";

class FontFamilyComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      defaultFontFamily: undefined,
    };
  }

  componentDidMount() {
    const editorElm = document.getElementsByClassName("DraftEditor-root");
    if (editorElm && editorElm.length > 0) {
      const editorStyles = window.getComputedStyle(editorElm[0]);
      const defaultFontFamily = editorStyles.getPropertyValue("font-family");
      this.setState({
        defaultFontFamily,
      });
    }
  }

  render() {
    const { defaultFontFamily } = this.state;
    const {
      config: { options },
      onChange,
      expanded,
      doCollapse,
      onExpandEvent,
      doExpand,
    } = this.props;
    let {
      currentState: { fontFamily: currentFontFamily },
    } = this.props;
    currentFontFamily =
      currentFontFamily ||
      (options &&
        defaultFontFamily &&
        options.some((opt) => opt.toLowerCase() === defaultFontFamily.toLowerCase()) &&
        defaultFontFamily);

    return (
      <div className={styles.wrapper}>
        <Dropdown
          className={styles.dropdown}
          optionWrapperClassName={styles.optionwrapper}
          onChange={onChange}
          expanded={expanded}
          doExpand={doExpand}
          doCollapse={doCollapse}
          onExpandEvent={onExpandEvent}
        >
          <span className={styles.placeholder}>{currentFontFamily}</span>
          {options.map((family, index) => (
            <DropdownOption active={currentFontFamily === family} value={family} key={[index]}>
              {family}
            </DropdownOption>
          ))}
        </Dropdown>
      </div>
    );
  }
}

FontFamilyComponent.propTypes = {
  expanded: PropTypes.bool.isRequired,
  onExpandEvent: PropTypes.func.isRequired,
  doExpand: PropTypes.func.isRequired,
  doCollapse: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  config: PropTypes.shape({
    options: PropTypes.arrayOf(PropTypes.string),
  }).isRequired,
  currentState: PropTypes.objectOf(PropTypes.object).isRequired,
};

export default FontFamilyComponent;
