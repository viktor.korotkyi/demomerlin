import React, { Component } from "react";
import PropTypes from "prop-types";
import MarginComponent from "./Component";
import { getSelectedBlock, getSelectedBlocksMetadata, setBlockData } from "../../utils/draft-js";

class Margin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentMargin: undefined,
    };
  }

  componentDidMount() {
    const { editorState } = this.props;
    const data = getSelectedBlocksMetadata(editorState).get("margin");
    if (data) {
      this.setState({
        currentMargin: data,
      });
    }
  }

  componentDidUpdate(prevProps) {
    const { editorState } = this.props;
    if (editorState !== prevProps.editorState) {
      this.setCurrentMatgin(editorState);
    }
  }

  setCurrentMatgin = (editorState) => {
    this.setState({
      currentMargin: getSelectedBlocksMetadata(editorState).get("margin"),
    });
  };

  toggleMargin = (margin) => {
    const { editorState, onChange } = this.props;
    const selectedBlock = getSelectedBlock(editorState);
    const blockDom = document.querySelector(`[data-offset-key='${selectedBlock.getKey()}-0-0']`);
    margin.forEach((item) => {
      blockDom.style[item.type] = `${item.value}px`;
    });
    onChange(setBlockData(editorState, { margin }));
  };

  render() {
    const { currentMargin } = this.state;

    return <MarginComponent currentState={currentMargin} onChange={this.toggleMargin} />;
  }
}

Margin.propTypes = {
  onChange: PropTypes.func.isRequired,
  editorState: PropTypes.objectOf(PropTypes.object).isRequired,
};

export default Margin;
