import React, { Component } from "react";
import PropTypes from "prop-types";
import { List } from "immutable";

import styles from "../../../../../../scss/components/editorPage/controls/Margin.module.scss";

class MarginComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      defaultMarginTop: 0,
      defaultMarginRight: 0,
      defaultMarginLeft: 0,
      defaultMarginBottom: 0,
    };
    this.handleChange = this.handleChange.bind(this);
    this.set = this.set.bind(this);
  }

  componentDidUpdate(prevProps) {
    const { currentState } = this.props;
    if (currentState !== prevProps.currentState && currentState) {
      this.setMargin(currentState);
    } else if (currentState !== prevProps.currentState && !currentState) {
      this.setDefaultMargin();
    }
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  setMargin = (currentState) => {
    const margin = List.isList(currentState) ? currentState.toJSON() : currentState;
    this.setState({
      defaultMarginTop: margin[0].value,
      defaultMarginRight: margin[1].value,
      defaultMarginLeft: margin[2].value,
      defaultMarginBottom: margin[3].value,
    });
  };

  setDefaultMargin = () => {
    this.setState({
      defaultMarginTop: 0,
      defaultMarginRight: 0,
      defaultMarginLeft: 0,
      defaultMarginBottom: 0,
    });
  };

  set(e) {
    e.preventDefault();
    const { onChange } = this.props;
    const {
      defaultMarginTop,
      defaultMarginRight,
      defaultMarginLeft,
      defaultMarginBottom,
    } = this.state;
    onChange([
      { type: "marginTop", value: defaultMarginTop },
      { type: "marginRight", value: defaultMarginRight },
      { type: "marginLeft", value: defaultMarginLeft },
      { type: "marginBottom", value: defaultMarginBottom },
    ]);
  }

  render() {
    const {
      defaultMarginTop,
      defaultMarginRight,
      defaultMarginLeft,
      defaultMarginBottom,
    } = this.state;

    return (
      <div className={styles.wrapper}>
        <div>
          <span>Margin Top</span>
          <input
            type="text"
            name="defaultMarginTop"
            placeholder="marginTop"
            value={defaultMarginTop}
            onChange={(e) => this.handleChange(e)}
          />
        </div>
        <div>
          <span>Margin Right</span>
          <input
            type="text"
            name="defaultMarginRight"
            placeholder="marginRight"
            value={defaultMarginRight}
            onChange={(e) => this.handleChange(e)}
          />
        </div>
        <div>
          <span>Margin Left</span>
          <input
            type="text"
            name="defaultMarginLeft"
            placeholder="marginLeft"
            value={defaultMarginLeft}
            onChange={(e) => this.handleChange(e)}
          />
        </div>
        <div>
          <span>Margin Bottom</span>
          <input
            type="text"
            name="defaultMarginBottom"
            placeholder="marginBottom"
            value={defaultMarginBottom}
            onChange={(e) => this.handleChange(e)}
          />
        </div>
        <button type="button" onClick={this.set}>
          set
        </button>
      </div>
    );
  }
}

MarginComponent.propTypes = {
  onChange: PropTypes.func.isRequired,
  currentState: PropTypes.arrayOf(PropTypes.object),
};

MarginComponent.defaultProps = {
  currentState: undefined,
};

export default MarginComponent;
