import React, { Component } from "react";
import PropTypes from "prop-types";
import Option from "../../../Option/Option";

import styles from "../../../../../../scss/components/editorPage/controls/History.module.scss";

class HistoryComponent extends Component {
  onChange = (obj) => {
    const { onChange } = this.props;
    onChange(obj);
  };

  render() {
    const {
      config: { options, undo, redo },
      currentState: { undoDisabled, redoDisabled },
    } = this.props;

    return (
      <div className={styles.wrapper}>
        {options.indexOf("undo") >= 0 && (
          <Option value="undo" onClick={this.onChange} disabled={undoDisabled}>
            <img src={undo.icon} alt="" />
          </Option>
        )}
        {options.indexOf("redo") >= 0 && (
          <Option value="redo" onClick={this.onChange} disabled={redoDisabled}>
            <img src={redo.icon} alt="" />
          </Option>
        )}
      </div>
    );
  }
}

HistoryComponent.propTypes = {
  onChange: PropTypes.func.isRequired,
  config: PropTypes.shape({
    options: PropTypes.arrayOf(PropTypes.string),
    undo: PropTypes.shape({
      icon: PropTypes.string,
    }),
    redo: PropTypes.shape({
      icon: PropTypes.string,
    }),
  }).isRequired,
  currentState: PropTypes.shape({
    undoDisabled: PropTypes.bool,
    redoDisabled: PropTypes.bool,
  }).isRequired,
};

export default HistoryComponent;
