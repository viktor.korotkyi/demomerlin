import React, { Component } from "react";
import { EditorState } from "draft-js";
import PropTypes from "prop-types";
import HistoryComponent from "./Component";

class History extends Component {
  constructor(props) {
    super(props);
    const state = {
      expanded: false,
      undoDisabled: false,
      redoDisabled: false,
    };
    const { editorState } = props;
    if (editorState) {
      state.undoDisabled = editorState.getUndoStack().size === 0;
      state.redoDisabled = editorState.getRedoStack().size === 0;
    }
    this.state = state;
  }

  componentDidUpdate(prevProps) {
    const { editorState } = this.props;
    if (editorState && prevProps.editorState !== editorState) {
      this.setDisableModal(editorState);
    }
  }

  setDisableModal = (editorState) => {
    this.setState({
      undoDisabled: editorState.getUndoStack().size === 0,
      redoDisabled: editorState.getRedoStack().size === 0,
    });
  };

  onChange = (action) => {
    const { editorState, onChange } = this.props;
    const newState = EditorState[action](editorState);
    if (newState) {
      onChange(newState);
    }
  };

  render() {
    const { config } = this.props;
    const { undoDisabled, redoDisabled } = this.state;

    return (
      <HistoryComponent
        config={config}
        currentState={{ undoDisabled, redoDisabled }}
        onChange={this.onChange}
      />
    );
  }
}

History.propTypes = {
  onChange: PropTypes.func.isRequired,
  editorState: PropTypes.objectOf(PropTypes.object).isRequired,
  config: PropTypes.shape({
    options: PropTypes.arrayOf(PropTypes.string),
  }).isRequired,
};

export default History;
