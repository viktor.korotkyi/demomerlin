import textAlign from "./TextAlign";
import blockType from "./BlockType";
import fontSize from "./FontSize";
import fontFamily from "./FontFamily";
import inline from "./Inline";
import image from "./Image";
import history from "./History";
import margin from "./Margin";
import required from "./Required";

export default {
  inline,
  textAlign,
  blockType,
  fontSize,
  fontFamily,
  image,
  history,
  margin,
  required,
};
