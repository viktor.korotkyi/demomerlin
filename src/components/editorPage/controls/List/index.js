import React, { Component } from "react";
import PropTypes from "prop-types";
import { RichUtils } from "draft-js";

import ListComponent from "./Component";
import { getSelectedBlock } from "../../utils/draft-js";

class List extends Component {
  constructor(props) {
    super(props);
    const { editorState } = this.props;
    this.state = {
      currentBlock: editorState ? getSelectedBlock(editorState) : undefined,
    };
  }

  componentDidUpdate(prevProps) {
    const { editorState } = this.props;
    if (editorState && editorState !== prevProps.editorState) {
      this.setCurrentBlock(editorState);
    }
  }

  setCurrentBlock = (editorState) => {
    this.setState({ currentBlock: getSelectedBlock(editorState) });
  };

  onChange = (value) => {
    if (value === "unordered") {
      this.toggleBlockType("unordered-list-item");
    } else if (value === "ordered") {
      this.toggleBlockType("ordered-list-item");
    }
  };

  toggleBlockType = (blockType) => {
    const { onChange, editorState } = this.props;
    const newState = RichUtils.toggleBlockType(editorState, blockType);
    if (newState) {
      onChange(newState);
    }
  };

  render() {
    const { config } = this.props;
    const { currentBlock } = this.state;
    let listType;
    if (currentBlock.get("type") === "unordered-list-item") {
      listType = "unordered";
    } else if (currentBlock.get("type") === "ordered-list-item") {
      listType = "ordered";
    }

    return <ListComponent config={config} currentState={{ listType }} onChange={this.onChange} />;
  }
}

List.propTypes = {
  onChange: PropTypes.func.isRequired,
  editorState: PropTypes.objectOf(PropTypes.object).isRequired,
  config: PropTypes.shape({
    options: PropTypes.arrayOf(PropTypes.string),
  }).isRequired,
};

export default List;
