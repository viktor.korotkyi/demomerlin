import React, { Component } from "react";
import PropTypes from "prop-types";
import Option from "../../../Option/Option";

import styles from "../../../../../../scss/components/editorPage/controls/List.module.scss";

class ListComponent extends Component {
  toggleBlockType = (blockType) => {
    const { onChange } = this.props;
    onChange(blockType);
  };

  render() {
    const {
      config: { options, unordered, ordered },
      currentState: { listType },
    } = this.props;

    return (
      <div className={styles.wrapper}>
        {options.indexOf("unordered") >= 0 && (
          <Option
            value="unordered"
            onClick={this.toggleBlockType}
            active={listType === "unordered"}
          >
            <img src={unordered.icon} alt="" />
          </Option>
        )}
        {options.indexOf("ordered") >= 0 && (
          <Option value="ordered" onClick={this.toggleBlockType} active={listType === "ordered"}>
            <img src={ordered.icon} alt="" />
          </Option>
        )}
      </div>
    );
  }
}

ListComponent.propTypes = {
  onChange: PropTypes.func.isRequired,
  config: PropTypes.shape({
    options: PropTypes.arrayOf(PropTypes.string),
    unordered: PropTypes.shape({
      icon: PropTypes.string,
    }),
    ordered: PropTypes.shape({
      icon: PropTypes.string,
    }),
  }).isRequired,
  currentState: PropTypes.objectOf(PropTypes.object).isRequired,
};

export default ListComponent;
