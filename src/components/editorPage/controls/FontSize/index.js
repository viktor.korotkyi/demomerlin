import React, { Component } from "react";
import PropTypes from "prop-types";
import FontSizeComponent from "./Component";
import { getSelectionCustomInlineStyle, toggleCustomInlineStyle } from "../../utils/draft-js";

class FontSize extends Component {
  constructor(props) {
    super(props);
    const { editorState, modalHandler } = props;
    this.signalExpanded = false;
    this.state = {
      expanded: false,
      currentFontSize: editorState
        ? getSelectionCustomInlineStyle(editorState, ["FONTSIZE"]).FONTSIZE
        : undefined,
    };
    modalHandler.registerCallBack(this.expandCollapse);
  }

  componentDidUpdate(prevProps) {
    const { editorState } = this.props;
    if (editorState && editorState !== prevProps.editorState) {
      this.setCurrentFontSize(editorState);
    }
  }

  componentWillUnmount() {
    const { modalHandler } = this.props;
    modalHandler.deregisterCallBack(this.expandCollapse);
  }

  setCurrentFontSize = (editorState) => {
    this.setState({
      currentFontSize: getSelectionCustomInlineStyle(editorState, ["FONTSIZE"]).FONTSIZE,
    });
  };

  onExpandEvent = () => {
    const { expanded } = this.state;
    this.signalExpanded = !expanded;
    this.setState({
      expanded: !expanded,
    });
  };

  expandCollapse = () => {
    this.setState({
      expanded: this.signalExpanded,
    });
    this.signalExpanded = false;
  };

  doExpand = () => {
    this.setState({
      expanded: true,
    });
  };

  doCollapse = () => {
    this.setState({
      expanded: false,
    });
  };

  toggleFontSize = (fontSize) => {
    const { editorState, onChange } = this.props;
    const newState = toggleCustomInlineStyle(editorState, "fontSize", fontSize);
    if (newState) {
      onChange(newState);
    }
  };

  render() {
    const { config } = this.props;
    const { expanded, currentFontSize } = this.state;
    const fontSize = currentFontSize && Number(currentFontSize.substring(9));

    return (
      <FontSizeComponent
        config={config}
        currentState={{ fontSize }}
        onChange={this.toggleFontSize}
        expanded={expanded}
        onExpandEvent={this.onExpandEvent}
        doExpand={this.doExpand}
        doCollapse={this.doCollapse}
      />
    );
  }
}

FontSize.propTypes = {
  onChange: PropTypes.func.isRequired,
  editorState: PropTypes.objectOf(PropTypes.object).isRequired,
  modalHandler: PropTypes.shape({
    callBacks: PropTypes.arrayOf(PropTypes.func),
    registerCallBack: PropTypes.func,
    deregisterCallBack: PropTypes.func,
  }).isRequired,
  config: PropTypes.shape({
    icon: PropTypes.string,
  }).isRequired,
};

export default FontSize;
