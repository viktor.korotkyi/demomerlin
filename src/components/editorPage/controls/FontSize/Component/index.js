import React, { Component } from "react";
import PropTypes from "prop-types";

import Dropdown from "../../../Dropdown/Dropdown";
import DropdownOption from "../../../DropdownOption/DropdownOption";

import styles from "../../../../../../scss/components/editorPage/controls/FontSize.module.scss";

class FontSizeComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      defaultFontSize: undefined,
    };
  }

  componentDidMount() {
    const editorElm = document.getElementsByClassName("DraftEditor-root");
    if (editorElm && editorElm.length > 0) {
      const editorStyles = window.getComputedStyle(editorElm[0]);
      let defaultFontSize = editorStyles.getPropertyValue("font-size");
      defaultFontSize = defaultFontSize.substring(0, defaultFontSize.length - 2);
      this.setState({
        defaultFontSize,
      });
    }
  }

  render() {
    const {
      config: { icon, options },
      onChange,
      expanded,
      doCollapse,
      onExpandEvent,
      doExpand,
    } = this.props;
    let {
      currentState: { fontSize: currentFontSize },
    } = this.props;
    let { defaultFontSize } = this.state;
    defaultFontSize = Number(defaultFontSize);
    currentFontSize =
      currentFontSize || (options && options.indexOf(defaultFontSize) >= 0 && defaultFontSize);

    return (
      <div className={styles.wrapper}>
        <Dropdown
          className={styles.dropdown}
          onChange={onChange}
          expanded={expanded}
          doExpand={doExpand}
          doCollapse={doCollapse}
          onExpandEvent={onExpandEvent}
        >
          {currentFontSize ? <span>{currentFontSize}</span> : <img src={icon} alt="" />}
          {options.map((size, index) => (
            <DropdownOption
              className={styles.option}
              active={currentFontSize === size}
              value={size}
              key={[index]}
            >
              {size}
            </DropdownOption>
          ))}
        </Dropdown>
      </div>
    );
  }
}

FontSizeComponent.propTypes = {
  expanded: PropTypes.bool.isRequired,
  onExpandEvent: PropTypes.func.isRequired,
  doExpand: PropTypes.func.isRequired,
  doCollapse: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  config: PropTypes.shape({
    icon: PropTypes.string,
    options: PropTypes.arrayOf(PropTypes.number),
  }).isRequired,
  currentState: PropTypes.shape({
    fontSize: PropTypes.number,
  }).isRequired,
};

export default FontSizeComponent;
