import PropTypes from "prop-types";
import styles from "../../../../../../scss/components/editorPage/controls/RenderAddImageModal.module.scss";

const RenderAddImageModal = ({
  stopPropagation,
  handleChange,
  imgSrc,
  height,
  width,
  addImageFromState,
  doCollapse,
}) => (
  <div
    className={styles.modal}
    onClick={stopPropagation}
    onKeyUp={stopPropagation}
    role="button"
    tabIndex="0"
  >
    <div className={styles.modalUrlSection}>
      <span>Url</span>
      <input
        className={styles.modalUrlInput}
        name="imgSrc"
        onChange={handleChange}
        onBlur={handleChange}
        value={imgSrc}
      />
    </div>
    <div className={styles.modalSize}>
      <span>&#8597;</span>
      <input
        onChange={handleChange}
        onBlur={handleChange}
        value={height}
        name="height"
        className={styles.modalSizeInput}
        placeholder="Height"
      />
      <span>&#8596;</span>
      <input
        onChange={handleChange}
        onBlur={handleChange}
        value={width}
        name="width"
        className={styles.modalSizeInput}
        placeholder="Width"
      />
    </div>
    <span className={styles.modalBtnSection}>
      <button
        type="button"
        className={styles.modalBtn}
        onClick={addImageFromState}
        disabled={!imgSrc || !height || !width}
      >
        add
      </button>
      <button type="button" className={styles.modalBtn} onClick={doCollapse}>
        cancel
      </button>
    </span>
  </div>
);

RenderAddImageModal.defaultProps = {
  stopPropagation: () => {},
  handleChange: () => {},
  addImageFromState: () => {},
  doCollapse: () => {},
  imgSrc: "",
  width: 0,
  height: 0,
};

RenderAddImageModal.propTypes = {
  stopPropagation: PropTypes.func,
  handleChange: PropTypes.func,
  addImageFromState: PropTypes.func,
  doCollapse: PropTypes.func,
  imgSrc: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
};

export default RenderAddImageModal;
