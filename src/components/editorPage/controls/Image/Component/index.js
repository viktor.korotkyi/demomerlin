import React, { Component } from "react";
import PropTypes from "prop-types";
import Option from "../../../Option/Option";

import styles from "../../../../../../scss/components/editorPage/controls/Image.module.scss";
import RenderAddImageModal from "./renderAddImageModal";

class ImageComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imgSrc: "",
      height: props.config.defaultSize.height,
      width: props.config.defaultSize.width,
      alt: "",
    };
  }

  componentDidUpdate(prevProps) {
    const { config, expanded } = this.props;
    if (prevProps.expanded && !expanded) {
      this.updateState(config);
    }
  }

  updateState = (config) => {
    this.setState({
      imgSrc: "",
      height: config.defaultSize.height,
      width: config.defaultSize.width,
      alt: "",
    });
  };

  addImageFromState = () => {
    const { imgSrc, alt } = this.state;
    const { height, width } = this.state;
    const { onChange } = this.props;
    onChange(imgSrc, height, width, alt);
  };

  handleChange = (event) => {
    event.preventDefault();
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  stopPropagation = (e) => e.stopPropagation();

  render() {
    const { imgSrc, height, width } = this.state;
    const { doCollapse } = this.props;

    const {
      config: { icon },
      expanded,
      onExpandEvent,
    } = this.props;
    return (
      <div className={styles.wrapper} aria-haspopup="true">
        <Option value="unordered-list-item" onClick={onExpandEvent}>
          <img src={icon} alt="" />
        </Option>
        {expanded && (
          <RenderAddImageModal
            stopPropagation={this.stopPropagation}
            handleChange={this.handleChange}
            imgSrc={imgSrc}
            height={height}
            width={width}
            addImageFromState={this.addImageFromState}
            doCollapse={doCollapse}
          />
        )}
      </div>
    );
  }
}

ImageComponent.propTypes = {
  expanded: PropTypes.bool.isRequired,
  onExpandEvent: PropTypes.func.isRequired,
  doCollapse: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  config: PropTypes.shape({
    icon: PropTypes.string,
    defaultSize: PropTypes.shape({
      height: PropTypes.number,
      width: PropTypes.number,
    }),
  }).isRequired,
};

export default ImageComponent;
