import React, { Component } from "react";
import PropTypes from "prop-types";
import { AtomicBlockUtils, EditorState } from "draft-js";
import ImageComponent from "./Component";

class Image extends Component {
  constructor(props) {
    super(props);
    const { modalHandler } = this.props;
    this.signalExpanded = false;
    this.state = {
      expanded: false,
    };
    modalHandler.registerCallBack(this.expandCollapse);
  }

  componentWillUnmount() {
    const { modalHandler } = this.props;
    modalHandler.deregisterCallBack(this.expandCollapse);
  }

  onExpandEvent = () => {
    const { expanded } = this.state;
    this.signalExpanded = !expanded;
    this.setState({
      expanded: !expanded,
    });
  };

  doExpand = () => {
    this.setState({
      expanded: true,
    });
  };

  doCollapse = () => {
    this.setState({
      expanded: false,
    });
  };

  expandCollapse = () => {
    this.setState({
      expanded: this.signalExpanded,
    });
    this.signalExpanded = false;
  };

  addImage = (url, heightImg, widthImg) => {
    const { editorState, onChange } = this.props;
    const entityData = { src: url, height: heightImg, width: widthImg };
    const contentStateWithEntity = editorState
      .getCurrentContent()
      .createEntity("IMAGE", "IMMUTABLE", entityData);
    const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
    let newEditorState = EditorState.set(editorState, { currentContent: contentStateWithEntity });
    newEditorState = AtomicBlockUtils.insertAtomicBlock(newEditorState, entityKey, " ");
    onChange(newEditorState);
    this.doCollapse();
  };

  render() {
    const { config } = this.props;
    const { expanded } = this.state;

    return (
      <ImageComponent
        config={config}
        onChange={this.addImage}
        expanded={expanded}
        onExpandEvent={this.onExpandEvent}
        doExpand={this.doExpand}
        doCollapse={this.doCollapse}
      />
    );
  }
}

Image.propTypes = {
  onChange: PropTypes.func.isRequired,
  editorState: PropTypes.objectOf(PropTypes.object).isRequired,
  modalHandler: PropTypes.shape({
    callBacks: PropTypes.arrayOf(PropTypes.func),
    deregisterCallBack: PropTypes.func,
    registerCallBack: PropTypes.func,
  }).isRequired,
  config: PropTypes.shape({
    icon: PropTypes.string,
  }).isRequired,
};

export default Image;
