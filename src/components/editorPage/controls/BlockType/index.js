import React, { Component } from "react";
import PropTypes from "prop-types";
import { RichUtils } from "draft-js";
import constants from "../../../../constants/editor";
import BlockTypeComponent from "./Component";
import { getSelectedBlocksType } from "../../utils/draft-js";

class BlockType extends Component {
  constructor(props) {
    super(props);
    const { editorState, modalHandler } = props;
    this.signalExpanded = false;
    this.state = {
      expanded: false,
      currentBlockType: editorState ? getSelectedBlocksType(editorState) : "unstyled",
    };
    modalHandler.registerCallBack(this.expandCollapse);
    this.blocksTypes = constants.blocksTypesHeader;
  }

  componentDidUpdate(prevProps) {
    const { editorState } = this.props;
    if (editorState && editorState !== prevProps.editorState) {
      this.setCurrentBlockType(editorState);
    }
  }

  componentWillUnmount() {
    const { modalHandler } = this.props;
    modalHandler.deregisterCallBack(this.expandCollapse);
  }

  setCurrentBlockType = (editorState) => {
    this.setState({
      currentBlockType: getSelectedBlocksType(editorState),
    });
  };

  onExpandEvent = () => {
    const { expanded } = this.state;
    this.signalExpanded = !expanded;
    this.setState({
      expanded: !expanded,
    });
  };

  expandCollapse = () => {
    this.setState({
      expanded: this.signalExpanded,
    });
    this.signalExpanded = false;
  };

  doExpand = () => {
    this.setState({
      expanded: true,
    });
  };

  doCollapse = () => {
    this.setState({
      expanded: false,
    });
  };

  toggleBlockType = (blockType) => {
    const blockTypeValue = this.blocksTypes.find((bt) => bt.label === blockType).style;
    const { editorState, onChange } = this.props;
    const newState = RichUtils.toggleBlockType(editorState, blockTypeValue);
    if (newState) {
      onChange(newState);
    }
  };

  render() {
    const { config } = this.props;
    const { expanded, currentBlockType } = this.state;
    const blockType = this.blocksTypes.find((bt) => bt.style === currentBlockType);

    return (
      <BlockTypeComponent
        config={config}
        currentState={{ blockType: blockType && blockType.label }}
        onChange={this.toggleBlockType}
        expanded={expanded}
        onExpandEvent={this.onExpandEvent}
        doExpand={this.doExpand}
        doCollapse={this.doCollapse}
      />
    );
  }
}

BlockType.propTypes = {
  onChange: PropTypes.func.isRequired,
  editorState: PropTypes.objectOf(PropTypes.object).isRequired,
  modalHandler: PropTypes.shape({
    callBacks: PropTypes.arrayOf(PropTypes.func),
    deregisterCallBack: PropTypes.func,
    registerCallBack: PropTypes.func,
  }).isRequired,
  config: PropTypes.shape({
    inDropdown: PropTypes.bool,
  }).isRequired,
};

export default BlockType;
