import React, { Component } from "react";
import PropTypes from "prop-types";
import constants from "../../../../../constants/editor";
import Dropdown from "../../../Dropdown/Dropdown";
import DropdownOption from "../../../DropdownOption/DropdownOption";

import styles from "../../../../../../scss/components/editorPage/controls/BlockType.module.scss";

class BlockTypeComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      blockTypes: constants.blockTypesEditor,
    };
  }

  render() {
    const { config } = this.props;
    const { blockTypes } = this.state;
    const blocks = blockTypes.filter(({ label }) => config.options.indexOf(label) > -1);
    const {
      currentState: { blockType },
      expanded,
      doExpand,
      onExpandEvent,
      doCollapse,
      onChange,
    } = this.props;
    const currentBlockData = blockTypes.filter((blk) => blk.label === blockType);
    const currentLabel = currentBlockData && currentBlockData[0] && currentBlockData[0].displayName;

    return (
      <div className={styles.wrapper}>
        <Dropdown
          className={styles.dropdown}
          onChange={onChange}
          expanded={expanded}
          doExpand={doExpand}
          doCollapse={doCollapse}
          onExpandEvent={onExpandEvent}
        >
          <span>{currentLabel}</span>
          {blocks.map((block, index) => (
            <DropdownOption active={blockType === block.label} value={block.label} key={[index]}>
              {block.displayName}
            </DropdownOption>
          ))}
        </Dropdown>
      </div>
    );
  }
}

BlockTypeComponent.propTypes = {
  expanded: PropTypes.bool.isRequired,
  onExpandEvent: PropTypes.func.isRequired,
  doExpand: PropTypes.func.isRequired,
  doCollapse: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  config: PropTypes.shape({
    options: PropTypes.arrayOf(PropTypes.string),
  }).isRequired,
  currentState: PropTypes.shape({
    blockType: PropTypes.string,
  }).isRequired,
};

export default BlockTypeComponent;
