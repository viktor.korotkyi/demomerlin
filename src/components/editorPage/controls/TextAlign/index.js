import React, { Component } from "react";
import PropTypes from "prop-types";
import TextAlignmentComponent from "./Component";
import { getSelectedBlocksMetadata, setBlockData } from "../../utils/draft-js";

class TextAlign extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentTextAlignment: undefined,
    };
  }

  componentDidUpdate(prevProps) {
    const { editorState } = this.props;
    if (editorState !== prevProps.editorState) {
      this.setCurrentTextAlign(editorState);
    }
  }

  setCurrentTextAlign = (editorState) => {
    this.setState({
      currentTextAlignment: getSelectedBlocksMetadata(editorState).get("text-align"),
    });
  };

  addBlockAlignmentData = (value) => {
    const { editorState, onChange } = this.props;
    const { currentTextAlignment } = this.state;
    if (currentTextAlignment !== value) {
      onChange(setBlockData(editorState, { "text-align": value }));
    } else {
      onChange(setBlockData(editorState, { "text-align": undefined }));
    }
  };

  render() {
    const { config } = this.props;
    const { currentTextAlignment } = this.state;

    return (
      <TextAlignmentComponent
        config={config}
        currentState={{ textAlignment: currentTextAlignment }}
        onChange={this.addBlockAlignmentData}
      />
    );
  }
}

TextAlign.propTypes = {
  onChange: PropTypes.func.isRequired,
  editorState: PropTypes.objectOf(PropTypes.object).isRequired,
  config: PropTypes.shape({
    options: PropTypes.arrayOf(PropTypes.string),
  }).isRequired,
};

export default TextAlign;
