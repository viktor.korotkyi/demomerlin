import React, { Component } from "react";
import PropTypes from "prop-types";
import OptionList from "./OptionList";

import styles from "../../../../../../scss/components/editorPage/controls/TextAlign.module.scss";
import OptionTextAlign from "./OptionTextAlign";

class TextAlignmentComponent extends Component {
  render() {
    const { config, onChange, currentState } = this.props;

    return (
      <div className={styles.wrapper}>
        {OptionList.map((item, index) => (
          <OptionTextAlign
            key={[item, index].join("_")}
            currentState={currentState}
            onChange={onChange}
            option={config[item]}
            options={config.options}
            typeBtn={item}
          />
        ))}
      </div>
    );
  }
}

TextAlignmentComponent.propTypes = {
  onChange: PropTypes.func.isRequired,
  config: PropTypes.shape({
    options: PropTypes.arrayOf(PropTypes.string),
    left: PropTypes.shape({
      icon: PropTypes.string,
    }),
    right: PropTypes.shape({
      icon: PropTypes.string,
    }),
    center: PropTypes.shape({
      icon: PropTypes.string,
    }),
    justify: PropTypes.shape({
      icon: PropTypes.string,
    }),
  }).isRequired,
  currentState: PropTypes.shape({
    textAlignment: PropTypes.string,
  }).isRequired,
};

export default TextAlignmentComponent;
