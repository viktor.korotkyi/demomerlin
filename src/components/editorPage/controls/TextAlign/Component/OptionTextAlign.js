import PropTypes from "prop-types";
import Option from "../../../Option/Option";

const OptionTextAlign = ({ options, option, onChange, currentState, typeBtn }) => (
  <>
    {options.indexOf(typeBtn) >= 0 && (
      <Option
        value={typeBtn}
        active={currentState.textAlignment === { typeBtn }}
        onClick={onChange}
      >
        <img src={option.icon} alt="" />
      </Option>
    )}
  </>
);

OptionTextAlign.defaultProps = {
  right: {},
};

OptionTextAlign.propTypes = {
  onChange: PropTypes.func.isRequired,
  options: PropTypes.arrayOf(PropTypes.string).isRequired,
  option: PropTypes.shape({
    icon: PropTypes.string,
  }).isRequired,
  right: PropTypes.shape({
    icon: PropTypes.string,
  }),
  currentState: PropTypes.shape({
    textAlignment: PropTypes.string,
  }).isRequired,
  typeBtn: PropTypes.string.isRequired,
};

export default OptionTextAlign;
