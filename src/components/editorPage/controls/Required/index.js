import React, { Component } from "react";
import PropTypes from "prop-types";
import RequiredComponent from "./Component";
import { getSelectedBlocksMetadata, setBlockData } from "../../utils/draft-js";

class Required extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentRequired: false,
    };
  }

  componentDidUpdate(prevProps) {
    const { editorState } = this.props;

    if (editorState !== prevProps.editorState) {
      this.setCurrentRequired(editorState);
    }
  }

  setCurrentRequired = (editorState) => {
    this.setState({
      currentRequired: getSelectedBlocksMetadata(editorState).get("required"),
    });
  };

  toggleRequired = (required) => {
    const { editorState, onChange } = this.props;
    onChange(setBlockData(editorState, { required }));
  };

  render() {
    const { currentRequired } = this.state;

    return (
      <RequiredComponent currentState={currentRequired || false} onChange={this.toggleRequired} />
    );
  }
}

Required.propTypes = {
  onChange: PropTypes.func.isRequired,
  editorState: PropTypes.objectOf(PropTypes.object).isRequired,
};

export default Required;
