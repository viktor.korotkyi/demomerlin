import React, { Component } from "react";
import PropTypes from "prop-types";
import Option from "../../../Option/Option";

import styles from "../../../../../../scss/components/editorPage/controls/Required.module.scss";

class RequiredComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      required: false,
    };
  }

  render() {
    const { required } = this.state;
    const { onChange, currentState } = this.props;

    return (
      <div className={styles.wrapper}>
        <Option
          value={!(currentState || required)}
          active={currentState || required}
          onClick={onChange}
        >
          Switch fixed
        </Option>
      </div>
    );
  }
}

RequiredComponent.propTypes = {
  onChange: PropTypes.func.isRequired,
  currentState: PropTypes.bool.isRequired,
};

export default RequiredComponent;
