import PropTypes from "prop-types";
import { useEffect, useRef } from "react";
import { convertToRaw, DefaultDraftBlockRenderMap } from "draft-js";
import { composeDecorators } from "draft-js-plugins-editor";
import { Map, List } from "immutable";
import Editor from "@draft-js-plugins/editor";
import createImagePlugin from "@draft-js-plugins/image";
import createResizeablePlugin from "draft-js-resizeable-plugin";
import createFocusPlugin from "draft-js-focus-plugin";
import createAlignmentPlugin from "draft-js-alignment-plugin";
import createBlockDndPlugin from "draft-js-drag-n-drop-plugin";
import ModalHandler from "./helpers/modalHandler";
import { getCustomStyleMap } from "./utils/draft-js";
import Toolbar from "./Toolbar/Toolbar";
import constants from "../../constants/storage";
import editorConstants from "../../constants/editor";
import ListSizeInput from "./ListSizeInput";
import SizeInput from "./SizeInput";
import { sliceArray } from "../../utils/storageWork";
import styles from "../../../scss/components/editorPage/Editor.module.scss";

const focusPlugin = createFocusPlugin();
const blockDndPlugin = createBlockDndPlugin();
const resizeablePlugin = createResizeablePlugin({
  vertical: "absolute",
  horizontal: "absolute",
});
const alignmentPlugin = createAlignmentPlugin();
const { AlignmentTool } = alignmentPlugin;

const decorator = composeDecorators(
  focusPlugin.decorator,
  resizeablePlugin.decorator,
  alignmentPlugin.decorator,
  blockDndPlugin.decorator
);
const imagePlugin = createImagePlugin({ decorator });

const plugins = [blockDndPlugin, focusPlugin, alignmentPlugin, resizeablePlugin, imagePlugin];

const MyEditor = ({ handlerDataTemplate, widthBlock, heightBlock, onChange, editorState }) => {
  const editorRef = useRef(null);
  const modalHandler = new ModalHandler();
  const {
    defaultStylesBlock,
    draftStyleDefault,
    defaultMargin,
    elementPre,
    stringfontfamily,
    stringFontsize,
    stringUnderline,
    stringBold,
    stringItalic,
    left,
    flexStart,
    right,
    flexEnd,
    defaultString,
    center,
  } = editorConstants;
  const newBlockRenderMap = Map({
    code: {
      element: elementPre,
    },
  });
  const blockRenderMap = DefaultDraftBlockRenderMap.merge(newBlockRenderMap);

  const getSizeAllBlock = () => {
    let array;
    if (typeof window !== "undefined") {
      array = document.getElementsByClassName(draftStyleDefault);
    }
    return array ? Object.values(array).map((item) => item.clientHeight) : [];
  };

  const getAllVerticalMarginBlocks = (marginList) => {
    const marginArray = List.isList(marginList) ? marginList.toJSON() : defaultMargin;
    const top = marginArray[0].value ? Number(marginArray[0].value) : 0;
    const bottom = marginArray[3].value ? Number(marginArray[3].value) : 0;
    return top + bottom;
  };

  const getMarginBlocks = (marginList) => {
    const marginArray = List.isList(marginList) ? marginList.toJSON() : defaultMargin;
    return marginArray;
  };

  const getVerticalMargin = (data) => {
    const listMap = data.map((item) => item.data.margin);
    return listMap.map((item) => getAllVerticalMarginBlocks(item));
  };

  const getMargin = (data) => {
    const listMap = data.map((item) => item.data.margin);
    return listMap.map((item) => getMarginBlocks(item));
  };

  // return in px
  const getCoordinates = (index, arrayBlocks, arrayMarginVerticalBlock) => {
    const array = sliceArray(0, index, arrayBlocks);
    const arrayMargin = sliceArray(0, index, arrayMarginVerticalBlock);
    const sizeAllBlock = array.length > 0 ? array.reduce((total, amount) => total + amount) : 0;
    const sumMargins =
      arrayMargin.length > 0 ? arrayMargin.reduce((total, amount) => total + amount) : 0;
    return sizeAllBlock + sumMargins;
  };

  const filterStyleArray = (array) =>
    array.map((item) => item.style.split("-")).filter((el) => el.length > 1);

  const filterFontStyleArray = (array) =>
    array.map((item) => item.style.split("-")).filter((el) => el.length <= 1);

  const searchTypeStyle = (array, type) => array.filter((item) => item.indexOf(type) !== -1);

  const getFontFamily = (data) => {
    let font = "";
    const arrayStyles = filterStyleArray(data);
    arrayStyles.forEach((item) => {
      const [type, fontfamily] = item;
      if (item.length > 1 && type === stringfontfamily) {
        font = fontfamily;
      }
    });
    return font;
  };

  const getFontSize = (data) => {
    let size = "14";
    const arrayStyles = filterStyleArray(data);
    arrayStyles.forEach((item) => {
      const [type, fontSize] = item;
      if (item.length > 1 && type === stringFontsize) {
        size = fontSize;
      }
    });
    return size;
  };

  const getTextDecoration = (data) => {
    const array = filterFontStyleArray(data);
    return searchTypeStyle(array, constants.underline).length > 0 ? stringUnderline : "";
  };

  const getFontWeight = (data) => {
    const array = filterFontStyleArray(data);
    return searchTypeStyle(array, constants.bold).length > 0 ? stringBold : "";
  };

  const getFontStyle = (data) => {
    const array = filterFontStyleArray(data);
    return searchTypeStyle(array, constants.italic).length > 0 ? stringItalic : "";
  };

  const convertAligment = (data) => {
    switch (data) {
      case left:
        return flexStart;
      case right:
        return flexEnd;
      case defaultString:
        return flexStart;
      case center:
        return center;
      default:
        return flexStart;
    }
  };

  const createElements = (editorContent, arraySizeBlock, arrayMarginVerticalBlock) => {
    return editorContent.blocks.map((item, index) => {
      const coordinates = getCoordinates(index, arraySizeBlock, arrayMarginVerticalBlock);
      const inlineStyle = item.inlineStyleRanges ? item.inlineStyleRanges : defaultStylesBlock;
      if (item.type === "atomic") {
        return {
          id: item.key,
          type: "image",
          y: coordinates,
          x: 0,
          url: editorContent.entityMap[item.entityRanges[0].key].data.src,
          width: editorContent.entityMap[item.entityRanges[0].key].data.width,
          height: editorContent.entityMap[item.entityRanges[0].key].data.height,
          alignment: convertAligment(
            editorContent.entityMap[item.entityRanges[0].key].data.alignment
          ),
          margin: getMargin(editorContent.blocks)[index],
          z: !!(item.data.required && item.data.required !== false),
        };
      }

      return {
        id: item.key,
        type: item.type,
        y: coordinates,
        x: 0,
        height: arraySizeBlock[index],
        inlineStyleRanges: inlineStyle,
        fontFamily: getFontFamily(inlineStyle),
        fontSize: getFontSize(inlineStyle),
        textAlign: convertAligment(item.data["text-align"]),
        textDecoration: getTextDecoration(inlineStyle),
        fontWeight: getFontWeight(inlineStyle),
        fontStyle: getFontStyle(inlineStyle),
        text: item.text,
        margin: getMargin(editorContent.blocks)[index],
        fixedText: !!(item.data.required && item.data.required !== false),
      };
    });
  };

  const createDataTemplate = () => {
    const editorContent = convertToRaw(editorState.getCurrentContent());
    const arraySizeBlock = getSizeAllBlock();
    const arrayMarginVerticalBlock = getVerticalMargin(editorContent.blocks);
    const newElements = createElements(editorContent, arraySizeBlock, arrayMarginVerticalBlock);
    handlerDataTemplate(newElements, constants.elements);
  };

  useEffect(() => {
    modalHandler.init(constants.wrapperId);
  }, []);

  useEffect(() => {
    createDataTemplate();
  }, [editorState]);

  const blockStyleFn = (block) => {
    const blockKey = block.getKey();
    const blockAlignment = block.getData() && block.getData().get("text-align");
    const blockMargin = block.getData() && block.getData().get("margin");
    let blockDom;
    if (typeof window !== "undefined") {
      blockDom = document.querySelector(`[data-offset-key='${blockKey}-0-0']`);
    }
    if (blockDom && blockMargin && !blockDom.hasAttribute("style")) {
      blockMargin.forEach((item) => {
        blockDom.style[item.type] = `${item.value}px`;
      });
    }
    if (blockAlignment) {
      return `align-${blockAlignment}`;
    }
    return null;
  };

  const setEditorSize = (e) => {
    const editor = editorRef.current;
    const { name } = e.target;
    let v = e.target.value;
    if (v > 1500) {
      v = constants.defaultMax;
    }
    editor.style[name] = `${v}`;
    handlerDataTemplate(`${v}`, name);
  };

  const initialSize = () => {
    const editor = editorRef.current;
    editor.style.width = widthBlock ? `${widthBlock}${constants.unitPx}` : constants.width;
    editor.style.height = heightBlock ? `${heightBlock}${constants.unitPx}` : constants.height;
  };

  useEffect(() => {
    initialSize();
  }, [widthBlock, heightBlock]);

  return (
    <div
      id={constants.wrapperId}
      onClick={modalHandler.onEditorClick}
      ref={(divElement) => divElement}
      className={styles.container}
      onKeyUp={modalHandler.onEditorClick}
      role="button"
      tabIndex="0"
    >
      <div className={styles.sizeEditorBlock}>
        {ListSizeInput.map((item, index) => (
          <SizeInput
            key={[index, item.name].join("_")}
            name={item.name}
            onChange={setEditorSize}
            typeInput={item.typeInput}
            valueText={item.valueText === constants.widthBlock ? widthBlock : heightBlock}
          />
        ))}
      </div>
      <Toolbar editorState={editorState} modalHandler={modalHandler} onChange={onChange} />
      <div className={styles.wrapper} ref={editorRef}>
        <Editor
          editorState={editorState}
          onChange={onChange}
          blockStyleFn={blockStyleFn}
          blockRenderMap={blockRenderMap}
          customStyleMap={getCustomStyleMap()}
          plugins={plugins}
          editorKey="editor"
        />
      </div>
      <AlignmentTool />
    </div>
  );
};

MyEditor.defaultProps = {
  handlerDataTemplate: () => {},
  widthBlock: "",
  heightBlock: "",
  editorState: {},
  onChange: () => {},
  sizeTemplate: {},
};

MyEditor.propTypes = {
  handlerDataTemplate: PropTypes.func,
  widthBlock: PropTypes.string,
  heightBlock: PropTypes.string,
  editorState: PropTypes.shape({
    getCurrentContent: PropTypes.func,
  }),
  onChange: PropTypes.func,
  sizeTemplate: PropTypes.shape({}),
};

export default MyEditor;
