import PropTypes from "prop-types";
import classNames from "classnames";

import styles from "../../../../scss/components/editorPage/DropdownOption.module.scss";

const DropDownOption = ({
  children,
  active,
  highlighted,
  className,
  setHighlighted,
  index,
  onSelect,
  onClick,
  value,
}) => {
  const onClickOption = (event) => {
    if (onSelect) {
      onSelect(value);
    }
    if (onClick) {
      event.stopPropagation();
      onClick(value);
    }
  };

  const setHighlightedHandle = () => {
    setHighlighted(index);
  };

  const resetHighlighted = () => {
    setHighlighted(-1);
  };
  return (
    <div
      onMouseEnter={setHighlightedHandle}
      onMouseLeave={resetHighlighted}
      onClick={onClickOption}
      onKeyUp={onClickOption}
      role="button"
      tabIndex="0"
    >
      <li
        className={classNames(styles.default, className, {
          [styles.active]: active,
          [styles.highlighted]: highlighted,
        })}
      >
        {children}
      </li>
    </div>
  );
};

DropDownOption.propTypes = {
  children: PropTypes.node.isRequired,
  setHighlighted: PropTypes.func,
  highlighted: PropTypes.bool,
  active: PropTypes.bool.isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  className: PropTypes.string,
  onSelect: PropTypes.func,
  onClick: PropTypes.func,
  index: PropTypes.number,
};

DropDownOption.defaultProps = {
  className: "",
  setHighlighted: () => {},
  onSelect: () => {},
  onClick: () => {},
  highlighted: false,
  index: -1,
};

export default DropDownOption;
