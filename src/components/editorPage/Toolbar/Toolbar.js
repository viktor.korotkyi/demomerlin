import React from "react";
import PropTypes from "prop-types";

import toolbar from "./config";
import Controls from "../controls";

import styles from "../../../../scss/components/editorPage/Toolbar.module.scss";

const Toolbar = ({ editorState, modalHandler, onChange }) => (
  <div className={styles.wrapper}>
    {toolbar.options.map((opt, index) => {
      const Control = Controls[opt];
      const config = toolbar[opt];
      return (
        <Control
          key={[index]}
          editorState={editorState}
          modalHandler={modalHandler}
          onChange={onChange}
          config={config}
        />
      );
    })}
  </div>
);

Toolbar.propTypes = {
  onChange: PropTypes.func.isRequired,
  editorState: PropTypes.objectOf(PropTypes.object).isRequired,
  modalHandler: PropTypes.shape({
    callBacks: PropTypes.arrayOf(PropTypes.func),
  }).isRequired,
};

export default Toolbar;
