export default {
  options: [
    "inline",
    "blockType",
    "fontSize",
    "fontFamily",
    "textAlign",
    "image",
    "required",
    "history",
    "margin",
  ],
  inline: {
    options: ["bold", "italic", "underline"],
    bold: { icon: "/static/images/editorIcons/bold.svg" },
    italic: { icon: "/static/images/editorIcons/italic.svg" },
    underline: { icon: "/static/images/editorIcons/underline.svg" },
  },
  blockType: {
    options: ["Normal", "H1", "H2", "H3", "H4", "H5", "H6"],
  },
  fontSize: {
    icon: "/static/images/editorIcons/font-size.svg",
    options: [8, 9, 10, 11, 12, 14, 16, 18, 24, 30, 36, 48, 60, 72, 96],
  },
  fontFamily: {
    options: [
      "Arial",
      "Georgia",
      "Impact",
      "Tahoma",
      "Times New Roman",
      "Verdana",
      "Dancing Script",
      "Merriweather",
      "Roboto",
      "Source Sans Pro",
    ],
  },
  textAlign: {
    options: ["left", "center", "right", "justify"],
    left: { icon: "/static/images/editorIcons/align-left.svg" },
    center: { icon: "/static/images/editorIcons/align-center.svg" },
    right: { icon: "/static/images/editorIcons/align-right.svg" },
    justify: { icon: "/static/images/editorIcons/align-justify.svg" },
  },
  image: {
    icon: "/static/images/editorIcons/image.svg",
    inputAccept: "images/gif,images/jpeg,images/jpg,images/png,images/svg",
    defaultSize: {
      height: 50,
      width: 50,
    },
  },
  required: {},
  history: {
    options: ["undo", "redo"],
    undo: { icon: "/static/images/editorIcons/undo.svg" },
    redo: { icon: "/static/images/editorIcons/redo.svg" },
  },
  margin: {},
};
