import { create } from "react-test-renderer";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import SelectTimeZone from "../../../src/components/general/SelectTimeZone";

describe("<SelectTimeZone /> component test", () => {
  const mockFunction = jest.fn();

  it("should match the snapshot", () => {
    const component = create(<SelectTimeZone />);
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("element with 'Timezone' text should be in the document", () => {
    const { getAllByText } = render(<SelectTimeZone />);
    expect(getAllByText("Timezone")[0]).toBeInTheDocument();
  });

  it("input value should be 'America/Anchorage'", () => {
    const { container } = render(<SelectTimeZone selectedTimezone="America/Anchorage" />);

    expect(container.querySelector("input")).toHaveValue("America/Anchorage");
  });
});
