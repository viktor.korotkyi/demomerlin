import { create } from "react-test-renderer";
import { render } from "@testing-library/react";
import ViewContent from "../../../src/components/general/ViewContent";

describe("<ButtonGroup /> component test", () => {
  const mockContent = {
    elements: [],
    width: "100",
    height: "200",
  };

  it("should match the snapshot", () => {
    const component = create(<ViewContent content={mockContent} />);
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("element with text 'live preview' should be present in the document", () => {
    const { getByText } = render(<ViewContent content={mockContent} />);
    expect(getByText("live preview")).toBeInTheDocument();
  });

  it("element styles should be correct", () => {
    const { container } = render(<ViewContent content={mockContent} />);
    expect(container.querySelector(".template")).toHaveStyle(`
      width: 100px;
      height: 200px;
    `);
  });

  it("element styles should be incorrect", () => {
    const { container } = render(<ViewContent content={mockContent} />);
    expect(container.querySelector(".template")).not.toHaveStyle(`
      width: 500px;
      height: 500px;
    `);
  });
});
