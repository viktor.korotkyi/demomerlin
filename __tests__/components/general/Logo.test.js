import { create } from "react-test-renderer";
import { render, screen } from "@testing-library/react";
import Logo from "../../../src/components/general/Logo";

describe("<Logo /> component test", () => {
  it("should match the snapshot", () => {
    const component = create(<Logo text="Hello" />);
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("text prop should match element with such a text in the document", () => {
    const { getByText } = render(<Logo text="Hello" />);

    expect(getByText("Hello")).toBeInTheDocument();
  });

  it("image with 'logo' alt should be present in the document", () => {
    const { getByAltText } = render(<Logo text="Hello" />);

    expect(getByAltText("logo")).toBeInTheDocument();
  });
});
