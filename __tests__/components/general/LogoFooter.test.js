import { create } from "react-test-renderer";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import LogoFooter from "../../../src/components/general/LogoFooter";

describe("<LogoFooter /> component test", () => {
  const mockFunction = jest.fn();

  it("should match the snapshot", () => {
    const component = create(<LogoFooter />);
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("privacy prop should be present in the document", () => {
    const { getByText } = render(<LogoFooter privacy="privacy" />);

    expect(getByText("privacy")).toBeInTheDocument();
  });

  it("terms prop should be present in the document", () => {
    const { getByText } = render(<LogoFooter terms="terms" />);

    expect(getByText("terms")).toBeInTheDocument();
  });
});
