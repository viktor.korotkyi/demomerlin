import { create } from "react-test-renderer";
import { render } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import ButtonBoxCreate from "../../../src/components/general/ButtonBoxCreate";

describe("<ButtonBoxCreate /> component test", () => {
  const mockFunction = jest.fn();

  it("should match the snapshot", () => {
    const component = create(<ButtonBoxCreate />);
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("button should not be disabled", () => {
    const { container } = render(<ButtonBoxCreate />);
    let button = container.querySelector(".MuiButton-containedPrimary");
    expect(button.disabled).toBeFalsy();
  });

  it("button should be disabled", () => {
    const { container } = render(<ButtonBoxCreate disabled={true} />);
    let button = container.querySelector(".MuiButton-containedPrimary");
    expect(button.disabled).toBeTruthy();
  });

  it("saveBtn callback prop should be callable", () => {
    const { getByText } = render(<ButtonBoxCreate saveBtn={mockFunction} />);
    userEvent.click(getByText("Save"));
    expect(mockFunction).toHaveBeenCalled();
  });

  it("cancelBtn callback prop should be callable", () => {
    const { getByText } = render(<ButtonBoxCreate cancelBtn={mockFunction} />);
    userEvent.click(getByText("Cancel"));
    expect(mockFunction).toHaveBeenCalled();
  });

  it("deleteBtn callback prop should be callable", () => {
    const { getByText } = render(<ButtonBoxCreate deleteBtn={mockFunction} />);
    userEvent.click(getByText("Delete"));
    expect(mockFunction).toHaveBeenCalled();
  });

  it("widthContainer prop should be 100px", () => {
    const { container } = render(<ButtonBoxCreate widthContainer="100px" />);

    expect(window.getComputedStyle(container.firstChild).getPropertyValue("width")).toEqual(
      "100px"
    );
  });

  it("widthContainer prop should be empty", () => {
    const { container } = render(<ButtonBoxCreate />);

    expect(window.getComputedStyle(container.firstChild).getPropertyValue("width")).toBeFalsy();
  });
});
