import { create } from "react-test-renderer";
import { render } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import PricingAdjustment from "../../../src/components/general/PricingAdjustment";

// this code was taken from https://github.com/vercel/next.js/discussions/23034#discussioncomment-478452
import * as nextRouter from "next/router";

nextRouter.useRouter = jest.fn();
nextRouter.useRouter.mockImplementation(() => ({
  query: {
    portalName: "text-portal",
    portalChild: "customers",
    setting: "1",
  },
}));

describe("<PricingAdjustment /> component test", () => {
  let mockFunction = jest.fn();
  const mockList = Array.from({ length: 5 }, (_, i) => ({ label: `label ${i}` }));

  beforeEach(() => {
    mockFunction = jest.fn();
  });

  it("should match the snapshot", () => {
    const component = create(<PricingAdjustment listAdjustment={mockList} />);

    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("input onChange handler should should be called 4 times", () => {
    const { container } = render(
      <PricingAdjustment listAdjustment={mockList} onChange={mockFunction} />
    );

    userEvent.type(container.querySelector("#valueTribune"), "1000");
    expect(mockFunction).toHaveBeenCalledTimes(4);
  });

  it("input onChange handler should be called 0 times if non-digit value would be passed in there", () => {
    const { container } = render(
      <PricingAdjustment listAdjustment={mockList} onChange={mockFunction} />
    );

    userEvent.type(container.querySelector("#valueTribune"), "non-digit");
    expect(mockFunction).toHaveBeenCalledTimes(0);
  });

  it("checkbox value should be set to false", () => {
    const { container } = render(
      <PricingAdjustment listAdjustment={mockList} onChange={mockFunction} />
    );

    expect(container.querySelector(".PrivateSwitchBase-input-5").checked).toBeFalsy();
  });

  it("checkbox value should be set to true when it would be clicked", () => {
    const { getByLabelText } = render(
      <PricingAdjustment listAdjustment={mockList} onChange={mockFunction} />
    );

    const checkbox = getByLabelText("Pricing Per Day");
    userEvent.click(checkbox);
    expect(checkbox.checked).toBeTruthy();
  });

  it("select options length should be the same as mockList length", () => {
    const { container } = render(
      <PricingAdjustment listAdjustment={mockList} onChange={mockFunction} />
    );
    const select = container.querySelector("#selectedAdjustmentPrice");

    expect(select.options.length).toEqual(mockList.length);
  });
});
