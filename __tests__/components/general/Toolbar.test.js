import { create } from "react-test-renderer";
import { render } from "@testing-library/react";
import Toolbar from "../../../src/components/general/Toolbar";

describe("<Toolbar /> component test", () => {
  it("should match the snapshot", () => {
    const component = create(
      <Toolbar>
        <p>I am a child</p>
      </Toolbar>
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("children prop should be present in the document", () => {
    const { getByText } = render(
      <Toolbar>
        <p>I am a child</p>
      </Toolbar>
    );

    expect(getByText("I am a child")).toBeInTheDocument();
  });

  it("children prop should be undefined", () => {
    const { container } = render(
      <Toolbar>
        <p>number 1</p>
        <p>number 2</p>
      </Toolbar>
    );

    expect(container).toContainHTML("<p>number 1</p><p>number 2</p>");
  });
});
