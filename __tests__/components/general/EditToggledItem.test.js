import { create } from "react-test-renderer";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import EditToggledItem from "../../../src/components/general/EditToggledItem";

describe("<EditToggledItem /> component test", () => {
  let mockFunction = jest.fn();

  beforeEach(() => {
    mockFunction = jest.fn();
  });

  it("should match the snapshot", () => {
    const component = create(
      <EditToggledItem
        textFieldDefaultValue="test"
        switchChecked
        saveButtonDisabled={false}
        handleTextField={mockFunction}
        handleSwitch={mockFunction}
        handleSave={mockFunction}
        handleCancel={mockFunction}
      />
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("Element with 'New api url' text should be in the document", () => {
    const { getByText } = render(
      <EditToggledItem
        textFieldDefaultValue="test"
        switchChecked
        saveButtonDisabled={false}
        handleTextField={mockFunction}
        handleSwitch={mockFunction}
        handleSave={mockFunction}
        handleCancel={mockFunction}
      />
    );

    expect(getByText("New api url")).toBeInTheDocument();
  });

  it("Checkbox should be present in the document", () => {
    const { getByRole } = render(
      <EditToggledItem
        textFieldDefaultValue="test"
        switchChecked
        saveButtonDisabled={false}
        handleTextField={mockFunction}
        handleSwitch={mockFunction}
        handleSave={mockFunction}
        handleCancel={mockFunction}
      />
    );

    expect(getByRole("checkbox")).toBeInTheDocument();
  });

  it("saveButtonDisabled prop set to true should disable button", () => {
    const { container } = render(
      <EditToggledItem
        textFieldDefaultValue="test"
        switchChecked
        saveButtonDisabled={true}
        handleTextField={mockFunction}
        handleSwitch={mockFunction}
        handleSave={mockFunction}
        handleCancel={mockFunction}
      />
    );

    expect(container.querySelector(".MuiButton-containedPrimary")).toBeDisabled();
  });

  it("checkbox should be checked", () => {
    const { getByRole } = render(
      <EditToggledItem
        textFieldDefaultValue="test"
        switchChecked
        saveButtonDisabled={true}
        handleTextField={mockFunction}
        handleSwitch={mockFunction}
        handleSave={mockFunction}
        handleCancel={mockFunction}
      />
    );

    expect(getByRole("checkbox")).toBeChecked();
  });

  it("handleSave callback prop should be callable", () => {
    const { container } = render(
      <EditToggledItem
        textFieldDefaultValue="test"
        switchChecked
        saveButtonDisabled={false}
        handleTextField={mockFunction}
        handleSwitch={mockFunction}
        handleSave={mockFunction}
        handleCancel={mockFunction}
      />
    );

    userEvent.click(container.querySelector(".MuiButton-containedPrimary"));
    expect(mockFunction).toHaveBeenCalled();
  });

  it("handleCancel callback prop should be callable", () => {
    const { container } = render(
      <EditToggledItem
        textFieldDefaultValue="test"
        switchChecked
        saveButtonDisabled={false}
        handleTextField={mockFunction}
        handleSwitch={mockFunction}
        handleSave={mockFunction}
        handleCancel={mockFunction}
      />
    );

    userEvent.click(container.querySelector(".MuiButton-containedSecondary"));
    expect(mockFunction).toHaveBeenCalled();
  });

  it("input value should be 'Hello, World!'", () => {
    const { container } = render(
      <EditToggledItem
        textFieldDefaultValue=""
        switchChecked
        saveButtonDisabled={false}
        handleTextField={mockFunction}
        handleSwitch={mockFunction}
        handleSave={mockFunction}
        handleCancel={mockFunction}
      />
    );

    userEvent.type(container.querySelector(".MuiInput-input"), "Hello, World!");
    expect(container.querySelector(".MuiInput-input")).toHaveValue("Hello, World!");
  });

  it("handleTextField callback prop should be called as symbols typed in input (13)", () => {
    const { container } = render(
      <EditToggledItem
        textFieldDefaultValue=""
        switchChecked
        saveButtonDisabled={false}
        handleTextField={mockFunction}
        handleSwitch={mockFunction}
        handleSave={mockFunction}
        handleCancel={mockFunction}
      />
    );

    userEvent.type(container.querySelector(".MuiInput-input"), "Hello, World!");
    expect(mockFunction).toHaveBeenCalledTimes(13);
  });

  it("handleSwitch should be called once when switch value has been changed", () => {
    render(
      <EditToggledItem
        textFieldDefaultValue=""
        switchChecked
        saveButtonDisabled={false}
        handleTextField={mockFunction}
        handleSwitch={mockFunction}
        handleSave={mockFunction}
        handleCancel={mockFunction}
      />
    );

    userEvent.click(screen.getByRole("checkbox"));
    expect(mockFunction).toHaveBeenCalledTimes(1);
  });
});
