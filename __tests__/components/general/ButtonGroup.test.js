import { create } from "react-test-renderer";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import ButtonGroup from "../../../src/components/general/ButtonGroup";

describe("<ButtonGroup /> component test", () => {
  const mockFunction = jest.fn();

  it("should match the snapshot", () => {
    const component = create(<ButtonGroup successLabel="Update" />);
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("buttons' titles should be 'Update' and 'Leave'", () => {
    const { getByText } = render(<ButtonGroup successLabel="Update" cancelLabel="Leave" />);
    expect(getByText("Update")).toBeInTheDocument();
    expect(getByText("Leave")).toBeInTheDocument();
  });

  it("buttons' titles should be 'Save' and 'Cancel'", () => {
    const { getByText } = render(<ButtonGroup />);
    expect(getByText("Save")).toBeInTheDocument();
    expect(getByText("Cancel")).toBeInTheDocument();
  });

  it("should be called function on save button click", () => {
    const { container } = render(<ButtonGroup handleSave={mockFunction} />);
    const button = container.querySelector(".MuiButton-containedPrimary");
    userEvent.click(button);
    expect(mockFunction).toHaveBeenCalled();
  });

  it("should be called function on cancel button click", () => {
    const { container } = render(<ButtonGroup handleCancel={mockFunction} />);
    const button = container.querySelector(".MuiButton-containedSecondary");
    userEvent.click(button);
    expect(mockFunction).toHaveBeenCalled();
  });

  it("wrapper should not be empty", () => {
    const { getByRole } = render(<ButtonGroup />);

    expect(getByRole("group")).toBeInTheDocument();
  });

  it("button title should be 'Success!'", () => {
    const { container } = render(<ButtonGroup successLabel="Success!" />);
    const button = container.querySelector(".MuiButton-containedPrimary");
    expect(button).toHaveTextContent("Success!");
  });

  it("cancelLabel prop should be 'Cancel!'", () => {
    const { container } = render(<ButtonGroup cancelLabel="Cancel!" />);
    const button = container.querySelector(".MuiButton-containedSecondary");
    expect(button).toHaveTextContent("Cancel!");
  });

  it("Rerender component with new cancelLabel prop", () => {
    const { rerender } = render(<ButtonGroup cancelLabel="Cancel!" />);
    expect(screen.getAllByRole("button")[1]).toHaveTextContent("Cancel!");
    rerender(<ButtonGroup cancelLabel="Discard" />);
    expect(screen.getAllByRole("button")[1]).toHaveTextContent("Discard");
  });
});
