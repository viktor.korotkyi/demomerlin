import { create } from "react-test-renderer";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import ContainerButton from "../../../src/components/general/ContainerButton";

describe("<ConatinerButton /> component test", () => {
  const mockFunction = jest.fn();

  it("should match the snapshot", () => {
    const component = create(<ContainerButton />);
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("element with 'Update' text should be in the document", () => {
    const { getByText } = render(<ContainerButton />);

    expect(getByText("Update")).toBeInTheDocument();
  });

  it("element with 'Cancel' text should be in the document", () => {
    const { getByText } = render(<ContainerButton />);

    expect(getByText("Cancel")).toBeInTheDocument();
  });

  it("clickFirst callback prop should be callable", () => {
    const { getByText } = render(<ContainerButton clickFirst={mockFunction} />);

    userEvent.click(getByText("Update"));
    expect(mockFunction).toHaveBeenCalled();
  });

  it("clickSecond callback prop should be callable", () => {
    const { getByText } = render(<ContainerButton clickSecond={mockFunction} />);

    userEvent.click(getByText("Cancel"));
    expect(mockFunction).toHaveBeenCalled();
  });
});
