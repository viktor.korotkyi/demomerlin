import { create } from "react-test-renderer";
import { render } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import EditList from "../../../src/components/general/EditList";

describe("<EditList /> component test", () => {
  const mockFunction = jest.fn();
  const mockListItems = Array.from({ length: 5 }, (_, i) => ({
    id: i,
    name: `Test ${i}`,
    edit: false,
  }));

  it("should match the snapshot", () => {
    const component = create(<EditList label="List" items={mockListItems} />);
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("'List' text should be present in the document if at least one item is being edited", () => {
    const { getByText } = render(
      <EditList
        label="List"
        items={[...mockListItems, { id: 123, name: "Test 123", edit: true }]}
      />
    );

    expect(getByText("List")).toBeInTheDocument();
  });

  it("Amount of li's should be as length of items is", () => {
    const { container } = render(<EditList label="List" items={mockListItems} />);
    const listLength = container.querySelectorAll("li").length;
    expect(listLength).toEqual(mockListItems.length);
  });

  it("Amount of li's should be as length of items is", () => {
    const { container } = render(<EditList label="List" items={mockListItems} />);
    const listLength = container.querySelectorAll("li").length;
    expect(listLength).toEqual(mockListItems.length);
  });

  it("Two buttons should be present in the document if at least one item has edit property set to true", () => {
    const { getByText } = render(
      <EditList
        label="List"
        items={[...mockListItems, { id: 123, name: "Test 123", edit: true }]}
      />
    );

    expect(getByText("Save")).toBeInTheDocument();
    expect(getByText("Cancel")).toBeInTheDocument();
  });

  it("Buttons should NOT be present if none of the items have edit property set to true", () => {
    const { queryByText } = render(<EditList label="List" items={mockListItems} />);

    expect(queryByText("Save")).toBeNull();
    expect(queryByText("Cancel")).toBeNull();
  });

  it("'handleEdit' callback handler should be callable", () => {
    const { getByText } = render(
      <EditList
        label="List"
        handleEdit={mockFunction}
        items={[...mockListItems, { id: 123, name: "Test 123", edit: true }]}
      />
    );

    userEvent.click(getByText("Cancel"));
    expect(mockFunction).toHaveBeenCalled();
  });
});
