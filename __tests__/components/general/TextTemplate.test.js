import { create } from "react-test-renderer";
import { render } from "@testing-library/react";
import TextTemplate from "../../../src/components/general/TextTemplate";

describe("<ButtonGroup /> component test", () => {
  const mockItem = {
    text: "I am a typical heading",
    fontFamily: "sans-serif",
    fontSize: "20",
    fontStyle: "italic",
    fontWeight: "700",
    textAlign: "center",
    textDecoration: "underline",
    margin: [
      { type: "marginTop", value: 20 },
      { type: "marginRight", value: 25 },
      { type: "marginBottom", value: 10 },
      { type: "marginLeft", value: 30 },
    ],
    y: 100,
    x: 100,
  };

  it("should match the snapshot", () => {
    const component = create(<TextTemplate item={mockItem} />);
    let tree = component.toJSON();

    expect(tree).toMatchSnapshot();
  });

  it("element with text 'I am a typical heading' should be present in the document", () => {
    const { getByText } = render(<TextTemplate item={mockItem} />);

    expect(getByText("I am a typical heading")).toBeInTheDocument();
  });

  it("element with text 'I am a typical heading' should have such styles to be present", () => {
    const { getByText } = render(<TextTemplate item={mockItem} />);

    expect(getByText("I am a typical heading")).toHaveStyle(`
      align-self: center;
      font-size: 20px;
      font-family: sans-serif;
      font-style: italic;
      font-weight: 700;
      text-decoration: underline;
    `);
  });

  it("element with text 'I am a typical heading' should NOT have such styles to be present", () => {
    const { getByText } = render(<TextTemplate item={mockItem} />);

    expect(getByText("I am a typical heading")).not.toHaveStyle(`
      align-self: start;
      font-size: 10px;
      font-family: Arial;
      font-style: normal;
      font-weight: 100;
      text-decoration: none;
    `);
  });
});
