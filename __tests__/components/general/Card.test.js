import { create } from "react-test-renderer";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Card from "../../../src/components/general/Card";

describe("<Card /> component test", () => {
  const mockFunction = jest.fn();

  it("should match the snapshot", () => {
    const component = create(<Card logo="test" />);
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("card header should be presented in the document", () => {
    const { getByText } = render(<Card logo="test" header="Card" />);

    expect(getByText("Card")).toBeInTheDocument();
  });

  it("clickCard prop should be callable", () => {
    const { container } = render(<Card logo="test" clickCard={mockFunction} />);
    userEvent.click(container.querySelector(".MuiCardActionArea-root"));
    expect(mockFunction).toHaveBeenCalled();
  });
});
