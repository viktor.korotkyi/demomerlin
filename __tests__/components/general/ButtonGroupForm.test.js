import { create } from "react-test-renderer";
import { render, fireEvent } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import ButtonGroupForm from "../../../src/components/general/ButtonGroupForm";

describe("<ButtonGroupForm /> component test", () => {
  const mockFunction = jest.fn();

  it("should match the snapshot", () => {
    const component = create(<ButtonGroupForm />);
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("defaultInputValue prop should equal to 'something default'", () => {
    const { container } = render(<ButtonGroupForm defaultInputValue="something default" />);
    const input = container.querySelector("input");
    expect(input.value).toEqual("something default");
  });

  it("label prop should equal to 'thisIsLabel'", () => {
    const { getByText } = render(<ButtonGroupForm label="thisIsLabel" />);
    expect(getByText("thisIsLabel")).toHaveTextContent("thisIsLabel");
  });

  it("save button should be disabled", () => {
    const { container } = render(<ButtonGroupForm handleSave={mockFunction} />);
    const saveButton = container.querySelector(".MuiButton-containedPrimary");

    expect(saveButton.disabled).toBeTruthy();
  });

  it("save button should be clickable", () => {
    const { container, findByText } = render(<ButtonGroupForm />);
    const input = container.querySelector("input");

    fireEvent.change(input, { target: { value: "hello." } });
    findByText("hello.", null, { timeout: 1500 }).then(() =>
      expect(container.querySelector(".MuiButton-containedPrimary").disabled).toBeFalsy()
    );
  });

  it("input value should be 'another value'", () => {
    const { container } = render(<ButtonGroupForm defaultInputValue="default value" />);
    const input = container.querySelector("input");

    fireEvent.change(input, { target: { value: "another value" } });
    expect(input.value).toEqual("another value");
  });

  it("save button should have been called after 1.5 seconds", () => {
    const { container, findByText } = render(
      <ButtonGroupForm defaultInputValue="default value" handleSave={mockFunction} />
    );
    const input = container.querySelector("input");

    fireEvent.change(input, { target: { value: "another value" } });
    findByText("another value", null, { timeout: 1500 }).then(() =>
      expect(mockFunction).toHaveBeenCalled()
    );
  });

  it("cancel button should have been called", () => {
    const { getByText } = render(<ButtonGroupForm handleCancel={mockFunction} />);

    userEvent.click(getByText("cancel", { exact: false }));
    expect(mockFunction).toHaveBeenCalled();
  });
});
