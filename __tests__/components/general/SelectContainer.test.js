import { create } from "react-test-renderer";
import { render } from "@testing-library/react";
import SelectContainer from "../../../src/components/general/SelectContainer";

describe("<SelectContainer /> component test", () => {
  const mockList = Array.from({ length: 5 }, (_, i) => ({ value: `value ${i}` }));

  it("should match the snapshot", () => {
    const component = create(
      <SelectContainer
        titleSelect="Test title"
        minWidth={200}
        maxWidth={400}
        listSelect={mockList}
      />
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("titleSelect should be present in the document", () => {
    const { getByText } = render(
      <SelectContainer
        titleSelect="Test title"
        minWidth={200}
        maxWidth={400}
        listSelect={mockList}
      />
    );

    expect(getByText("Test title")).toBeInTheDocument();
  });

  it("placeholderSelect should be present in the document", () => {
    const { getByText } = render(
      <SelectContainer
        titleSelect="Test title"
        minWidth={200}
        maxWidth={400}
        listSelect={mockList}
        placeholderSelect="test placeholder"
      />
    );

    expect(getByText("test placeholder")).toBeInTheDocument();
  });

  it("button should be present in the document", () => {
    const { getByRole } = render(
      <SelectContainer
        titleSelect="Test title"
        minWidth={200}
        maxWidth={400}
        listSelect={mockList}
        placeholderSelect="test placeholder"
      />
    );

    expect(getByRole("button")).toBeInTheDocument();
  });

  it("button should be present in the document", () => {
    const { getByRole } = render(
      <SelectContainer
        titleSelect="Test title"
        minWidth={200}
        maxWidth={400}
        listSelect={mockList}
        placeholderSelect="test placeholder"
      />
    );

    expect(getByRole("button")).toBeInTheDocument();
  });
});
